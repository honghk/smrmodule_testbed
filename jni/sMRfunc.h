
#ifndef __SMR_FUNC_H__
#define __SMR_FUNC_H__

#include "sMRstruct.h"

extern void sMR_start(void) ;       // only Main call function
extern void sMR_end(void) ;         // only Main call function
extern int sMR_open(char *dir) ;	
extern void sMR_close(void) ;

///[READ]/////
extern int sMR_read_smr_manage(void) ;
extern int sMR_read_smr_meshinfo(void) ;
extern int sMR_read_smr_cacheinfo(int flag) ;

///[READ]/////
extern int sMR_read_node_count(int map_id, int *node_count) ;
extern int sMR_read_link_count(int map_id, int *link_count) ;

extern int sMR_read_mesh_checksum(int map_id, int *check_sum) ;
extern int sMR_read_node_list(int map_id, int node_count, SNODE *s_node_list) ;
extern int sMR_read_node(int map_id, int node_id, SNODE *s_node) ;
extern int sMR_read_crossnode(int map_id, SNODE *s_node, SCROSSNODE *s_crossnode) ;
extern int sMR_read_crossnode(int map_id, int node_id, SCROSSNODE *s_crossnode, int *s_crossnode_count) ;
extern int sMR_read_adjnode(int map_id, SNODE *s_node, SADJNODE *s_adjnode) ;
extern int sMR_read_adjnode(int map_id, int node_id, SADJNODE *s_adjnode) ;
extern int sMR_read_link(int map_id, int link_id, SLINK *s_link) ;
extern int sMR_read_link_list(int map_id, int link_count, SLINK *s_link_list) ;
// s_vertex_list는 호출한 곳에서 free()로 해제 해주어야 한다.[delete로 해재 하면 안됨]
extern int sMR_read_vertex_list(int map_id, int link_id, SVERTEX **s_vertex_list, int *vertex_count) ;
// s_vertex_list는 호출한 곳에서 free()로 해제 해주어야 한다.[delete로 해제 하면 안됨]
extern int sMR_read_mapid_vertex_list(int map_id, SVERTEX **s_vertex_list, int *vertex_count) ;
// s_vertex_list는 호출한 곳에서 free()로 해제 해주어야 한다.[delete로 해제 하면 안됨]
extern int sMR_read_start_end_vertex_list(int map_id, int link_id, SVERTEX **s_vertex_list, int *vertex_count) ;
////[2013.06.24]/////////////////////
extern int sMR_read_vertex_count(int map_id, int link_id, int *vertex_count) ;
extern int sMR_read_vertex_data(int map_id, int link_id, SVERTEX *vertex_list) ;

extern int sMR_read_mapid_vertex_count(int map_id, int *vertex_count) ;
extern int sMR_read_mapid_vertex_data(int map_id, SVERTEX *vertex_list) ;

extern int sMR_read_start_end_vertex_count(int map_id, int link_id, int *vertex_count) ;
extern int sMR_read_start_end_vertex_data(int map_id, int link_id, SVERTEX *vertex_list) ;
////[2013.06.24]/////////////////////


extern int sMR_read_mesh(int map_id, char *buf, int buf_size) ;
extern int sMR_read_mesh(int map_id, char *buf, int ds_mesh, int buf_size) ;

///[GET]//////
extern int sMR_get_real_mesh(int xpos, int ypos, REAL_MESH_INFO *real_mesh_list, int *count) ;
extern int sMR_get_srank_map(int map_id, SRANK_MAP *srank_map) ;
extern int sMR_get_smesh_info(int map_id, SMESH_INFO *smesh_info) ;
extern void sMR_get_smap_header(SMAP_HEADER *smap_header) ;
extern void sMR_get_srank_manager(SRANK_MANAGER *srank_manager) ;
extern void sMR_get_version(int *version) ;

///[SET]/////
int sMR_set_dir(char *dirPath) ;


#endif //__SMR_FUNC_H__

