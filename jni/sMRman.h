#ifndef __SMR_MAN_H__
#define __SMR_MAN_H__

//#ifdef __cplusplus
//extern "C" {
//#endif

//#pragma pack(1)

//#include <stdio.h>
//#include <string.h>
//#include <map>

#include "sMRstruct.h"
#include "sMRfunc.h"

////[OUTER SOURCE]////////////////////////////////////////////////////////////////////////////////////
extern char* sMR_get_log();
///[ETC]//////
extern void sMR_init(void) ;		// only Main One call function
extern void sMR_open_set(int flag) ;
extern int sMR_open_manage(void) ;
extern int sMR_open_meshinfo(void) ;
extern int sMR_open_meshdata(void) ;
extern int sMR_open_cacheinfo(void) ;

extern void sMR_close_manage(void) ;
extern void sMR_close_meshinfo(void) ;
extern void sMR_close_cacheinfo(void) ;
extern void sMR_close_meshdata(void) ;
extern int sMR_file_exist(char *fileName) ;
extern int sMR_file_delete(char *fileName) ;
extern int sMR_delete(void) ;
/*
extern void sMR_start(void) ;      	// only Main call function
extern void sMR_end(void) ;         // only Main call function
extern int sMR_open(char *dir) ;
extern void sMR_close(void) ;

///[READ]/////
extern int sMR_read_node_count(int map_id, int *node_count) ;
extern int sMR_read_link_count(int map_id, int *link_count) ;

extern int sMR_read_mesh_checksum(int map_id, int *check_sum) ;
extern int sMR_read_node_list(int map_id, int node_count, SNODE *s_node_list) ;
extern int sMR_read_node(int map_id, int node_id, SNODE *s_node) ;
extern int sMR_read_crossnode(int map_id, SNODE *s_node, SCROSSNODE *s_crossnode) ;
extern int sMR_read_crossnode(int map_id, int node_id, SCROSSNODE *s_crossnode, int *s_crossnode_count) ;
extern int sMR_read_adjnode(int map_id, SNODE *s_node, SADJNODE *s_adjnode) ;
extern int sMR_read_link(int map_id, int link_id, SLINK *s_link) ;
extern int sMR_read_link_list(int map_id, int link_count, SLINK *s_link_list) ;
// s_vertex_list�� ȣ���� ������ free���־�� �Ѵ�.
extern int sMR_read_vertex_list(int map_id, int link_id, SVERTEX **s_vertex_list, int *vertex_count) ;

extern int sMR_read_mesh(int map_id, char *buf, int buf_size) ;
extern int sMR_read_mesh(int map_id, char *buf, int ds_mesh, int buf_size) ;

///[GET]//////
extern int sMR_get_srank_map(int map_id, SRANK_MAP *srank_map) ;
extern int sMR_get_smesh_info(int map_id, SMESH_INFO *smesh_info) ;
extern void sMR_get_smap_header(SMAP_HEADER *smap_header) ;
extern void sMR_get_srank_manager(SRANK_MANAGER *srank_manager) ;
extern void sMR_get_version(int *version) ;
*/
extern int sMR_read_adjnode(int map_id, int node_id, SADJNODE *s_adjnode) ;
extern void sMR_get_now_fetch_count(int *fetch_count) ;

///[SET]//////
//extern int sMR_set_smesh_info(int map_id) ;
extern int sMR_set_smesh_info(int map_id, int ds) ;
extern int sMR_mapping_init(void) ;
extern int sMR_ridx_init(void) ;

///[WRITE]////
extern int sMR_write_smr_manage(char *buf, int buf_size, int *file_size) ;
extern int sMR_write_smr_meshinfo(char *buf, int buf_size, int *file_size) ;
extern int sMR_write_smr_cacheinfo(char *buf, int buf_size, int *file_size) ;
extern int sMR_overwrite_smr_cacheinfo(void) ;
extern int sMR_make_smr_cacheinfo(void) ;
extern int sMR_write_mesh(char *buf, int buf_size, int *file_end, int *file_size) ;
extern int sMR_write_meshdata(char *buf, int buf_size, int *file_end, int *file_size,
		int *get_idx, int *get_offset, int count) ;
extern int sMR_write_smr_zero(void) ;

////[SIDX FUNCTION]////////////////////////////////////////////////////////////////////////////////////
extern int sidx_insert(int in) ;
extern int sidx_delete(int in) ;
extern int sidx_multi_insert(int *in_list, int count) ;
extern int sidx_multi_get(int **out_list, int *count) ;
extern int sidx_multi_delete(int *in_list, int count) ;
extern int sidx_init(void) ;
extern int sidx_clear(void) ;

////[RIDX FUNCTION]////////////////////////////////////////////////////////////////////////////////////
extern int ridx_init(int sz) ;
extern void ridx_end(void) ;
extern int ridx_set_entry(int *entry) ;
extern int ridx_get(int map_id, int *entry) ;
extern int i_smr_read_mesh_ridx(int map_id, int entry) ;

////[INNER FUNCTION]////////////////////////////////////////////////////////////////////////////////////

extern void i_smr_create(void) ;		//�����ڿ�: Ÿ���̺귯������ ������� ����
extern void i_smr_destruct(void) ;	//�����ڿ�: Ÿ���̺귯������ ������� ����

extern int 	i_smr_open(void) ;
extern int	i_smr_open_manage(void) ;
extern int	i_smr_open_meshinfo(void) ;
extern int	i_smr_open_meshdata(void) ;
extern int	i_smr_open_cacheinfo(void) ;
extern int 	i_smr_close(void) ;
extern void i_smr_close_manage(void) ;
extern void i_smr_close_meshinfo(void) ;
extern void i_smr_close_cacheinfo(void) ;
extern void i_smr_close_meshdata(void) ;
extern int 	i_smr_re_open(void) ;
extern int	i_smr_file_exist(char *fileName) ;
extern int	i_smr_file_delete(char *fileName) ;
extern unsigned short i_smr_check_sum(unsigned short *buf, int len);
	
extern int 	i_smr_read_smr_manage(void) ;					
extern int 	i_smr_read_smr_meshinfo(void) ;
extern int 	i_smr_read_smr_cacheinfo(int flag) ;

extern int 	i_smr_read_node_count(int map_id, int *node_count) ;
extern int 	i_smr_read_link_count(int map_id, int *link_count) ;

extern int 	i_smr_read_mesh(int map_id, char *buf, int buf_size) ;				//debug�뵵
extern int	i_smr_read_mesh(int map_id, char *buf, int ds_mesh, int buf_size) ;	//debug�뵵

extern int 	i_smr_read_mesh_checksum(int map_id, int *check_sum) ;
extern int 	i_smr_read_node_list(int map_id, int node_count, SNODE *s_node_list) ;
extern int 	i_smr_read_node(int map_id, int node_id, SNODE *s_node) ;
extern int 	i_smr_read_crossnode(int map_id, SNODE *s_node, SCROSSNODE *s_crossnode) ;
extern int	i_smr_read_crossnode(int map_id, int node_id, SCROSSNODE *s_crossnode, int *s_crossnode_count) ;

extern int 	i_smr_read_adjnode(int map_id, SNODE *s_node, SADJNODE *s_adjnode) ;
extern int	i_smr_read_adjnode(int map_id, int node_id, SADJNODE *s_adjnode) ;

extern int 	i_smr_read_link(int map_id, int link_id, SLINK *s_link) ;
extern int 	i_smr_read_link_list(int map_id, int link_count, SLINK *s_link_list) ;

extern int 	i_smr_read_vertex_list(int map_id, int link_id,  SVERTEX **s_vertex_list, int *vertex_count) ;
extern int	i_smr_read_vertex_count(int map_id, int link_id, int *vertex_count) ;
extern int	i_smr_read_vertex_data(int map_id, int link_id, SVERTEX *vertex_list) ;

extern int	i_smr_read_mapid_vertex_list(int map_id, SVERTEX **s_vertex_list, int *vertex_count) ;
extern int	i_smr_read_mapid_vertex_count(int map_id, int *vertex_count) ;
extern int	i_smr_read_mapid_vertex_data(int map_id, SVERTEX *vertex_list) ;

extern int	i_smr_read_start_end_vertex_list(int map_id, int link_id, SVERTEX **s_vertex_list, int *vertex_count) ;
extern int	i_smr_read_start_end_vertex_count(int map_id, int link_id, int *vertex_count) ;
extern int	i_smr_read_start_end_vertex_data(int map_id, int link_id, SVERTEX *vertex_list) ;

///[GET]//////
extern void i_smr_get_now_fetch_count(int *fetch_count) ;
extern int 	i_smr_get_srank_map(int map_id, SRANK_MAP *srank_map) ;
extern int 	i_smr_get_smesh_info(int map_id, SMESH_INFO *smesh_info) ;
extern void i_smr_get_smap_header(SMAP_HEADER *smap_header) ;
extern void	i_smr_get_srank_manager(SRANK_MANAGER *srank_manager) ;
extern void i_smr_get_version(int *version) ;
extern int i_smr_get_real_width_count(void) ;
extern int i_smr_get_real_height_count(void) ;
extern int i_smr_get_real_mesh(int xpos, int ypos, REAL_MESH_INFO *real_mesh_list, int *count) ;
extern int i_smr_get_dir(char *dirPath) ;

///[SET]//////
extern int	i_smr_set_dir(char *dirPath) ;
extern int 	i_smr_set_smesh_info(int map_id, int ds) ;

///[ETC///////
extern int i_smr_mapping_init(void)  ;
extern int i_smr_mapping_clear(void) ;
extern int 	i_smr_exist_smesh_info(int map_id) ;

extern int i_smr_reopen_manage(void) ;
extern int i_smr_reopen_meshinfo(void) ;
extern int i_smr_reopen_cacheinfo(void) ;
extern int i_smr_reopen_meshdata(void) ;

///[WRITE]////
extern int	i_smr_write_smr_manage(char *buf, int buf_size, int *file_size) ;
extern int 	i_smr_write_smr_meshinfo(char *buf, int buf_size, int *file_size) ;
extern int 	i_smr_write_smr_cacheinfo(char *buf, int buf_size, int *file_size) ;
extern int	i_smr_make_smr_cacheinfo(void) ;

extern int i_smr_overwrite_smr_cacheinfo(void) ;
extern int i_smr_write_mesh(char *buf, int buf_size, int *file_end, int *file_size) ;
extern int i_smr_write_smr_zero(void) ;
////[INNER FUNCTION]////////////////////////////////////////////////////////////////////////////////////

////[DEBUG SUPPORT FUNCTION]////////////////////////////////////////////////////////////////////////////
extern void debug_setting_cache_info(int flag) ;
extern void debug_copy_cache_info(int *d_cache_info) ;


//#ifdef __cplusplus
//}
//#endif

#endif
