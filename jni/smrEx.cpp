
#include "sMRstruct.h"
#include "sMRfunc.h"
#include "smrEx.h"
#include "sMRman.h"

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getSlink( JNIEnv* env, jobject, jint map_id, jobjectArray info)
{
        int ret = NO_ERR;
        int link_cnt = 0;
        int i = 0;
        int j = 0;
        int vertex_cnt = 0;

        jclass clazz;
        jfieldID fid;
        jmethodID mid;
        jobject obj;

        jobject clazz_vertex_init;
        jclass clazz_vertex;
        jobject obj_vertex;
        jobjectArray obj_vertex_arr;

        SLINK *s_link_list = NULL;
        SVERTEX *s_vertex_list = NULL ;

        ret = sMR_read_link_count(map_id, &link_cnt) ;

        if(ret != NO_ERR)
        {
            goto error_exit ;
        }

        if((s_link_list = (SLINK *) malloc (link_cnt * sizeof(SLINK))) == NULL) {
            D("ERR: malloc fail\n") ;
            ret = ERR_MALLOC_FAIL ;//exit  없어졌으니 주의
        }
        memset(s_link_list, 0, link_cnt * sizeof(SLINK)) ;

        ret = sMR_read_link_list(map_id, link_cnt, s_link_list) ;

        if(ret != NO_ERR)
        {
            goto error_exit ;
        }

        ///////////////////////////////////////////////////////////

        for(i = 0; i < link_cnt; i++)
        {

            obj = (env)->GetObjectArrayElement(info, i); // JAVA
            clazz = (env)->GetObjectClass(obj); // JNI

            if(0 == clazz) {
                D("ERR: GetObjectClass reutruned 0\n");
                return ERR_UNKNOWN ;
            }

            fid = (env)->GetFieldID(clazz, "_startId", "S");
            (env)->SetShortField(obj, fid, s_link_list[i]._startId);
            fid = (env)->GetFieldID(clazz, "_endId", "S");
            (env)->SetShortField(obj, fid, s_link_list[i]._endId);
            fid = (env)->GetFieldID(clazz, "_length", "S");
            (env)->SetShortField(obj, fid, s_link_list[i]._length);
            fid = (env)->GetFieldID(clazz, "_attr", "S");
            (env)->SetShortField(obj, fid, s_link_list[i]._attr);
            fid = (env)->GetFieldID(clazz, "_numOfVertex", "S");
            (env)->SetShortField(obj, fid, s_link_list[i]._numOfVertex);
            fid = (env)->GetFieldID(clazz, "_vertexIndex", "S");
            (env)->SetShortField(obj, fid, s_link_list[i]._vertexIndex);
            fid = (env)->GetFieldID(clazz, "laneNum", "I");
            (env)->SetIntField(obj, fid, s_link_list[i].laneNum());
            fid = (env)->GetFieldID(clazz, "drive", "I");
            (env)->SetIntField(obj, fid, s_link_list[i].drive());
            fid = (env)->GetFieldID(clazz, "divide", "I");
            (env)->SetIntField(obj, fid, s_link_list[i].divide());
            fid = (env)->GetFieldID(clazz, "linkType", "I");
            (env)->SetIntField(obj, fid, s_link_list[i].linkType());
            fid = (env)->GetFieldID(clazz, "linkAttr", "I");
            (env)->SetIntField(obj, fid, s_link_list[i].linkAttr());
            fid = (env)->GetFieldID(clazz, "roadType", "I");
            (env)->SetIntField(obj, fid, s_link_list[i].roadType());

            (env)->DeleteLocalRef(obj);
            (env)->DeleteLocalRef(clazz);
        }

        (env)->DeleteLocalRef(info);

        free(s_link_list);
        s_link_list = NULL;

error_exit:

        return ret;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getNode( JNIEnv* env, jobject, jint map_id, jobjectArray info)
{
        int ret = NO_ERR;
        int node_cnt = 0 ;
        int cross_cnt = 0;
        int i = 0;
        int j = 0;

        jclass clazz;
        jclass clazz_adj;
        jclass clazz_cross;

        jfieldID fid;
        jobject obj;
        jobject obj_adj;
        jobject obj_cross;

        jcharArray jCharArr;
        jshortArray jShortArr;
        jshort* jSarr;
        jchar* jCarr;

        SNODE *s_node_list ;
        SADJNODE s_adjnode;
        SCROSSNODE s_crossnode;

        ret = sMR_read_node_count(map_id, &node_cnt) ;

        if(ret != NO_ERR)
        {
            goto error_exit ;
        }

        if((s_node_list = (SNODE *) malloc (node_cnt * sizeof(SNODE))) == NULL)
        {
            D("ERR: malloc fail\n") ;
            ret = ERR_MALLOC_FAIL ;
        }

        memset(s_node_list, 0, node_cnt * sizeof(SNODE)) ;

        ret = sMR_read_node_list(map_id, node_cnt, s_node_list);

        if(ret != NO_ERR)
        {
            goto error_exit ;
        }

        ///////////////////////////////////////////////////////////

        for(i = 0; i < node_cnt; i++)
        {

            obj = (env)->GetObjectArrayElement(info, i); // JAVA
            clazz = (env)->GetObjectClass(obj); // JNI
            if(0 == clazz) {
                D("ERR: GetObjectClass reutruned 0\n");
                return ERR_UNKNOWN ;
            }
            fid = (env)->GetFieldID(clazz, "offLon", "C");
            (env)->SetCharField(obj, fid, s_node_list[i].offLon);
            fid = (env)->GetFieldID(clazz, "offLat", "C");
            (env)->SetCharField(obj, fid, s_node_list[i].offLat);
            fid = (env)->GetFieldID(clazz, "ds", "I");
            (env)->SetIntField(obj, fid, s_node_list[i].ds);
            fid = (env)->GetFieldID(clazz, "isAdjNode", "Z");
            (env)->SetBooleanField(obj, fid, s_node_list[i].isAdjNode());
            fid = (env)->GetFieldID(clazz, "isUpperMatchedNode", "Z");
            (env)->SetBooleanField(obj, fid, s_node_list[i].isUpperMatchedNode());
            fid = (env)->GetFieldID(clazz, "numofJCLink", "I");
            (env)->SetIntField(obj, fid, s_node_list[i].getnumofJCLink());
            fid = (env)->GetFieldID(clazz, "exNodeDs", "I");
            (env)->SetIntField(obj, fid, s_node_list[i].getExNodeDS());

            if(s_node_list[i].isAdjNode()==1)
            {

                memset(&s_adjnode, 0, sizeof(SADJNODE)) ;
                ret = sMR_read_adjnode(map_id, &s_node_list[i], &s_adjnode);

                if(ret != NO_ERR)
                {
                    goto error_exit ;
                }

                fid = (env)->GetFieldID(clazz, "sAdjNode", "Lcom/thinkware/smr/smrtestbed/Snode$SAdjNode;");
                obj_adj = (env)->GetObjectField(obj, fid);

                ///////////////////////////////////////////////////////////////
                clazz_adj = (env)->GetObjectClass(obj_adj);

                fid = (env)->GetFieldID(clazz_adj, "adjMapId", "I");
                (env)->SetIntField(obj_adj, fid, s_adjnode.adjMapId);
                fid = (env)->GetFieldID(clazz_adj, "adjNodeId", "S");
                (env)->SetShortField(obj_adj, fid, s_adjnode.adjNodeId);
                fid = (env)->GetFieldID(clazz_adj, "adjLinkId", "S");
                (env)->SetShortField(obj_adj, fid, s_adjnode.adjLinkId);

                (env)->DeleteLocalRef(obj_adj);
                (env)->DeleteLocalRef(clazz_adj);
             }

            else if(s_node_list[i].isAdjNode()==0)
            {

                memset(&s_crossnode, 0, sizeof(SCROSSNODE));
                ret = sMR_read_crossnode(map_id, &s_node_list[i], &s_crossnode);

                if(ret != NO_ERR)
                {
                    goto error_exit ;
                }

                fid = (env)->GetFieldID(clazz, "sCrossNode", "Lcom/thinkware/smr/smrtestbed/Snode$SCrossNode;");
                obj_cross = (env)->GetObjectField(obj, fid);

                ///////////////////////////////////////////////////////////////
                clazz_cross = (env)->GetObjectClass(obj_cross);

                fid = (env)->GetFieldID(clazz_cross, "conlink", "[S");
                jShortArr = (jshortArray)env->GetObjectField(obj_cross, fid);
                jSarr = (env)->GetShortArrayElements(jShortArr, 0) ;

                fid = (env)->GetFieldID(clazz_cross, "conpasscode", "[C");
                jCharArr = (jcharArray)env->GetObjectField(obj_cross, fid);
                jCarr = (env)->GetCharArrayElements(jCharArr, 0);

                for(j = 0; j < 8; j++)
                {
                    jSarr[j] = s_crossnode.conlink[j];
                    jCarr[j] = s_crossnode.conpasscode[j];
                }
                (env)->ReleaseShortArrayElements(jShortArr, jSarr, 0);
                (env)->ReleaseCharArrayElements(jCharArr, jCarr, 0);

                (env)->DeleteLocalRef(jShortArr);
                (env)->DeleteLocalRef(jCharArr);
                (env)->DeleteLocalRef(obj_cross);
                (env)->DeleteLocalRef(clazz_cross);

            }

            else{

            }
            (env)->DeleteLocalRef(obj);
            (env)->DeleteLocalRef(clazz);
        }

        (env)->DeleteLocalRef(info);

        free(s_node_list);
        s_node_list = NULL;

error_exit:

        return ret;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getSvertex( JNIEnv* env, jobject, jint map_id, jint link_id, jint vertex_id, jobject info)
{
        int ret = NO_ERR;
        int vertex_cnt = 0;
        int link_cnt = 0;
        int i = 0;

        jclass clazz;
        jfieldID fid;

        SVERTEX *s_vertex_list = NULL;

        ret = sMR_read_link_count(map_id, &link_cnt) ;

        if(ret != NO_ERR)
        {
            goto error_exit ;
        }

        ret = sMR_read_vertex_list(map_id, link_id, &s_vertex_list, &vertex_cnt);

        if(ret != NO_ERR)
        {
            goto error_exit ;
        }

        clazz = (env)->GetObjectClass(info);
        if(0 == clazz) {
           D("ERR: GetObjectClass reutruned 0\n");
           return ERR_UNKNOWN ;
        }

        for(i = 0; i < vertex_cnt; i++)
        {
            if(i == vertex_id)
            {
                fid = (env)->GetFieldID(clazz, "offLon", "S");
                (env)->SetShortField(info, fid, s_vertex_list[vertex_id].offLon);
                fid = (env)->GetFieldID(clazz, "offLat", "S");
                (env)->SetShortField(info, fid, s_vertex_list[vertex_id].offLat);
                break;
            }
        }



        if(s_vertex_list != NULL){
            free(s_vertex_list) ;
        }
        s_vertex_list = NULL ;

        (env)->DeleteLocalRef(clazz);
        (env)->DeleteLocalRef(info);

error_exit:

        return ret;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getCrossnode( JNIEnv* env, jobject, jint map_id, jint node_id, jobject info)
{
        int ret = NO_ERR;
        int cross_cnt = 0;

        jclass clazz;
        jfieldID fid;

        SCROSSNODE scrossnode;

        memset(&scrossnode, 0, sizeof(SCROSSNODE));
        ret = sMR_read_crossnode(map_id, node_id, &scrossnode, &cross_cnt);

        ///////////////////////////////////////////////////////////
        clazz = (env)->GetObjectClass(info);

        if(0 == clazz) {
            D("ERR: GetObjectClass reutruned 0\n");
            return ERR_UNKNOWN ;
        }

        fid = (env)->GetFieldID(clazz, "conlink", "[S");
        jshortArray jShortArr = (jshortArray)env->GetObjectField(info, fid) ;
        jshort* jSarr = (env)->GetShortArrayElements(jShortArr, 0) ;

        fid = (env)->GetFieldID(clazz, "conpasscode", "[C");
        jcharArray jCharArr = (jcharArray)env->GetObjectField(info, fid) ;
        jchar* jCarr = (env)->GetCharArrayElements(jCharArr, 0) ;

        int i = 0;

        for(i = 0; i < cross_cnt; i++)
        {
            jSarr[i] = scrossnode.conlink[i];
            jCarr[i] = scrossnode.conpasscode[i];
        }
        (env)->ReleaseShortArrayElements(jShortArr, jSarr, 0) ;
        (env)->ReleaseCharArrayElements(jCharArr, jCarr, 0) ;
        (env)->DeleteLocalRef(clazz);
        (env)->DeleteLocalRef(info);


        return ret;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getSmeshInfo( JNIEnv* env, jobject, jint map_id, jobject info)
{
        int ret = NO_ERR;

        jclass clazz;
        jfieldID fid;

        SMESH_INFO m_mesh_info ;

        memset(&m_mesh_info, 0, sizeof(SMESH_INFO));
        ret = sMR_get_smesh_info(map_id, &m_mesh_info);

        if(ret != NO_ERR)
        {
            goto error_exit ;
        }

        ///////////////////////////////////////////////////////////
        clazz = (env)->GetObjectClass(info);
        if(0 == clazz) {
            D("ERR: GetObjectClass reutruned 0\n");
            return ERR_UNKNOWN ;
        }
        fid = (env)->GetFieldID(clazz, "meshId", "S");
        (env)->SetShortField(info, fid, m_mesh_info.meshId);
        fid = (env)->GetFieldID(clazz, "ds", "I");
        (env)->SetIntField(info, fid, m_mesh_info.ds);
        fid = (env)->GetFieldID(clazz, "size", "I");
        (env)->SetIntField(info, fid, m_mesh_info.size);

        (env)->DeleteLocalRef(clazz);
        (env)->DeleteLocalRef(info);

error_exit:

        return ret;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getSrankMap( JNIEnv* env, jobject, jint map_id, jobject info)
{
        int ret = NO_ERR ;

    	jclass clazz ;
    	jfieldID fid ;

    	SRANK_MAP m_srank_map ;

    	memset(&m_srank_map, 0, sizeof(SRANK_MAP)) ;
    	ret = sMR_get_srank_map(map_id, &m_srank_map) ;

    	if(ret != NO_ERR)
        {
            goto error_exit ;
        }

    	///////////////////////////////////////////////////////////
    	clazz = (env)->GetObjectClass(info) ;
    	if(0 == clazz) {
    		D("ERR: GetObjectClass reutruned 0\n") ;
    		return ERR_UNKNOWN ;
    	}
    	fid = (env)->GetFieldID(clazz, "numOfNode", "S") ;
    	(env)->SetShortField(info, fid, m_srank_map.numOfNode);
    	fid = (env)->GetFieldID(clazz, "numOfLink", "S") ;
    	(env)->SetShortField(info, fid, m_srank_map.numOfLink);
    	fid = (env)->GetFieldID(clazz, "xratio", "I") ;
    	(env)->SetIntField(info, fid, m_srank_map.xratio);
    	fid = (env)->GetFieldID(clazz, "left", "I") ;
    	(env)->SetIntField(info, fid, m_srank_map.left);
    	fid = (env)->GetFieldID(clazz, "bottom", "I") ;
    	(env)->SetIntField(info, fid, m_srank_map.bottom);
    	fid = (env)->GetFieldID(clazz, "offsetNode", "I") ;
    	(env)->SetIntField(info, fid, m_srank_map.offsetNode);
        fid = (env)->GetFieldID(clazz, "offsetCrossnode", "I") ;
        (env)->SetIntField(info, fid, m_srank_map.offsetCrossnode);
        fid = (env)->GetFieldID(clazz, "offsetAdjnode", "I") ;
        (env)->SetIntField(info, fid, m_srank_map.offsetAdjnode);
        fid = (env)->GetFieldID(clazz, "offsetLink", "I") ;
        (env)->SetIntField(info, fid, m_srank_map.offsetLink);
        fid = (env)->GetFieldID(clazz, "offsetVertex", "I") ;
        (env)->SetIntField(info, fid, m_srank_map.offsetVertex);

        (env)->DeleteLocalRef(clazz);
        (env)->DeleteLocalRef(info);

error_exit:

    	return ret ;
}
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_sidxInsert( JNIEnv* env, jobject, jint map_id)
{
    int ret = NO_ERR;
    ret = sidx_insert(map_id);
    return ret;
}

JNIEXPORT jstring JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_logCall( JNIEnv* env, jobject thiz )
{
    return env->NewStringUTF((const char *)sMR_get_log());
}

JNIEXPORT jstring JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_stringFromJNI( JNIEnv* env, jobject thiz )
{
	return env->NewStringUTF((const char *)"Hello from JNI !");
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_jcSMR_intFromJNI( JNIEnv* env, jobject thiz, jintArray arr )
{
	int ret = NO_ERR ;

	jint *carr = NULL ;
	int len = 0 ;

	int i = 0 ;
	int *idx = NULL ;
	int idx_num = 0 ;

	carr = (env)->GetIntArrayElements(arr, NULL) ; // arr가져오기
	if(carr == NULL) return -1 ; // 못가져오면 에러코드

	len = (env)->GetArrayLength(arr) ;
	D("len[%d]\n", len) ;

error_exit:
	(env)->ReleaseIntArrayElements(arr, carr, 0) ;
	return ret ;
}


////[TEST]////////////////////////////////////////////////////////////////////////////////

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_jcSMR_getNowFetchCount( JNIEnv* env, jobject thiz )
{
	int fetch_cnt = 0 ;
	sMR_get_now_fetch_count(&fetch_cnt) ; // m_cash_info가 0보다 크거나 같을때 numberOfMesh를 가져온다
	return fetch_cnt ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_jcSMR_testReadNodeList( JNIEnv* env, jobject thiz, jint map_id )
{
	int ret = 0 ;

	int i = 0 ;
	int node_cnt = 0 ;
	SNODE *s_node_list = NULL ; // offLon, offLat, ds, isAdjNode, isUpperMatchedNode, getnum of JC Link
	SNODE temp ;// offLon, offLat, ds, isAdjNode, isUpperMatchedNode, getnum of JC Link, get Ex Node DS

	int *list = NULL ;
	int count = 0 ;

	ret = sMR_read_node_count(map_id, &node_cnt) ; //mapid가 정상인지 node_cnt 가져오고 빈공간 sidx에 mapid를 넣어줌
	if(ret) goto error_exit ;
	D("node_cnt[%d]\n", node_cnt) ;

	if((s_node_list = (SNODE *) malloc (node_cnt * sizeof(SNODE))) == NULL) {
		D("ERR: malloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(s_node_list, 0, node_cnt * sizeof(SNODE)) ;

	ret = sMR_read_node_list(map_id, node_cnt, s_node_list) ; // parameter정산인지 확인후..... 

	for(i = 0 ; i < node_cnt ; i++) {
		D("[%d]==>offLon[%d] offLat[%d]\n",
				i, s_node_list[i].offLon, s_node_list[i].offLat) ;
		D("    isAdjNode(%d)isUpperMatchedNode(%d)getnumofJCLink(%d)getExNodeDS(%d)\n",
				s_node_list[i].isAdjNode(), s_node_list[i].isUpperMatchedNode(),
				s_node_list[i].getnumofJCLink(), s_node_list[i].getExNodeDS()) ;
	}

	if(s_node_list != NULL) free(s_node_list) ;
	s_node_list = NULL ;

error_exit :
	D("=====================================\n") ;
	sidx_multi_get(&list, &count) ;
	for(i = 0 ; i < count ; i++) {
		D("sidx[%d]=>[%d]\n", i, list[i]) ;
	}
	if(list != NULL) free(list) ;
	list = NULL ;

	return ret;
}
///////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_init( JNIEnv* env, jobject thiz )
{
	sMR_init() ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_setDir( JNIEnv * env, jobject jobj, jstring name )
{
	int ret = NO_ERR ;
	jboolean iscopy;
	const char *mfile = (env)->GetStringUTFChars(name, &iscopy); 
	D("mfile[%s]\n", mfile) ;

	ret = sMR_set_dir((char *) mfile) ;

	(env)->ReleaseStringUTFChars(name, mfile);
	return ret ;
}

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_start( JNIEnv* env, jobject thiz )
{
	sMR_start() ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_open( JNIEnv* env, jobject thiz )
{
	return sMR_open(NULL) ;
}

/////////////////////////////////
JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_openSet( JNIEnv* env, jobject thiz, jint flag )
{
	sMR_open_set(flag) ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_openManage( JNIEnv* env, jobject thiz )
{
	return sMR_open_manage() ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_openMeshinfo( JNIEnv* env, jobject thiz )
{
	return sMR_open_meshinfo() ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_openMeshdata( JNIEnv* env, jobject thiz )
{
	return sMR_open_meshdata() ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_openCacheinfo( JNIEnv* env, jobject thiz )
{
	return sMR_open_cacheinfo() ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_readSmrManage( JNIEnv* env, jobject thiz ) //다시 한번 확인
{
	return sMR_read_smr_manage() ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_readSmrMeshinfo( JNIEnv* env, jobject thiz )
{
	return sMR_read_smr_meshinfo() ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_readCacheinfo( JNIEnv* env, jobject thiz, jint flag )
{
	return sMR_read_smr_cacheinfo(flag) ;
}

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_closeManage( JNIEnv* env, jobject thiz )
{
	return sMR_close_manage() ;
}

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_closeMeshinfo( JNIEnv* env, jobject thiz )
{
	return sMR_close_meshinfo() ;
}

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_closeCacheinfo( JNIEnv* env, jobject thiz )
{
	return sMR_close_cacheinfo() ;
}

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_closeMeshdata( JNIEnv* env, jobject thiz )
{
	return sMR_close_meshdata() ;
}

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_close( JNIEnv* env, jobject thiz )
{
	return sMR_close() ;
}

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_end( JNIEnv* env, jobject thiz )
{
	return sMR_end() ;
}
/////////////////////////10/24 오전
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_fileExist( JNIEnv * env, jobject jobj, jstring name )
{ // 읽기 가능한 파일인지 검사 
	int ret = NO_ERR ;
	jboolean iscopy;
	const char *mfile = (env)->GetStringUTFChars(name, &iscopy);
//	D("mfile[%s]\n", mfile) ;

	ret = sMR_file_exist((char *) mfile) ;

	(env)->ReleaseStringUTFChars(name, mfile);
	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_fileDelete( JNIEnv * env, jobject jobj, jstring name )
{
	int ret = NO_ERR ;
	jboolean iscopy;
	const char *mfile = (env)->GetStringUTFChars(name, &iscopy);
	D("mfile[%s]\n", mfile) ;

	ret = sMR_file_delete((char *) mfile) ;

	(env)->ReleaseStringUTFChars(name, mfile);
	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_delete( JNIEnv * env, jobject jobj )
{
	int ret = NO_ERR ;

	ret = sMR_delete() ;

	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_createSmrMeshdata( JNIEnv* env, jobject thiz )
{
	int file_end = 0 ; // 이어 쓰기 전 
	int file_size = 0 ; // 이어 쓰기 후  
	//sMR_write_mesh ÇÔ¼ö´Â ³»ºÎÀûÀ¸·Î appendÇÏ´Â ÇÔ¼öÀÌ¹Ç·Î
	//¹Ì¸® ¾ø´Â °ÍÀ» È®ÀÎÇÏ°í È£ÃâÇÏ´Â °ÍÀÌ´Ù.
	return sMR_write_mesh(NULL, 0, &file_end, &file_size) ;
}

///////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_writeSmrManage( JNIEnv* env, jobject obj, jbyteArray arr)
{
	int ret = NO_ERR ;
	jbyte *buf = NULL ;
	int len = 0 ;
	int file_size = 0 ;

	buf = (env)->GetByteArrayElements(arr, NULL) ;
	if(buf == NULL) {
		D("ERR: GetByteArrayElements() return NULL\n") ;
		return -1 ;
	}
	len = (env)->GetArrayLength(arr) ;

	ret = sMR_write_smr_manage((char *)buf, len, &file_size) ;
	if(len != file_size) {
		D("ERR: sMR_write_smr_manage() fail: len[%d] not equal file_size[%d]\n", len, file_size) ;
		ret = -1 ;
	}

	(env)->ReleaseByteArrayElements(arr, buf, JNI_ABORT) ;

	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_writeSmrMeshinfo( JNIEnv* env, jobject obj, jbyteArray arr)
{
	int ret = NO_ERR ;
	jbyte *buf = NULL ;
	int len = 0 ;
	int file_size = 0 ;

	buf = (env)->GetByteArrayElements(arr, NULL) ;
	if(buf == NULL) {
		D("ERR: GetByteArrayElements() return NULL\n") ;
		return -1 ;
	}
	len = (env)->GetArrayLength(arr) ;

	ret = sMR_write_smr_meshinfo((char *)buf, len, &file_size) ;
	if(len != file_size) {
		D("ERR: sMR_write_smr_meshinfo() fail: len[%d] not equal file_size[%d]\n", len, file_size) ;
		ret = -1 ;
	}

	(env)->ReleaseByteArrayElements(arr, buf, JNI_ABORT) ;
	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_writeSmrCacheinfo( JNIEnv* env, jobject obj, jbyteArray arr)
{
	int ret = NO_ERR ;
	jbyte *buf = NULL ;
	int len = 0 ;
	int file_size = 0 ;

	buf = (env)->GetByteArrayElements(arr, NULL) ;
	if(buf == NULL) {
		D("ERR: GetByteArrayElements() return NULL\n") ;
		return -1 ;
	}
	len = (env)->GetArrayLength(arr) ;

	ret = sMR_write_smr_cacheinfo((char *)buf, len, &file_size) ;
	if(len != file_size) {
		D("ERR: sMR_write_smr_cacheinfo() fail: len[%d] not equal file_size[%d]\n", len, file_size) ;
		ret = -1 ;
	}

	(env)->ReleaseByteArrayElements(arr, buf, JNI_ABORT) ;
	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_writeMesh( JNIEnv* env, jobject obj, jbyteArray arr) // 이어 쓰기 (a 옵션)
{
	int ret = NO_ERR ;
	jbyte *buf = NULL ;
	int len = 0 ;
	int file_end = 0 ;
	int file_size = 0 ;

	buf = (env)->GetByteArrayElements(arr, NULL) ;
	if(buf == NULL) {
		D("ERR: GetByteArrayElements() return NULL\n") ;
		return -1 ;
	}
	len = (env)->GetArrayLength(arr) ;

	ret = sMR_write_mesh((char *)buf, len, &file_end, &file_size) ; // file_end 이어쓰기 전 file_size 이어쓰기 후
	if((file_size-file_end) != len) {
		D("ERR: sMR_write_mesh() fail: len[%d] not equal file_size[%d]\n", len, file_size) ;
		ret = -1 ;
	}

	(env)->ReleaseByteArrayElements(arr, buf, JNI_ABORT) ;
	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_writeMeshdata( JNIEnv* env, jobject obj,
		jbyteArray arr1, jintArray arr2, jintArray arr3, jint count)
{
	int ret = NO_ERR ;
	jbyte *buf1 = NULL ;
	int len1 = 0 ;

	jint *buf2 = NULL ;

	jint *buf3 = NULL ;

	int file_end = 0 ;
	int file_size = 0 ;

	buf1 = (env)->GetByteArrayElements(arr1, NULL) ;
	if(buf1 == NULL) {
		D("ERR: GetByteArrayElements() return NULL\n") ;
		return -1 ;
	}
	len1 = (env)->GetArrayLength(arr1) ;

	buf2= (env)->GetIntArrayElements(arr2, NULL) ;
	if(buf2 == NULL) {
		D("ERR: GetIntArrayElements() return NULL\n") ;
		return -1 ;
	}

	buf3= (env)->GetIntArrayElements(arr3, NULL) ;
	if(buf3 == NULL) {
		D("ERR: GetIntArrayElements() return NULL\n") ;
		return -1 ;
	}

	ret = sMR_write_meshdata((char *)buf1, len1, &file_end, &file_size, buf2, buf3, (int)count) ; 

	(env)->ReleaseByteArrayElements(arr1, buf1, JNI_ABORT) ;
	(env)->ReleaseIntArrayElements(arr2, buf2, JNI_ABORT) ;
	(env)->ReleaseIntArrayElements(arr3, buf3, JNI_ABORT) ;
	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_writeSmrZero( JNIEnv* env, jobject obj)
{
	int ret = NO_ERR ;

	ret = sMR_write_smr_zero() ; // 파일 fopen상태인거 다 close하고 파일 초기화(쓰기 모드로 열고 닫고) 다시 읽기 모드로 오 

	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_overwriteSmrCacheinfo( JNIEnv* env, jobject obj)
{
	int ret = NO_ERR ;

	ret = sMR_overwrite_smr_cacheinfo() ; // m_cache_info[numofMesh+1]에 m_cache_info체크섬을 넣어줌

	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_makeSmrCacheinfo( JNIEnv* env, jobject obj)
{
	int ret = NO_ERR ;

	ret = sMR_make_smr_cacheinfo() ; // m_cache_info[i] = m_mesh_info[i].ds 해주고 [numofMesh+1] 에 체크섬을 넣어줌

	return ret ;
}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_mappingInit( JNIEnv* env, jobject obj)
{
	int ret = NO_ERR ;

	ret = sMR_mapping_init() ; // REAL_MAPP m_mapping 배열 

	return ret ;
}

//2013.06.27
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_ridxInit( JNIEnv* env, jobject obj)
{
	int ret = NO_ERR ;

	ret = sMR_ridx_init() ; // r_idx_count 구하고 r_idx[i].data, r_idx[i].map_id = -1로 초기화 

	return ret ;
}

///////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_sidxMultiGet(JNIEnv *env, jobject obj, jintArray arr)
{
	int ret = NO_ERR ;

	jint *carr = NULL ;
	int len = 0 ;

	int i = 0 ;
	int *idx = NULL ;
	int idx_num = 0 ;

	carr = (env)->GetIntArrayElements(arr, NULL) ;
	if(carr == NULL) return -1 ;

	len = (env)->GetArrayLength(arr) ;

	ret = sidx_multi_get(&idx, &idx_num) ;
//	D("sidx_multi_get()[%d]idx_num[%d]\n", ret, idx_num) ;
	if(ret != NO_ERR) goto error_exit ;

	for(i = 0 ; i < idx_num ; i++) {
		carr[i] = (jint) idx[i] ;
	//	D("carr[%d]==>[%d]\n", i, carr[i]) ;
	}

error_exit:
	if(idx != NULL) free(idx) ;
	idx = NULL ;

	(env)->ReleaseIntArrayElements(arr, carr, 0) ;
	return ret ;
}

///////////////////////////////////////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getSmapHeader( JNIEnv* env, jclass obj, jobject info)
{
	int ret = NO_ERR ;
	int numOfMesh = 0 ;

	jclass clazz ;
	jfieldID fid ;
	jstring name ;

	SMAP_HEADER m_smap_header ;

	memset(&m_smap_header, 0, sizeof(SMAP_HEADER)) ; 
	sMR_get_smap_header(&m_smap_header) ; // m_map_header얻어옴
	///////////////////////////////////////////////////////////
	clazz = (env)->GetObjectClass(info) ;
	if(0 == clazz) {
		D("ERR: GetObjectClass reutruned 0\n") ;
		return ERR_UNKNOWN ;
	}

	name = (env)->NewStringUTF(m_smap_header.version) ;
	if(name == NULL) {
		D("ERR: NewStringUTF fail\n") ;
		return ERR_UNKNOWN ;
	}
	fid = (env)->GetFieldID(clazz, "version", "Ljava/lang/String;") ;
	(env)->SetObjectField(info, fid, name) ;

	name = (env)->NewStringUTF(m_smap_header.volumeId) ;
	if(name == NULL) {
		D("ERR: NewStringUTF fail\n") ;
		return ERR_UNKNOWN ;
	}
	fid = (env)->GetFieldID(clazz, "volumeId", "Ljava/lang/String;") ;
	(env)->SetObjectField(info, fid, name) ;

	name = (env)->NewStringUTF(m_smap_header.systemId) ;
	if(name == NULL) {
		D("ERR: NewStringUTF fail\n") ;
		return ERR_UNKNOWN ;
	}
	fid = (env)->GetFieldID(clazz, "systemId", "Ljava/lang/String;") ;
	(env)->SetObjectField(info, fid, name) ;

	name = (env)->NewStringUTF(m_smap_header.createdDate) ;
	if(name == NULL) {
		D("ERR: NewStringUTF fail\n") ;
		return ERR_UNKNOWN ;
	}
	fid = (env)->GetFieldID(clazz, "createdDate", "Ljava/lang/String;") ;
	(env)->SetObjectField(info, fid, name) ;

	name = (env)->NewStringUTF(m_smap_header.updatedDate) ;
	if(name == NULL) {
		D("ERR: NewStringUTF fail\n") ;
		return ERR_UNKNOWN ;
	}
	fid = (env)->GetFieldID(clazz, "updatedDate", "Ljava/lang/String;") ;
	(env)->SetObjectField(info, fid, name) ;

	fid = (env)->GetFieldID(clazz, "centerLon", "I") ;
	(env)->SetIntField(info, fid, m_smap_header.centerLon); 
	fid = (env)->GetFieldID(clazz, "centerLat", "I") ;
	(env)->SetIntField(info, fid, m_smap_header.centerLat); 

	return ret ;
}
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getLink( JNIEnv* env, jobject, jint map_id, jint link_id, jobject info)
{
    int ret = NO_ERR;

    jclass clazz;
    jfieldID fid;

    SLINK s_link;

    memset(&s_link, 0, sizeof(SLINK)) ;
    ret = sMR_read_link(map_id, link_id, &s_link);

    clazz = (env)->GetObjectClass(info) ;
    	if(0 == clazz) {
    		D("ERR: GetObjectClass reutruned 0\n") ;
    		return ERR_UNKNOWN ;
    	}

    	fid = (env)->GetFieldID(clazz, "_startId", "S");
        (env)->SetShortField(info, fid, s_link._startId);
        fid = (env)->GetFieldID(clazz, "_endId", "S");
        (env)->SetShortField(info, fid, s_link._endId);
        fid = (env)->GetFieldID(clazz, "_length", "S");
        (env)->SetShortField(info, fid, s_link._length);
        fid = (env)->GetFieldID(clazz, "_attr", "S");
        (env)->SetShortField(info, fid, s_link._attr);
        fid = (env)->GetFieldID(clazz, "_numOfVertex", "S");
        (env)->SetShortField(info, fid, s_link._numOfVertex);
        fid = (env)->GetFieldID(clazz, "_vertexIndex", "S");
        (env)->SetShortField(info, fid, s_link._vertexIndex);
        fid = (env)->GetFieldID(clazz, "laneNum", "I");
        (env)->SetIntField(info, fid, s_link.laneNum());
        fid = (env)->GetFieldID(clazz, "drive", "I");
        (env)->SetIntField(info, fid, s_link.drive());
        fid = (env)->GetFieldID(clazz, "divide", "I");
        (env)->SetIntField(info, fid, s_link.divide());
        fid = (env)->GetFieldID(clazz, "linkType", "I");
        (env)->SetIntField(info, fid, s_link.linkType());
        fid = (env)->GetFieldID(clazz, "linkAttr", "I");
        (env)->SetIntField(info, fid, s_link.linkAttr());
        fid = (env)->GetFieldID(clazz, "roadType", "I");
        (env)->SetIntField(info, fid, s_link.roadType());

        (env)->DeleteLocalRef(info);
        (env)->DeleteLocalRef(clazz);

    return ret;

}

JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getSrankManager( JNIEnv* env, jclass obj, jobject info)
{
	int ret = NO_ERR ;
	int numOfMesh = 0 ;

	jclass clazz ;
	jfieldID fid ;

	SRANK_MANAGER m_rank_manager ;

	memset(&m_rank_manager, 0, sizeof(SRANK_MANAGER)) ;
	sMR_get_srank_manager(&m_rank_manager) ;

	///////////////////////////////////////////////////////////
	clazz = (env)->GetObjectClass(info) ;
	if(0 == clazz) {
		D("ERR: GetObjectClass reutruned 0\n") ;
		return ERR_UNKNOWN ;
	}
	fid = (env)->GetFieldID(clazz, "numOfMesh", "I") ;
	(env)->SetIntField(info, fid, m_rank_manager.numOfMesh); 
	fid = (env)->GetFieldID(clazz, "meshWidth", "I") ;
	(env)->SetIntField(info, fid, m_rank_manager.meshWidth); 
	fid = (env)->GetFieldID(clazz, "meshHeight", "I") ;
	(env)->SetIntField(info, fid, m_rank_manager.meshHeight); 
	fid = (env)->GetFieldID(clazz, "left", "I") ;
	(env)->SetIntField(info, fid, m_rank_manager.left); 
	fid = (env)->GetFieldID(clazz, "bottom", "I") ;
	(env)->SetIntField(info, fid, m_rank_manager.bottom); 
	fid = (env)->GetFieldID(clazz, "right", "I") ;
	(env)->SetIntField(info, fid, m_rank_manager.right); 
	fid = (env)->GetFieldID(clazz, "top", "I") ;
	(env)->SetIntField(info, fid, m_rank_manager.top); 

	return ret ;
}
/////////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_readMapIdVertexList( JNIEnv* env, jobject thiz )
{
	int ret = 0 ;
	int i = 0 ;
	int map_id = 5 ;

	SVERTEX *s_vertex_list = NULL ; // offLon, offLat
	int vertex_count = 0 ;

	//sMR_read_mapid_vertex_listÇÔ¼ö¸¦ ÀÌ¿ëÇÏ¿© ¾ò¾î¿Â s_vertex_list´Â ¹ÝµíÀÌ freeÇØÁÖ¾î¾ß ÇÑ´Ù.
	ret = sMR_read_mapid_vertex_list(map_id, &s_vertex_list, &vertex_count) ;
	D("sMR_read_mapid_vertex_list(ret:%d) --> vertex_count[%d]\n", ret, vertex_count) ;
	if(ret != NO_ERR) goto error_exit ;
	for(i = 0 ; i < vertex_count ; i++) {
		D("[%d] ==>offLon[%d]offLat[%d]\n", i, s_vertex_list[i].offLon, s_vertex_list[i].offLat) ; 
	}
error_exit:
	if(s_vertex_list != NULL) free(s_vertex_list) ;
	s_vertex_list = NULL ;
}

JNIEXPORT void JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_getRealMesh( JNIEnv* env, jobject thiz, jint xpos, jint ypos )
{
//	D("jcSMR g_x[%d]\n", g_x) ;
//	g_set() ;
	int i = 0 ;
	int ret = 0 ;
	REAL_MESH_INFO info[9] ;
	int count = 9 ;

	SRANK_MAP m_rank_map ;

	int real_width_count = 0 ;
	int real_height_count = 0 ;
//	real_width_count = i_smr_get_real_width_count() ;
//	real_height_count = i_smr_get_real_height_count() ;
//	D("read_width_count[%d] real_height_count[%d]\n", real_width_count, real_height_count) ;

	memset(&info, 0, sizeof(REAL_MESH_INFO) * 9) ;
//	ret = sMR_get_real_mesh(47115000, 13470000, info, &count) ;		//count¿¡ ÇÔ¼ö ¸Þ½¬ °³¼ö¸¦ µ¹·ÁÁØ´Ù.
	ret = sMR_get_real_mesh(xpos, ypos, info, &count) ;		//count¿¡ ÇÔ¼ö ¸Þ½¬ °³¼ö¸¦ µ¹·ÁÁØ´Ù.
	D("xpos[%d]ypos[%d]==>ret[%d] count[%d]\n", xpos, ypos, ret, count) ;
	if(ret == NO_ERR) {
		for(i = 0 ; i < count ; i++) {
			D("[%d] realMeshId[%d] meshId[%d] flag[%d]\n", 
				i, info[i].realMeshId, info[i].meshId, info[i].flag) ;
			//realMeshId -> ½ÇÁ¦ µµ¿±ID
			//meshId -> µµ¿±Á¤º¸°¡ ÀÖ´Â ÀÎµ¦½º Ã·ÀÚ

			//½ÇÁ¦ mesh¿¡ ´ëÇÑ µ¥ÀÌÅÍ´Â meshId·Î ±¸ÇÑ´Ù.
			//¿¹¸¦ µé¾î SRANK_MAP¸¦ ±¸ÇÑ´Ù¸é.

			if(info[i].meshId >= 0) {	// ¹è¿­Ã·ÀÚ°¡ Á¸ÀçÇÑ´Ù¸é(Áï ¸Þ½¬Á¤º¸°¡ ÀÖ¾î ÀÎµ¦½º Á¤º¸°¡ ÀÖ´Ù¸é)
				if(info[i].flag == 0) {	// ½ÇÁ¦ ¼­¹ö¿¡¼­ µ¥ÀÌÅÍ¸¦ °¡Á®¿Í¼­ LOCAL¿¡ Á¸ÀçÇÑ´Ù¸é
					memset(&m_rank_map, 0, sizeof(SRANK_MAP)) ;
					ret = sMR_get_srank_map(info[i].meshId, &m_rank_map) ;

					if(ret == NO_ERR) {
						D("meshId[%d]-->numofNode[%d]numofLink[%d]xratio[%d]left[%d]bottom[%d]"
								"o_node[%d]o_cross[%d]o_adj[%d]o_link[%d]o_vertex[%d]\n",
								info[i].meshId,
								m_rank_map.numOfNode,
								m_rank_map.numOfLink,
								m_rank_map.xratio,
								m_rank_map.left,
								m_rank_map.bottom,
								m_rank_map.offsetNode,
								m_rank_map.offsetCrossnode,
								m_rank_map.offsetAdjnode,
								m_rank_map.offsetLink,
								m_rank_map.offsetVertex) ;
					}
				}
			}
		}
	}
}

//JNIEXPORT jint JNICALL
//Java_com_thinkware_smr_jcSMR_start( JNIEnv* env, jobject thiz )
//{
//	int ret = 0 ;
//	i_smr_create() ;
//	i_smr_set_dir(const_cast<char *>("/mnt/sdcard/dir")) ;
//	ret = i_smr_open() ;
//	D("smr_init(%d)\n", ret) ;
//	if(ret != NO_ERR) goto error_exit ;

//	ret = i_smr_read_smr_manage() ;
//	D("smr_read_smr_manage(%d)\n") ;
//	if(ret != NO_ERR) goto error_exit ;
//	ret = i_smr_read_smr_meshinfo(1) ;
//	if(ret != NO_ERR) goto error_exit ;
//	sMR_start() ;
//	sMR_open(const_cast<char *>("/mnt/sdcard/dir")) ;

//error_exit:
//	return ret ;
//}

///[TEST1]///////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_test1( JNIEnv* env, jobject thiz )
{
	int ret = 0 ;
	int node_cnt = 0 ;
	int link_cnt = 0 ;
	int i = 0 ;
	SRANK_MANAGER m_rank_manager ;
	SRANK_MAP m_rank_map ;

	memset(&m_rank_manager, 0, sizeof(SRANK_MANAGER)) ;
//	i_smr_get_srank_manager(&m_rank_manager) ;
	sMR_get_srank_manager(&m_rank_manager) ;

//	i_smr_read_node_count(5, &node_cnt) ;
//	i_smr_read_link_count(5, &link_cnt) ;
	sMR_read_node_count(5, &node_cnt) ;
	sMR_read_link_count(5, &link_cnt) ;
	D("node_cnt[%d] link_cnt[%d]\n", node_cnt, link_cnt) ;

	for(i = 0 ; i < m_rank_manager.numOfMesh ; i++)  {

		if(i == 10) break ;

		memset(&m_rank_map, 0, sizeof(SRANK_MAP)) ;
	//	i_smr_get_srank_map(i, &m_rank_map) ;
		sMR_get_srank_map(i, &m_rank_map) ;

		D("[%d]-->numofNode[%d]numofLink[%d]xratio[%d]left[%d]bottom[%d]"
				"o_node[%d]o_cross[%d]o_adj[%d]o_link[%d]o_vertex[%d]\n",
				i,
				m_rank_map.numOfNode,
				m_rank_map.numOfLink,
				m_rank_map.xratio,
				m_rank_map.left,
				m_rank_map.bottom,
				m_rank_map.offsetNode,
				m_rank_map.offsetCrossnode,
				m_rank_map.offsetAdjnode,
				m_rank_map.offsetLink,
				m_rank_map.offsetVertex) ;

	}

	return ret ;
}

///[TEST7]///////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_test7( JNIEnv* env, jobject thiz )
{
	int ret = NO_ERR ;

	int mesh_cnt = 0 ;  //µµ¿± °³¼ö
	int node_cnt = 0 ;  //µµ¿±³» ³ëµå °³¼ö
	int link_cnt = 0 ;  //µµ¿±³» ¸µÅ© °³¼ö
	int cross_cnt = 0 ;
	int vertex_cnt = 0 ;

	int check_sum_mesh  = 0 ;
	int i = 0 ;
	int j = 0 ;
	int n = 0 ;

	SRANK_MANAGER m_rank_manager ;
	SRANK_MAP m_rank_map ;
	SMESH_INFO m_mesh_info ;


	SNODE       *s_node_list = NULL ;
	SCROSSNODE  temp_crossnode ;
	SADJNODE    temp_adjnode ;

	SLINK *s_link_list = NULL ;

	SVERTEX *s_vertex_list = NULL ;

	memset(&m_rank_manager, 0, sizeof(SRANK_MANAGER)) ;
//	i_smr_get_srank_manager(&m_rank_manager) ;
	sMR_get_srank_manager(&m_rank_manager) ;

	mesh_cnt = m_rank_manager.numOfMesh ;

	D("==mesh_cnt[%d]==\n", mesh_cnt) ;

	for(i = 0 ; i < mesh_cnt ; i++) {

		if(i == 10) break ;     //debug

		memset(&m_rank_map, 0, sizeof(SRANK_MAP)) ;
	//	i_smr_get_srank_map(i, &m_rank_map) ;
		sMR_get_srank_map(i, &m_rank_map) ;
		memset(&m_mesh_info, 0, sizeof(SMESH_INFO)) ;
	//	i_smr_get_smesh_info(i, &m_mesh_info) ;
		sMR_get_smesh_info(i, &m_mesh_info) ;

		D("mesh[%d]-->numofNode[%d]numofLink[%d]xratio[%d]left[%d]bottom[%d]"
				"o_node[%d]o_cross[%d]o_adj[%d]o_link[%d]o_vertex[%d]\n",
				i,
				m_rank_map.numOfNode,
				m_rank_map.numOfLink,
				m_rank_map.xratio,
				m_rank_map.left,
				m_rank_map.bottom,
				m_rank_map.offsetNode,
				m_rank_map.offsetCrossnode,
				m_rank_map.offsetAdjnode,
				m_rank_map.offsetLink,
				m_rank_map.offsetVertex) ;
		D("mesh[%d]->meshId[%d] ds[%d] size[%d]\n",
				i, m_mesh_info.meshId, m_mesh_info.ds, m_mesh_info.size) ;

		//read checksum of mesh
	//	i_smr_read_mesh_checksum(i, &check_sum_mesh) ;
		sMR_read_mesh_checksum(i, &check_sum_mesh) ;
	//	i_smr_read_node_count(i, &node_cnt) ;
		sMR_read_node_count(i, &node_cnt) ;
	//	i_smr_read_link_count(i, &link_cnt) ;
		sMR_read_link_count(i, &link_cnt) ;

		D("mesh[%d]->check_sum[%d]node_cnt[%d]link_cnt[%d]\n",
				i, check_sum_mesh, node_cnt, link_cnt) ;

		//read node of mesh///////////////////////////////////////////////////////////////////////////////
		if((s_node_list = (SNODE *) malloc (node_cnt * sizeof(SNODE))) == NULL) {
			D("ERR: malloc fail\n") ;
			ret = ERR_MALLOC_FAIL ;
			goto error_exit ;
		}
		memset(s_node_list, 0, node_cnt * sizeof(SNODE)) ;

	//	i_smr_read_node_list(i, node_cnt, s_node_list) ;
		sMR_read_node_list(i, node_cnt, s_node_list) ;

		for(j = 0 ; j < node_cnt ; j++) {
			D("    nodeid[%d]==>offLon[%d] offLat[%d]\n",
					j, s_node_list[j].offLon, s_node_list[j].offLat) ;
			D("        isAdjNode(%d)isUpperMatchedNode(%d)getnumofJCLink(%d)getExNodeDS(%d)\n",
					s_node_list[j].isAdjNode(), s_node_list[j].isUpperMatchedNode(),
					s_node_list[j].getnumofJCLink(), s_node_list[j].getExNodeDS()) ;

			//read crossnode of mesh/////////////////////////////////////////////////////////////////////////
			if(s_node_list[j].isAdjNode() == 0) {     //crossnode¿¡ ÇÑÇØ¼­ Ãâ·Â

				cross_cnt = s_node_list[j].getnumofJCLink() ;

				memset(&temp_crossnode, 0, sizeof(SCROSSNODE)) ;
			//	i_smr_read_crossnode(i, &s_node_list[j], &temp_crossnode) ;
				sMR_read_crossnode(i, &s_node_list[j], &temp_crossnode) ;

				for(n = 0 ; n < cross_cnt ; n++) {
					D("            cross::::[%d] conlink[%d] temp_conpasscode[%d]\n",
							n, temp_crossnode.conlink[n], temp_crossnode.conpasscode[n]) ;
				}
			}
			//read adjnode of mesh
			else {      // adjnode¿¡ ÇÑÇØ¼­ Ãâ·Â
				memset(&temp_adjnode, 0, sizeof(SADJNODE)) ;
			//	i_smr_read_adjnode(i, &s_node_list[j], &temp_adjnode) ;
				sMR_read_adjnode(i, &s_node_list[j], &temp_adjnode) ;

				D("            adj::::adjMapId[%d]adjNodeId[%d]adjLinkId[%d]\n",
						temp_adjnode.adjMapId, temp_adjnode.adjNodeId, temp_adjnode.adjLinkId) ;
			}
		}

		if(s_node_list != NULL) free(s_node_list) ;
		s_node_list = NULL ;

		//read link of mesh//////////////////////////////////////////////////////////////////////////////////
	//	ret = i_smr_read_link_count(i, &link_cnt) ;
		ret = sMR_read_link_count(i, &link_cnt) ;
		if(ret) goto error_exit ;
		D("    link_cnt[%d]\n", link_cnt) ;

		if((s_link_list = (SLINK *) malloc (link_cnt * sizeof(SLINK))) == NULL) {
			D("ERR: malloc fail\n") ;
			ret = ERR_MALLOC_FAIL ;
			goto error_exit ;
		}
		memset(s_link_list, 0, link_cnt * sizeof(SLINK)) ;

	//	i_smr_read_link_list(i, link_cnt, s_link_list) ;
		sMR_read_link_list(i, link_cnt, s_link_list) ;

		for(j = 0 ; j < link_cnt ; j++) {
			D("    link_id[%d] ==> startId[%d]endId[%d]length[%d]numOfVertex[%d]vertexIndex[%d]\n",
					j,
					s_link_list[j]._startId, s_link_list[j]._endId,
					s_link_list[j]._length, s_link_list[j]._numOfVertex, s_link_list[j]._vertexIndex) ;
			D("            laneNum(%d)drive(%d)divide(%d)linkType(%d)linkAttr(%d)roadType(%d)\n",
					s_link_list[j].laneNum(), s_link_list[j].drive(), s_link_list[j].divide(),
					s_link_list[j].linkType(), s_link_list[j].linkAttr(), s_link_list[j].roadType()) ;

			//read vertex of mesh//////////////////////////////////////////////////////////////////////////////////
			//map_id, link_id ==> s_vertext_list
		//	i_smr_read_vertex_list(i, j, &s_vertex_list, &vertex_cnt) ;
			sMR_read_vertex_list(i, j, &s_vertex_list, &vertex_cnt) ;
			D("            ======>vertex_cnt[%d]=========>\n", vertex_cnt) ;
			for(n = 0 ; n < vertex_cnt ; n++) {
				D("                ====>vertex[%d]==>offLon[%d]offLat[%d]\n",
						n, s_vertex_list[n].offLon, s_vertex_list[n].offLat) ;
			}

			if(s_vertex_list != NULL) free(s_vertex_list) ;
			s_vertex_list = NULL ;

		}

		if(s_link_list != NULL) free(s_link_list) ;
		s_link_list = NULL ;

	}

error_exit:
	return ret ;
}

///[TEST6]///////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_test6( JNIEnv* env, jobject thiz )
{
	int ret = 0 ;

	int i = 0 ;
	int j = 0 ;
	int map_id = 1052 ;
	int link_cnt = 0 ;
	SLINK *s_link_list = NULL ;

	SVERTEX *s_vertex_list = NULL ;
	int vertex_cnt = 0 ;

//	ret = i_smr_read_link_count(map_id, &link_cnt) ;
	ret = sMR_read_link_count(map_id, &link_cnt) ;
	if(ret) goto error_exit ;
	D("link_cnt[%d]\n", link_cnt) ;

	if((s_link_list = (SLINK *) malloc (link_cnt * sizeof(SLINK))) == NULL) {
		D("ERR: malloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(s_link_list, 0, link_cnt * sizeof(SLINK)) ;

//	i_smr_read_link_list(map_id, link_cnt, s_link_list) ;
	sMR_read_link_list(map_id, link_cnt, s_link_list) ;

	for(i = 0 ; i < link_cnt ; i++) {
		D("map_id[%d] link_id[%d] ==> startId[%d]endId[%d]length[%d]numOfVertex[%d]vertexIndex[%d]\n",
				map_id, i,
				s_link_list[i]._startId, s_link_list[i]._endId,
				s_link_list[i]._length, s_link_list[i]._numOfVertex, s_link_list[i]._vertexIndex) ;
		D("    laneNum(%d)drive(%d)divide(%d)linkType(%d)linkAttr(%d)roadType(%d)\n",
				s_link_list[i].laneNum(), s_link_list[i].drive(), s_link_list[i].divide(),
				s_link_list[i].linkType(), s_link_list[i].linkAttr(), s_link_list[i].roadType()) ;

	//	i_smr_read_vertex_list(map_id, i, &s_vertex_list, &vertex_cnt) ;
		sMR_read_vertex_list(map_id, i, &s_vertex_list, &vertex_cnt) ;
		D("vertex_cnt[%d]\n", vertex_cnt) ;
		for(j = 0 ; j < vertex_cnt ; j++) {
			D("    vertex[%d]==>offLon[%d]offLat[%d]\n",
					j, s_vertex_list[j].offLon, s_vertex_list[j].offLat) ;
		}

		if(s_vertex_list != NULL) free(s_vertex_list) ;
		s_vertex_list = NULL ;
	}

	if(s_link_list != NULL) free(s_link_list) ;
	s_link_list = NULL ;

error_exit :
	return ret;
}

///[TEST5]///////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_test5( JNIEnv* env, jobject thiz )
{
	int ret = 0 ;

	int i = 0 ;
	int map_id = 1123 ;
	int link_cnt = 0 ;
	SLINK *s_link_list = NULL ;
	SLINK temp ;

//	ret = i_smr_read_link_count(map_id, &link_cnt) ;
	ret = sMR_read_link_count(map_id, &link_cnt) ;
	if(ret) goto error_exit ;
	D("link_cnt[%d]\n", link_cnt) ;

	if((s_link_list = (SLINK *) malloc (link_cnt * sizeof(SLINK))) == NULL) {
		D("ERR: malloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(s_link_list, 0, link_cnt * sizeof(SLINK)) ;

//	i_smr_read_link_list(map_id, link_cnt, s_link_list) ;
	sMR_read_link_list(map_id, link_cnt, s_link_list) ;

	for(i = 0 ; i < link_cnt ; i++) {
		D("map_id[%d] link_id[%d] ==> startId[%d]endId[%d]length[%d]numOfVertex[%d]vertexIndex[%d]\n",
				map_id, i,
				s_link_list[i]._startId, s_link_list[i]._endId,
				s_link_list[i]._length, s_link_list[i]._numOfVertex, s_link_list[i]._vertexIndex) ;
		D("    laneNum(%d)drive(%d)divide(%d)linkType(%d)linkAttr(%d)roadType(%d)\n",
				s_link_list[i].laneNum(), s_link_list[i].drive(), s_link_list[i].divide(),
				s_link_list[i].linkType(), s_link_list[i].linkAttr(), s_link_list[i].roadType()) ;
	}

	if(s_link_list != NULL) free(s_link_list) ;
	s_link_list = NULL ;

	D("=======================================================================\n") ;
	D("sizeof(SLINK)[%d]\n", sizeof(SLINK)) ;

	for(i = 0 ; i < link_cnt ; i++) {
		memset(&temp, 0, sizeof(SLINK)) ;

	//	i_smr_read_link(5, i, &temp) ;
		sMR_read_link(map_id, i, &temp) ;

		D("map_id[%d] link_id[%d] ==> startId[%d]endId[%d]length[%d]numOfVertex[%d]vertexIndex[%d]\n",
				map_id, i,
				temp._startId, temp._endId,
				temp._length, temp._numOfVertex, temp._vertexIndex) ;
		D("    laneNum(%d)drive(%d)divide(%d)linkType(%d)linkAttr(%d)roadType(%d)\n",
				temp.laneNum(), temp.drive(), temp.divide(),
				temp.linkType(), temp.linkAttr(), temp.roadType()) ;

	}

error_exit :
	return ret;
}

///[TEST4]///////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_test4( JNIEnv* env, jobject thiz )
{
	int ret = 0 ;

	int i = 0 ;
	int node_cnt = 0 ;

	int map_id = 1052 ;

	SNODE temp ;
	SADJNODE temp_adjnode ;

//	ret = i_smr_read_node_count(5, &node_cnt) ;
	ret = sMR_read_node_count(map_id, &node_cnt) ;
	if(ret) goto error_exit ;

	for(i = 0 ; i < node_cnt ; i++) {
		memset(&temp, 0, sizeof(SNODE)) ;

	//	i_smr_read_node(map_id, i, &temp) ;
		sMR_read_node(map_id, i, &temp) ;
		D("[%d]==>offLon[%d] offLat[%d]\n",
				i, temp.offLon, temp.offLat) ;
		D("    isAdjNode(%d)isUpperMatchedNode(%d)getnumofJCLink(%d)getExNodeDS(%d)\n",
				temp.isAdjNode(), temp.isUpperMatchedNode(),
				temp.getnumofJCLink(), temp.getExNodeDS()) ;

		if(temp.isAdjNode() == 1) {     //adjnode¿¡ ÇÑÇØ¼­ Ãâ·Â

			memset(&temp_adjnode, 0, sizeof(SADJNODE)) ;
		//	i_smr_read_adjnode(map_id, &temp, &temp_adjnode) ;
			sMR_read_adjnode(map_id, &temp, &temp_adjnode) ;

			D("    adjMapId[%d]adjNodeId[%d]adjLinkId[%d]\n",
					temp_adjnode.adjMapId, temp_adjnode.adjNodeId, temp_adjnode.adjLinkId) ;
		}
	}
	//Another method
	D("===================================================================\n") ;
	for(i = 0 ; i < node_cnt ; i++) {
		memset(&temp_adjnode, 0, sizeof(SADJNODE)) ;
	//	ret = i_smr_read_adjnode(map_id, i, &temp_adjnode) ;
		ret = sMR_read_adjnode(map_id, i, &temp_adjnode) ;
		if(ret == 0) {
			D("map_id[%d]node_id[%d]\n", map_id, i) ;
			D("    adjMapId[%d]adjNodeId[%d]adjLinkId[%d]\n",
					temp_adjnode.adjMapId, temp_adjnode.adjNodeId, temp_adjnode.adjLinkId) ;
		}
	}

error_exit:
	return ret ;
}

///[TEST3]///////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_test3( JNIEnv* env, jobject thiz )
{
	int ret = 0 ;

	int i = 0 ;
	int j = 0 ;
	int node_cnt = 0 ;
	int cross_cnt = 0 ;

	int map_id = 2500 ;

	SNODE temp ;
	SCROSSNODE temp_crossnode ;

//	ret = i_smr_read_node_count(5, &node_cnt) ;
	ret = sMR_read_node_count(map_id, &node_cnt) ;
	if(ret) goto error_exit ;

	for(i = 0 ; i < node_cnt ; i++) {
		memset(&temp, 0, sizeof(SNODE)) ;

	//	i_smr_read_node(map_id, i, &temp) ;
		sMR_read_node(map_id, i, &temp) ;
		D("[%d]==>offLon[%d] offLat[%d]\n",
				i, temp.offLon, temp.offLat) ;
		D("    isAdjNode(%d)isUpperMatchedNode(%d)getnumofJCLink(%d)getExNodeDS(%d)\n",
				temp.isAdjNode(), temp.isUpperMatchedNode(),
				temp.getnumofJCLink(), temp.getExNodeDS()) ;

		cross_cnt = temp.getnumofJCLink() ;
		if(temp.isAdjNode() == 0) {     //crossnode¿¡ ÇÑÇØ¼­ Ãâ·Â

			memset(&temp_crossnode, 0, sizeof(SCROSSNODE)) ;
		//	i_smr_read_crossnode(map_id, &temp, &temp_crossnode) ;
			sMR_read_crossnode(map_id, &temp, &temp_crossnode) ;

			for(j = 0 ; j < cross_cnt ; j++) {
				D("        conlink[%d] temp_conpasscode[%d]\n",
						temp_crossnode.conlink[j], temp_crossnode.conpasscode[j]) ;
			}
		}
	}
	//Another method
	D("===================================================================\n") ;
	for(i = 0 ; i < node_cnt ; i++) {
		memset(&temp_crossnode, 0, sizeof(SCROSSNODE)) ;
		cross_cnt = 0 ;
	//	ret = i_smr_read_crossnode(map_id, i, &temp_crossnode, &cross_cnt) ;
		ret = sMR_read_crossnode(map_id, i, &temp_crossnode, &cross_cnt) ;
		if(ret == 0) {
			D("map_id[%d]\n", map_id) ;
			for(j = 0 ; j < cross_cnt ; j++) {
				D("    node_id[%d] ==> conlink[%d] temp_conpasscode[%d]\n",
						i, temp_crossnode.conlink[j], temp_crossnode.conpasscode[j]) ;
			}
		}
	}
error_exit:
	return ret ;
}

///[TEST2]///////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL
Java_com_thinkware_smr_smrtestbed_jcSMR_test2( JNIEnv* env, jobject thiz, jint map_id )
{
	int ret = 0 ;

	int i = 0 ;
	//int map_id = 5 ;
	int node_cnt = 0 ;
	SNODE *s_node_list = NULL ;
	SNODE temp ;

//	ret = i_smr_read_node_count(map_id, &node_cnt) ;
	ret = sMR_read_node_count(map_id, &node_cnt) ;
	if(ret) goto error_exit ;
	D("node_cnt[%d]\n", node_cnt) ;

	if((s_node_list = (SNODE *) malloc (node_cnt * sizeof(SNODE))) == NULL) {
		D("ERR: malloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(s_node_list, 0, node_cnt * sizeof(SNODE)) ;

//	i_smr_read_node_list(map_id, node_cnt, s_node_list) ;
	sMR_read_node_list(map_id, node_cnt, s_node_list) ;

	for(i = 0 ; i < node_cnt ; i++) {
		D("[%d]==>offLon[%d] offLat[%d]\n",
				i, s_node_list[i].offLon, s_node_list[i].offLat) ;
		D("    isAdjNode(%d)isUpperMatchedNode(%d)getnumofJCLink(%d)getExNodeDS(%d)\n",
				s_node_list[i].isAdjNode(), s_node_list[i].isUpperMatchedNode(),
				s_node_list[i].getnumofJCLink(), s_node_list[i].getExNodeDS()) ;
	}

	if(s_node_list != NULL) free(s_node_list) ;
	s_node_list = NULL ;

	D("=======================================================================\n") ;
	D("sizeof(SNODE)[%d]\n", sizeof(SNODE)) ;

	for(i = 0 ; i < node_cnt ; i++) {
		memset(&temp, 0, sizeof(SNODE)) ;

	//	i_smr_read_node(5, i, &temp) ;
		sMR_read_node(map_id, i, &temp) ;
		D("[%d]==>offLon[%d] offLat[%d]\n",
				i, temp.offLon, temp.offLat) ;
		D("    isAdjNode(%d)isUpperMatchedNode(%d)getnumofJCLink(%d)getExNodeDS(%d)\n",
				temp.isAdjNode(), temp.isUpperMatchedNode(),
				temp.getnumofJCLink(), temp.getExNodeDS()) ;

	}

error_exit :
	return ret;
}

////////////////////////////////////////////////////////////////////////////

//#if defined(__cplusplus)                 // export for C++ //
//}
//#endif


