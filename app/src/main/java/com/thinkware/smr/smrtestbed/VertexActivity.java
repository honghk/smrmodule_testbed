package com.thinkware.smr.smrtestbed;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class VertexActivity extends AppCompatActivity {

    private SmrControl m_smrControl = null;
    private int m_vertexIndex = 0;
    private Snode[] m_snodes = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertex_textver);

        TextView textViewVertex = (TextView) findViewById(R.id.textViewVertex);
        ListView listViewVertexList = (ListView) findViewById(R.id.listViewVertexList);
        ArrayList<Slink.SVertex> listItem = new ArrayList<Slink.SVertex>();

        Intent intent = getIntent();
        int mapId = intent.getIntExtra("map_id",-1);
        int linkId = intent.getIntExtra("link_id",-1);
        m_vertexIndex = intent.getShortExtra("vertex_index", (short)-1);

        m_smrControl = new SmrControl();

        SrankMap srankMap =  m_smrControl.getSrankMap(mapId);
        Slink[] slinks = m_smrControl.getSlink(mapId);
        m_snodes = m_smrControl.getNode(mapId);

        textViewVertex.setText("map id : " + mapId + " / link id : " + linkId + " / vertex cnt : " + slinks[linkId]._numOfVertex);

        slinks[linkId].sVertex[0].lat = slinks[linkId].sVertex[0].offLat + m_snodes[slinks[linkId]._startId].lat;
        slinks[linkId].sVertex[0].lon = slinks[linkId].sVertex[0].offLon + m_snodes[slinks[linkId]._startId].lon;

        int i = 0;
        for (i = 0; i < slinks[linkId]._numOfVertex; i++){

            if(i != 0){
                slinks[linkId].sVertex[i].lat = slinks[linkId].sVertex[i].offLat + slinks[linkId].sVertex[i-1].lat;
                slinks[linkId].sVertex[i].lon = slinks[linkId].sVertex[i].offLon + slinks[linkId].sVertex[i-1].lon;
            }

            listItem.add(slinks[linkId].sVertex[i]);
        }

        ListAdaptervertex listAdaptervertex = new ListAdaptervertex(this, R.layout.listview_vertexitems_textver, listItem);
        listViewVertexList.setAdapter(listAdaptervertex);

    }

    class ListAdaptervertex extends BaseAdapter {

        Context m_context = null;
        LayoutInflater m_inflater = null;
        ArrayList<Slink.SVertex> m_arrSlinkVertex = null;
        int m_layout;

        public ListAdaptervertex(Context context, int layout_id, ArrayList<Slink.SVertex> arrSlinkVertex){
            m_context = context;
            m_layout = layout_id;
            m_arrSlinkVertex = arrSlinkVertex;

            m_inflater = LayoutInflater.from(m_context);
        }

        @Override
        public int getCount(){
            return m_arrSlinkVertex.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            if(convertView == null){
                convertView = m_inflater.inflate(m_layout, parent, false);
            }
            //convertView.setMinimumHeight(500);

            View vetexlistrow = convertView.findViewById(R.id.vertextitemrow);

            TextView Row1_Col1_Title = (TextView) vetexlistrow.findViewById(R.id.title_col1);
            TextView Row1_Col2_Title = (TextView) vetexlistrow.findViewById(R.id.title_col2);
            TextView Row1_Col3_Title = (TextView) vetexlistrow.findViewById(R.id.title_col3);

            TextView Row1_Col1_Content = (TextView) vetexlistrow.findViewById(R.id.content_col1);
            TextView Row1_Col2_Content = (TextView) vetexlistrow.findViewById(R.id.content_col2);
            TextView Row1_Col3_Content = (TextView) vetexlistrow.findViewById(R.id.content_col3);

            Row1_Col1_Title.setText("Vertex id");
            Row1_Col1_Content.setText((m_vertexIndex+position)+"");

            Row1_Col2_Title.setText("Longitude");
            Row1_Col2_Content.setText(m_arrSlinkVertex.get(position).lat+"");

            Row1_Col3_Title.setText("Latitude");
            Row1_Col3_Content.setText(m_arrSlinkVertex.get(position).lon+"");

//            TextView m_textViewIndex = (TextView) convertView.findViewById(R.id.textViewVertexIndex);
//            m_textViewIndex.setText("Vertex id\n" + (m_vertexIndex+position));
//
//            TextView m_textViewStartId = (TextView) convertView.findViewById(R.id.textViewVertexOffLon);
//            m_textViewStartId.setText("Longitude\n" + m_arrSlinkVertex.get(position).lat);
//
//            TextView m_textViewEndId = (TextView) convertView.findViewById(R.id.textViewVertexOffLan);
//            m_textViewEndId.setText("Latitude\n" + m_arrSlinkVertex.get(position).lon);


            return convertView;
        }

        @Override
        public Object getItem(int position){
            return m_arrSlinkVertex.get(position).offLon ;
        }

        @Override
        public long getItemId(int postion){
            return postion;
        }

    }
}
