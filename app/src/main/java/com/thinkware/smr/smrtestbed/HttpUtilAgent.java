package com.thinkware.smr.smrtestbed;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

/**
 * Created by honghk on 2016. 12. 8..
 */

public class HttpUtilAgent {

    private static final String TAG = "HttpUtilAgent";

    //에러코드/////////////////////////////////////////////
    private final static int SUCCESS = 0;
    private final static int ERR_TCP = 1000;
    private final static int TIME_OUT = 5000;
    ////////////////////////////////////////////////////////

    private String m_str_result_clause_agree = null;
    private String m_str_map_download_info = null;
    private Context m_context = null;

    public void setContext(Context context){ // 생성자
        this.m_context = context;
    }

    public String getPrivatekey(){
        JSONObject jsonObject = null;
        String private_key = null;

        try {
            jsonObject = new JSONObject(m_str_result_clause_agree);
            private_key = jsonObject.getString("private_key");

        }catch(JSONException je){
            Log.e(TAG,"getPrivatekey::JSONException : " + je);
        }
        return private_key;
    }

    public String getCompany(){
        JSONObject jsonObject = null;
        String company = null;

        try{
            jsonObject = new JSONObject(m_str_result_clause_agree);
            company = jsonObject.getString("company");

        }catch(JSONException je){
            Log.e(TAG,"getCompany::JSONException : " + je);
        }
        return company;
    }

    public String getSmrUrl(){
        JSONObject jsonObject = null;
        String smr_url = null;

        try{
            jsonObject = new JSONObject(m_str_map_download_info);
            smr_url = jsonObject.getString("smr_url");

        }catch(JSONException je){
            Log.e(TAG,"getSmrUrl::JSONException : " + je);
        }catch (Exception e){
            Log.e(TAG,"getSmrUrl::Exception : " + e);
        }
        return smr_url;
    }

    public int connectClauseAgree()
    {
        URL url = null;
        URLConnection urlConnection = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader bufferedReader = null;
        StringBuffer stringBuffer = null;

        String param = null;
        String temp = null;

        String str_url = ServerSelector.getInstance().getURLPrefix()+ServerSelector.APP_CLAUSEAGREE;

        int ret = SUCCESS;

        try{
            url = new URL(str_url);
            urlConnection = url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setConnectTimeout(TIME_OUT);
            urlConnection.setReadTimeout(TIME_OUT);

            urlConnection.setRequestProperty("method", "POST");
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            urlConnection.setRequestProperty("mdn", getMDN());

            urlConnection.setRequestProperty("os_type", "Android");
            urlConnection.setRequestProperty("os_ver", Build.VERSION.RELEASE);
            urlConnection.setRequestProperty("source", "Android");
            urlConnection.setRequestProperty("device", Build.MODEL);
            urlConnection.setRequestProperty("app_vercode", "");
            urlConnection.setRequestProperty("company", "");
            urlConnection.setRequestProperty("app_ver", "");
            urlConnection.setRequestProperty("private_key", "");
            urlConnection.setRequestProperty("auth_key", "");

            param = makePramClauseAgree().toString();

            outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream()); // 바이트 스트림을 문자 스트림으로 쓰겠다
            outputStreamWriter.write(param);
            outputStreamWriter.flush();

            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8")); //버퍼 단위로
            stringBuffer = new StringBuffer();
            temp = "";

            while((temp = bufferedReader.readLine()) != null){
                stringBuffer.append(temp);
            }
            m_str_result_clause_agree = stringBuffer.toString();

            if(outputStreamWriter != null){
                outputStreamWriter.close();
                outputStreamWriter = null;
            }

            if(bufferedReader != null){
                bufferedReader.close();
                bufferedReader = null;
            }

            if(stringBuffer != null){
                stringBuffer = null;
            }

            if(urlConnection != null){
                urlConnection = null;
            }

            if(url != null){
                url = null;
            }

        }catch(Exception exc){
            ret = ERR_TCP;
            Log.e(TAG, "connectClauseAgree::Exception : " + exc);

            try {
                if(outputStreamWriter != null){
                    outputStreamWriter.close();
                    outputStreamWriter = null;
                }

                if(bufferedReader != null){
                    bufferedReader.close();
                    bufferedReader = null;
                }

                if(stringBuffer != null){
                    stringBuffer = null;
                }

                if(urlConnection != null){
                    urlConnection = null;
                }

                if(url != null){
                    url = null;
                }
            }catch(IOException ioexc) {
                Log.e(TAG, "ERR : connectSmrData::Exeption(close) : " + ioexc);
            }
        }

        return ret;

    }

    public int connectMapDownloadInfo(){

        URL url = null;
        URLConnection urlConnection = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedReader bufferedReader = null;
        StringBuffer stringBuffer = null;

        String str_url = ServerSelector.getInstance().getURLPrefix() + ServerSelector.APP_DOWNLOAD_INFO;
        String param = null;
        String temp = null;
        String content_encoding = null;

        int ret = SUCCESS;

        try{
            url = new URL(str_url);
            urlConnection = url.openConnection();

            //HttpURLConnection.setFollowRedirects(true);// set redirects 웹페이지를 하나 이상의 URL주소로 만들어 주는 기술 넘겨받은 URL을 웹 브라우저가 열려고 하면 다른 URL의 문서가 열리게 된다
            //httpURLConnection.setInstanceFollowRedirects(true);// 302같은 리다이렉션을 자동으로 하지 않도록 지정하는 옵션
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setUseCaches(false);

            urlConnection.setConnectTimeout(TIME_OUT);
            urlConnection.setReadTimeout(TIME_OUT);
            urlConnection.setRequestProperty("Accept-Encoding", "gzip");// gzip 형태 압축

            urlConnection.setRequestProperty("method", "POST");
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            urlConnection.setRequestProperty("mdn", getMDN());

            urlConnection.setRequestProperty("os_type", "Android");
            urlConnection.setRequestProperty("os_ver", Build.VERSION.RELEASE);
            urlConnection.setRequestProperty("source", "Android");
            urlConnection.setRequestProperty("device", Build.MODEL);
            urlConnection.setRequestProperty("private_key", getPrivatekey());
            urlConnection.setRequestProperty("company", getCompany());
            urlConnection.setRequestProperty("app_vercode", "");
            urlConnection.setRequestProperty("app_ver", "");
            urlConnection.setRequestProperty("auth_key", "");

            param = makePramMapDownloadInfo().toString();

            outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(param);
            outputStreamWriter.flush();

            content_encoding = urlConnection.getContentEncoding();

            if((content_encoding != null )&& content_encoding.equalsIgnoreCase("gzip")) { // gzip일 경우 GZIPInputStream으로 InputStreamReader

                bufferedReader = new BufferedReader(new InputStreamReader(new GZIPInputStream(urlConnection.getInputStream()), "UTF-8"));
                // GZIPInputStream을 이용하여 byte배열 압축 해제 InputStreamReader 바이트 스트림에서 문자 스트림으로 변환 BufferedReader 한줄씩 읽어오기 위해

            }
            else{
                bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8")); // gzip이 아닐 떄
            }

            stringBuffer = new StringBuffer();
            temp = "";

            while((temp = bufferedReader.readLine()) != null){
                stringBuffer.append(temp);
            }

            m_str_map_download_info = stringBuffer.toString();

            if(outputStreamWriter != null){
                outputStreamWriter.close();
                outputStreamWriter = null;
            }

            if(bufferedReader != null){
                bufferedReader.close();
                bufferedReader = null;
            }

            if(stringBuffer != null){
                stringBuffer = null;
            }

            if(urlConnection != null){
                urlConnection = null;
            }

            if(url != null){
                url = null;
            }

        }catch(Exception exc){
            ret = ERR_TCP;
            Log.e(TAG, "connectClauseAgree::Exception : " + exc);

            try {
                if(outputStreamWriter != null){
                    outputStreamWriter.close();
                    outputStreamWriter = null;
                }

                if(bufferedReader != null){
                    bufferedReader.close();
                    bufferedReader = null;
                }

                if(stringBuffer != null){
                    stringBuffer = null;
                }

                if(urlConnection != null){
                    urlConnection = null;
                }

                if(url != null){
                    url = null;
                }
            }catch(IOException ioexc){
                Log.e(TAG, "ERR : connectSmrData::Exeption(close) : " + ioexc);
            }
        }

        return ret;
    }

    public String getMDN(){

        String mdn = null; // null or "" 확인 필요
        try{

            TelephonyManager telephonyManager = (TelephonyManager) m_context.getSystemService(Context.TELEPHONY_SERVICE);
            mdn = telephonyManager.getLine1Number();

        }catch(NullPointerException nullpointer){
            Log.e(TAG,"getMDN::NullPointerException : " + nullpointer);
            return "";
        }catch(Exception e){
            Log.e(TAG,"getMDN::Exception : " + e);
            return "";
        }

        return mdn;
    }

    public String getIMEI(){

        String imei = null;

        try{
            TelephonyManager telephonyManager = (TelephonyManager) m_context.getSystemService(Context.TELEPHONY_SERVICE);
            imei = telephonyManager.getDeviceId();
        }catch(NullPointerException nullpointer){
            Log.e(TAG, "getIMEI::NullPointerException : " + nullpointer);
            return "";
        }
        return imei;
    }

    public JSONObject makePramClauseAgree(){

        String imei = getIMEI();
        JSONObject jsonObject = null;
        int ad_status = 0;

        try{
            jsonObject = new JSONObject();
            jsonObject.put("ad_status",ad_status);
            jsonObject.put("device_id",imei);
//            json.put("ip","");
        }catch(JSONException je){
            Log.e(TAG,"makePramClauseAgree::JSONException : " + je);
        }

        return jsonObject;
    }

    public JSONObject makePramMapDownloadInfo(){

        JSONObject jsonObject = null;
        String smr_ver = "";
        String smdc_ver = "";

        try{
            jsonObject = new JSONObject();
            jsonObject.put("smr_ver",smr_ver);
            jsonObject.put("smdc_ver",smdc_ver);

        }catch(JSONException je){
            Log.e(TAG,"makePramMapDownloadInfo::JSONException : " + je);
        }

        return jsonObject;
    }

}
