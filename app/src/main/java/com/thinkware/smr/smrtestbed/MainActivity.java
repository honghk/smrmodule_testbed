package com.thinkware.smr.smrtestbed;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";

    //[에러코드]/////////////////////////////////////////////////////////////////////////////////////////////

    private final static int SUCCESS = 0;
    private final static int REQUEST_READ_PHONE_STATE_PERMISSION = 225;
    private final static int REQUEST_READ_EXTERNAL_STORAGE_PERMISSION = 226;
    private final static int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 227;
    private final static String ROOT_DIR = Environment.getExternalStorageDirectory().getAbsolutePath().toString() + File.separator;

    //[UI]///////////////////////////////////////////////////////////////////////////////////////////////

    private EditText m_editText_mapId = null;
    private EditText m_editText_linkId = null;
    private EditText m_editText_SetDir = null;

    private Button m_button_SmapHeader = null;
    private Button m_button_MapId = null;
    private Button m_button_LinkList = null;
    private Button m_button_NodeList = null;
    private Button m_button_VertexList = null;
    private Button m_button_MapNode = null;
    private Button m_button_SetDir = null;
    private RelativeLayout m_relativeLayoutMain = null;
    private TextView m_textViewInfo = null;
    private TextView m_textViewInfo2 = null;
    private TextView m_textViewInfo3 = null;
    private TextView m_textViewMapId = null;
    private TextView m_textViewFilePath = null;
    private Button m_button_SrankMap = null;
    private TextView m_textViewTitle = null;

    TextView m_textViewquicksearchtitle1 = null;
    TextView m_textViewquicksearchtitle2 = null;
    TextView m_textViewquicksearchtitle3 = null;
    TextView m_textViewquicksearchtitle4 = null;
    TextView m_textViewquicksearchtitle5 = null;
    TextView m_textViewquicksearchtitle6 = null;
    TextView m_textViewquicksearchtitle7 = null;

    TextView m_textViewquicksearchcontent1 = null;
    TextView m_textViewquicksearchcontent2 = null;
    TextView m_textViewquicksearchcontent3 = null;
    TextView m_textViewquicksearchcontent4 = null;
    TextView m_textViewquicksearchcontent5 = null;
    TextView m_textViewquicksearchcontent6 = null;
    TextView m_textViewquicksearchcontent7 = null;

    TextView m_textViewquicksearchtitle1_info2 = null;
    TextView m_textViewquicksearchtitle2_info2 = null;
    TextView m_textViewquicksearchtitle3_info2 = null;
    TextView m_textViewquicksearchtitle4_info2 = null;
    TextView m_textViewquicksearchtitle5_info2 = null;
    TextView m_textViewquicksearchtitle6_info2 = null;
    TextView m_textViewquicksearchtitle7_info2 = null;

    TextView m_textViewquicksearchcontent1_info2 = null;
    TextView m_textViewquicksearchcontent2_info2 = null;
    TextView m_textViewquicksearchcontent3_info2 = null;
    TextView m_textViewquicksearchcontent4_info2 = null;
    TextView m_textViewquicksearchcontent5_info2 = null;
    TextView m_textViewquicksearchcontent6_info2 = null;
    TextView m_textViewquicksearchcontent7_info2 = null;

    TextView m_textViewquicksearchtitle1_info3 = null;
    TextView m_textViewquicksearchtitle2_info3 = null;
    TextView m_textViewquicksearchtitle3_info3 = null;
    TextView m_textViewquicksearchtitle4_info3 = null;
    TextView m_textViewquicksearchtitle5_info3 = null;
    TextView m_textViewquicksearchtitle6_info3 = null;
    TextView m_textViewquicksearchtitle7_info3 = null;

    TextView m_textViewquicksearchcontent1_info3 = null;
    TextView m_textViewquicksearchcontent2_info3 = null;
    TextView m_textViewquicksearchcontent3_info3 = null;
    TextView m_textViewquicksearchcontent4_info3 = null;
    TextView m_textViewquicksearchcontent5_info3 = null;
    TextView m_textViewquicksearchcontent6_info3 = null;
    TextView m_textViewquicksearchcontent7_info3 = null;

    //    private Button m_button_SrankManager = null;

    ///////////////////////////////////////////////////////////////////////////////////////////////////

    private SmrControl m_smrControl = null;
    private HttpUtilAgent m_httpUtilAgent = null ;
    private SmrAsynctask m_SmrAsynctask = null;
    private Thread m_smrThread = null;
    private SrankMap m_srankMap = null;
    private InputMethodManager m_inputMethodManager = null;
    private int m_mapId = -1;
    private int m_linkId = -1;
    private boolean m_log_flag = true;
    private String m_dir = null;
    private String m_newDir = null;

    private int m_startNode = -1;
    private int m_endNode = -1;
    private short m_vertextIndex = -1;
    private int m_numberOfVertex = -1;

    private SharedPreferences.Editor m_editor = null;
    private SharedPreferences m_sharedPreferences = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_textver);

        Log.v("hongTest", "onCreate");//branch test 1

        initKeyboardControl(); // Keyboard 제어
        ////////////////////////////////////////////////////////////////////////////////////


        m_sharedPreferences = getSharedPreferences("SmrModule", MODE_PRIVATE);
        m_dir = m_sharedPreferences.getString("DirPath", null);

        if(m_dir == null){
            m_dir = "smrData" + File.separator + "mrdata";

            m_editor = m_sharedPreferences.edit();
            m_editor.putString("DirPath", m_dir);
            m_editor.commit();

        }

        if (m_httpUtilAgent == null) {
            m_httpUtilAgent = new HttpUtilAgent();
        }

        m_httpUtilAgent.setContext(getApplicationContext());

        if (m_smrControl == null) {
            m_smrControl = new SmrControl(this);
        }

        if(m_SmrAsynctask == null){
            m_SmrAsynctask = new SmrAsynctask();
        }

        ////////////////////////////////////////////////////////////////////////////////////

        if (m_relativeLayoutMain == null){
            m_relativeLayoutMain = (RelativeLayout) findViewById(R.id.activity_main);
            m_relativeLayoutMain.setOnClickListener(this);
        }
//
        if(m_editText_mapId == null){
            m_editText_mapId = (EditText) findViewById(R.id.editText_mapId);
            m_editText_mapId.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(6) }); //입력숫자개수 제한
        }
//
        if(m_editText_linkId == null){
            m_editText_linkId = (EditText) findViewById(R.id.editText_linkId);
            m_editText_linkId.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(6) }); //입력숫자개수 제한
        }

        if(m_editText_SetDir == null){
            m_editText_SetDir = (EditText) findViewById(R.id.editText_setDir);
            m_editText_SetDir.setFilters(new InputFilter[]{filterAlphaNum});
        }

        if(m_button_MapId == null){
            m_button_MapId = (Button) findViewById(R.id.button_search);
            m_button_MapId.setOnClickListener(this);
        }
//
        if(m_button_SmapHeader == null) {
            m_button_SmapHeader = (Button) findViewById(R.id.button_SmapHeader);
            m_button_SmapHeader.setOnClickListener(this);
        }

        if(m_button_SrankMap == null) {
            m_button_SrankMap = (Button) findViewById(R.id.button_SrankMap);
            m_button_SrankMap.setOnClickListener(this);
        }

        if(m_button_LinkList == null) {
            m_button_LinkList = (Button) findViewById(R.id.button_linklist);
            m_button_LinkList.setOnClickListener(this);
        }

        if(m_button_NodeList == null) {
            m_button_NodeList = (Button) findViewById(R.id.button_nodelist);
            m_button_NodeList.setOnClickListener(this);
        }

        if(m_button_VertexList == null) {
            m_button_VertexList = (Button) findViewById(R.id.button_vertexlist);
            m_button_VertexList.setOnClickListener(this);
        }

        if(m_button_MapNode == null) {
            m_button_MapNode = (Button) findViewById(R.id.button_mapNode);
            m_button_MapNode.setOnClickListener(this);
        }

        if(m_button_SetDir == null) {
            m_button_SetDir = (Button) findViewById(R.id.button_setDir);
            m_button_SetDir.setOnClickListener(this);
        }

        if(m_textViewInfo2 == null){
            m_textViewInfo2 = (TextView) findViewById(R.id.textViewInfo2);
        }
//        ////////////////////////////////////////////////////////////////////////////////////

        if(m_textViewInfo == null){
            m_textViewInfo = (TextView) findViewById(R.id.textViewInfo1);
        }

        View include7row_nopad = findViewById(R.id.include_7row_nopad_1);
        View include7row_nopad_2 = findViewById(R.id.include_7row_nopad_2);
        View include7row_nopad_3 = findViewById(R.id.include_7row_nopad_3);

        setTextViewId(include7row_nopad, include7row_nopad_2, include7row_nopad_3);

        m_textViewquicksearchtitle1.setText("map id : ");
        m_textViewquicksearchtitle2.setText("메시 내 노드 개수 : ");
        m_textViewquicksearchtitle3.setText("메시 내 링크 개수 : ");
        m_textViewquicksearchtitle4.setText("");

        m_textViewquicksearchtitle5.setVisibility(View.GONE);
        m_textViewquicksearchtitle6.setVisibility(View.GONE);
        m_textViewquicksearchtitle7.setVisibility(View.GONE);
        m_textViewquicksearchcontent5.setVisibility(View.GONE);
        m_textViewquicksearchcontent6.setVisibility(View.GONE);
        m_textViewquicksearchcontent7.setVisibility(View.GONE);

        m_textViewquicksearchtitle1_info2.setText("시작노드 id : ");
        m_textViewquicksearchtitle2_info2.setText("종료노드 id : ");
        m_textViewquicksearchtitle3_info2.setText("링크 길이 : ");
        m_textViewquicksearchtitle4_info2.setText("보간점 개수 : ");
        m_textViewquicksearchtitle5_info2.setText("차선수 : ");

        m_textViewquicksearchtitle6_info2.setVisibility(View.GONE);
        m_textViewquicksearchtitle7_info2.setVisibility(View.GONE);
        m_textViewquicksearchcontent6_info2.setVisibility(View.GONE);
        m_textViewquicksearchcontent7_info2.setVisibility(View.GONE);

        m_textViewquicksearchtitle1_info3.setText("보간점 인덱스 :");
        m_textViewquicksearchtitle2_info3.setText("링크 통행코드 : ");
        m_textViewquicksearchtitle3_info3.setText("본선 분리 : ");
        m_textViewquicksearchtitle4_info3.setText("링크 종별 : ");
        m_textViewquicksearchtitle5_info3.setText("링크 속성 : ");
        m_textViewquicksearchtitle6_info3.setText("도로 종별 : ");

        m_textViewquicksearchtitle7_info3.setVisibility(View.GONE);
        m_textViewquicksearchcontent7_info3.setVisibility(View.GONE);

        if(m_textViewFilePath == null){
            m_textViewFilePath = (TextView) findViewById(R.id.textView_FilePath);
        }

        m_textViewFilePath.setText("File Path : " + ROOT_DIR + m_dir);

        checkPermission(); // excute

    }//

    private class SmrAsynctask extends AsyncTask<Void, String , Integer>{

        protected Integer doInBackground(Void... vd){

            int ret = SUCCESS;

            int i;
            for(i = 0; i < 3; i++){
                ret = m_httpUtilAgent.connectClauseAgree();
                if(ret==SUCCESS) break;
            }

            if(ret != SUCCESS){
                logCall("서버 통신 에러(connectClauseAgree) ret = " + ret);
                return ret;
            }

            ret = SUCCESS;

            for(i = 0; i < 3; i++){
                ret = m_httpUtilAgent.connectMapDownloadInfo();
            }

            if(ret!=SUCCESS){
                logCall("서버 통신 에러(connectMapDownloadInfo) ret = " + ret);
                return ret;
            }

            //ret = m_smrControl.startSmr(m_httpUtilAgent.getSmrUrl(), getApplicationContext().getFilesDir().toString());
            ret = m_smrControl.startSmr(m_httpUtilAgent.getSmrUrl(), ROOT_DIR + m_dir);

            return ret;
        }

        protected void onPostExecute(Integer result){

            if(result != SUCCESS){
                Toast.makeText(getApplicationContext(), "에러코드 : " + result, Toast.LENGTH_SHORT).show();
            }

            SrankManager srankManager = m_smrControl.getSrankManager();
//
            if(srankManager.numOfMesh == 0){
                Toast.makeText(getApplicationContext(), "초기데이터를 가져오는데 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }else if(result==SUCCESS){
                Toast.makeText(getApplicationContext(), "초기데이터를 가져오는데 성공하였습니다.(서버 통신 정상)", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getApplicationContext(), "초기데이터를 가져오는데 성공하였습니다.(서버 통신 비정상)", Toast.LENGTH_SHORT).show();
            }

            m_textViewMapId = (TextView) findViewById(R.id.textView_mapId);
            m_textViewMapId.setText("map id :\n(0~" + (srankManager.numOfMesh-1) +")");

        }
    }

    //[Log Function]///////////////////////////////////////////////////////////////////////////////////////////

    public void logCall(String str_log){

        if(m_log_flag==true){
            LogDialogFragment logDialogFragment = new LogDialogFragment();
            logDialogFragment.setActivity(this);
            logDialogFragment.LogSet(str_log);
            FragmentManager fragmentManager = getSupportFragmentManager();
            logDialogFragment.show(fragmentManager, "log");
            m_log_flag = false;
        }

    }

    public void setLogFlag(boolean flag){
        m_log_flag = flag;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //[Click Function]///////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onClick(View v) {

        if(v == null) {
            return;
        }else if(v == m_button_MapId){

            int ret = SUCCESS;
            hideKeyboard();

            if(m_editText_mapId.getText() == null || m_editText_mapId.getText().length() == 0) {
                Toast.makeText(getApplicationContext(), "map id 입력 후 클릭해주세요.", Toast.LENGTH_SHORT).show();
                return;
            }else{

                m_mapId = Integer.parseInt(m_editText_mapId.getText().toString());
                m_srankMap = m_smrControl.getSrankMap(m_mapId);

                ret = m_smrControl.sidxInsert(m_mapId);

                m_smrThread = new Thread(m_smrControl.m_smrTread);
                m_smrThread.setName("SMR Thread");
                m_smrThread.start();

                try{
                    m_smrThread.join();
                }catch(InterruptedException interexc){
                    Log.e(TAG, "InterruptedException : " + interexc);
                }

                if(ret==SUCCESS){
                    Toast.makeText(getApplicationContext(), "map id : " + m_mapId + "의 데이터를 다운르드 하였습니다.", Toast.LENGTH_SHORT).show();

                    m_textViewquicksearchcontent1.setText(m_mapId+"");
                    m_textViewquicksearchcontent2.setText(m_srankMap.numOfNode+"");
                    m_textViewquicksearchcontent3.setText(m_srankMap.numOfLink+"");
                    m_textViewquicksearchcontent4.setText("");


                }else{
                    Toast.makeText(getApplicationContext(), "map id : " + m_mapId + "의 데이터를 다운르드 실패[에러코드 : "+ ret +"]", Toast.LENGTH_SHORT).show();
                }

               if(m_editText_linkId.getText() != null && m_editText_linkId.getText().length() != 0){

                    m_linkId = Integer.parseInt(m_editText_linkId.getText().toString());

                    if(Integer.parseInt(m_editText_linkId.getText().toString()) >= m_srankMap.numOfLink){
                        Toast.makeText(getApplicationContext(), "링크 id가 링크 범위를 벗어 났습니다.", Toast.LENGTH_SHORT).show();
                        m_linkId = -1;
                    }

                    linkInfoView();

                }

            }

        }else if(v == m_button_SmapHeader){

            Intent intent = new Intent(getApplicationContext(), SmapHeaderActivity.class);
            startActivity(intent);

        }else if(v == m_button_SrankMap){

            if(m_mapId == -1){
                Toast.makeText(getApplicationContext(), "map id SEARCH 후 클릭해주세요. ", Toast.LENGTH_SHORT).show();
                return;
            }

            Intent intent = new Intent(getApplicationContext(), SrankMapActivity.class);
            intent.putExtra("map_id", m_mapId);
            startActivity(intent);


        }else if(v == m_button_LinkList){

            if(m_mapId == -1){
                Toast.makeText(getApplicationContext(), "map id SEARCH 후 클릭해주세요. ", Toast.LENGTH_SHORT).show();
                return;
            }else{
                Intent intent = new Intent(getApplicationContext(), LinkActivity.class);
                intent.putExtra("map_id", m_mapId);
                startActivity(intent);
            }
        }else if(v == m_button_NodeList){

            if(m_mapId == -1 || m_linkId == -1){
                Toast.makeText(getApplicationContext(), "map id, link id 모두 입력 후 클릭해주세요. ", Toast.LENGTH_SHORT).show();
                return;
            }else{
                    Intent intent = new Intent(getApplicationContext(), NodeActivity.class);
                    intent.putExtra("map_id", m_mapId);
                    intent.putExtra("start_node_id", m_startNode);
                    intent.putExtra("end_node_id", m_endNode);
                    startActivity(intent);
            }
        }else if(v == m_button_VertexList){

            if(m_linkId == -1 || m_mapId == -1){
                Toast.makeText(getApplicationContext(), "map id, link id 모두 입력 후 클릭해주세요. ", Toast.LENGTH_SHORT).show();
                return;
            }else{

                if(m_numberOfVertex <= 0){
                    Toast.makeText(getApplicationContext(), "보간점 개수가 0입니다. ", Toast.LENGTH_SHORT).show();
                    return;
                }else{

                    Intent intent = new Intent(getApplicationContext(), VertexActivity.class);
                    intent.putExtra("map_id", m_mapId);
                    intent.putExtra("link_id", m_linkId);
                    intent.putExtra("vertex_index", m_vertextIndex);
                    startActivity(intent);
                }


            }

        } else if(v == m_relativeLayoutMain){

            hideKeyboard();

        }else if(v == m_button_MapNode){

            if(m_mapId == -1){
                Toast.makeText(getApplicationContext(), "map id 입력 후 클릭해주세요. ", Toast.LENGTH_SHORT).show();
                return;
            }else{
                Intent intent = new Intent(getApplicationContext(), NodeActivity.class);
                intent.putExtra("map_id", m_mapId);

                startActivity(intent);
            }

//            m_smrControl.end();
//            finish();

        }else if(v == m_button_SetDir){

            hideKeyboard();

            m_newDir =  m_editText_SetDir.getText().toString();

            if(m_newDir == null || m_newDir.length() == 0){
                Toast.makeText(getApplicationContext(), "파일경로 입력 후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            }else{
                m_smrControl.setDir(ROOT_DIR + m_newDir);
                m_smrControl.createDirIfNotExists();

                filecopy(ROOT_DIR + m_dir, ROOT_DIR + m_newDir); // m_dir = "smrData" + File.separator + "mrdata";
                m_dir = m_newDir;

                m_editor = m_sharedPreferences.edit();
                m_editor.putString("DirPath", m_dir);
                m_editor.commit();

//                m_textViewFilePath.setText("File Path : " + ROOT_DIR + m_dir);
                Toast.makeText(getApplicationContext(), "파일경로 수정완료", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void filecopy(String orgFilePath, String newFilePath){

        File orgFile = new File(orgFilePath);
        File newFile = new File(newFilePath);

        if(orgFile.exists()){
            orgFile.renameTo(newFile);
        }

        orgFile.delete();

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //[Android 6.0 Permission Function]///////////////////////////////////////////////////////////////////////////////////////////

    public void checkPermission(){

        int READ_PHONE_PERMISSION = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int READ_EXTERNALSTORAGE_PERMISSION = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int WRITE_EXTERNALSTORAGE_PERMISSION = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if(READ_PHONE_PERMISSION == PackageManager.PERMISSION_GRANTED && READ_EXTERNALSTORAGE_PERMISSION == PackageManager.PERMISSION_GRANTED && WRITE_EXTERNALSTORAGE_PERMISSION == PackageManager.PERMISSION_GRANTED){

            m_SmrAsynctask.execute();

        }else{

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_READ_PHONE_STATE_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e(TAG, "Permission request" + requestCode);

        switch (requestCode) {

            case REQUEST_READ_PHONE_STATE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission Granted");
                    //Proceed to next steps
                    m_SmrAsynctask.execute();

                }
                else {
                    Log.e(TAG, "Permission Denied");
                }
                return;
            }
            case REQUEST_READ_EXTERNAL_STORAGE_PERMISSION:
            {
                if (grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission Granted");
                    //Proceed to next steps
                    m_SmrAsynctask.execute();

                }
                else {
                    Log.e(TAG, "Permission Denied");
                }
                return;
            }
            case REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION:
            {
                if (grantResults.length > 0 && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission Granted");
                    //Proceed to next steps
                    m_SmrAsynctask.execute();

                }
                else {
                    Log.e(TAG, "Permission Denied");
                }
                return;
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //[KeyBoard Function]///////////////////////////////////////////////////////////////////////////////////////////

    private void initKeyboardControl(){
        m_inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    private void hideKeyboard(){
        m_inputMethodManager.hideSoftInputFromWindow(m_editText_mapId.getWindowToken(),0);
    }

    //[가로모드 처리]//////////////////////////////////////////////////////////////////////////////////////////////

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
////    액티비티가 처음 생성 되었을 때 호출 일반 정적 설정을 모두 여기서 수행해야함
////    이 메서드에는 액티비티의 이전 상태가 캡처 된 경우에 해당 상태를 포함한 번들 객체가 전달
////    이 뒤에는 항상 onStart가 따라옴
//
//    }

    @Override
    public void onStart(){
        Log.v("hongTest", "onStart");
        super.onStart();

//        액티비티가 사용자에게 표시되기 직전에 호출
//        액티비티가 전경으로 나오면 onResume이 뒤에 따라오고, 액티비티가 숨겨지면 onStop이 뒤에 따라옴

    }

    @Override
    public void onStop(){
        Log.v("hongTest", "onStop");
        super.onStop();

//        액티비티가 더 이상 사용자에게 표시되지 않게 되면 호출
//        액티비티가 다시 사용자와 상호작용 하면 onRestart가 뒤에 따라오고
//        액티비티가 사라지면 onDetroy가 뒤에 따라옴

    }

    @Override
    public void onPause(){
        Log.v("hongTest", "onPause");
        super.onPause();

//        시스템이 다른 액티비티를 재개하기 직전에 호출
//        이 메서드는 일반적으로 데이터를 유지하기 위해 저장되지 않은 변경 사항을 커밋하는데
//        애니메이션을 비롯하여 CPU를 소모하는 기타 작업을 중단하는 등 여러가지 용도에 사용
//        이 메서드는 빨리 끝내야한다 이것이 반환 될떄까지 다음 액티비티가 재개되지 않기 때문
//        액티비티가 다시 전경으로 돌아오면 onResume이 뒤에 따라오고 액티비티가 사용자에게 보이지 않게 되면 onStop이 뒤에 따라옴

    }

    @Override
    public void onResume(){
        Log.v("hongTest", "onResume");
        super.onResume();

//        액티비티가 시작되고 사용자와 상호작용하기 직전에 호출
//        이시점에서 액티비티는 액티비티 스택 맨위에 있고 사용자가 정보를 입력하고 있음
//        이 뒤에는 onPause가 따라옴

    }

    @Override
    public void onDestroy(){
        Log.v("hongTest", "onDestroy");
        super.onDestroy();

//        액티비티가 소멸되기 전에 호출 액티비티가 받는 마지막 호출 이것이
//        호출될 수 있는 경우는 액티비티가 완료되는 중이기 때문
//        isFinishing메서드로 구분할 수 있음

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
            Log.v("hongTest", "onConfigurationChanged");

        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_main_textver);

            m_relativeLayoutMain = (RelativeLayout) findViewById(R.id.activity_main);
            m_relativeLayoutMain.setOnClickListener(this);

            m_editText_mapId = (EditText) findViewById(R.id.editText_mapId);

            if(m_mapId != -1){
                m_editText_mapId.setText(Integer.toString(m_mapId));
                m_editText_mapId.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(6) }); //입력숫자개수 제한
            }

            m_editText_linkId = (EditText) findViewById(R.id.editText_linkId);

            if(m_linkId != -1){
                m_editText_linkId.setText(Integer.toString(m_linkId));
                m_editText_linkId.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(6) }); //입력숫자개수 제한
            }

            if(m_editText_SetDir == null){
                m_editText_SetDir = (EditText) findViewById(R.id.editText_setDir);
                m_editText_SetDir.setFilters(new InputFilter[]{filterAlphaNum});
            }

            m_button_MapId = (Button) findViewById(R.id.button_search);
            m_button_MapId.setOnClickListener(this);

            m_button_SmapHeader = (Button) findViewById(R.id.button_SmapHeader);
            m_button_SmapHeader.setOnClickListener(this);

//            m_button_SrankManager = (Button) findViewById(R.id.button_SrankManager);
//            m_button_SrankManager.setOnClickListener(this);

            m_button_SrankMap = (Button) findViewById(R.id.button_SrankMap);
            m_button_SrankMap.setOnClickListener(this);

            m_button_LinkList = (Button) findViewById(R.id.button_linklist);
            m_button_LinkList.setOnClickListener(this);

            m_button_NodeList = (Button) findViewById(R.id.button_nodelist);
            m_button_NodeList.setOnClickListener(this);

            m_button_MapNode = (Button) findViewById(R.id.button_mapNode);
            m_button_MapNode.setOnClickListener(this);

            m_button_VertexList = (Button) findViewById(R.id.button_vertexlist);
            m_button_VertexList.setOnClickListener(this);


            if(m_button_SetDir == null) {
                m_button_SetDir = (Button) findViewById(R.id.button_setDir);
                m_button_SetDir.setOnClickListener(this);
            }

//            m_textViewInfo = (TextView) findViewById(R.id.textViewInfo1);
//            m_textViewInfo2 = (TextView) findViewById(R.id.textViewInfo2);
//            m_textViewInfo3 = (TextView) findViewById(R.id.textViewInfo3);

            View include7row_nopad = findViewById(R.id.include_7row_nopad_1);
            View include7row_nopad_2 = findViewById(R.id.include_7row_nopad_2);
            View include7row_nopad_3 = findViewById(R.id.include_7row_nopad_3);

            setTextViewId(include7row_nopad, include7row_nopad_2, include7row_nopad_3);

            SmapHeader smapHeader = m_smrControl.getSmapHeader();
            SrankManager srankManager = m_smrControl.getSrankManager();

            m_textViewMapId = (TextView) findViewById(R.id.textView_mapId);
            m_textViewMapId.setText("map id :\n(0~" + (srankManager.numOfMesh-1) +")");

            linkInfoView();

            if(m_srankMap != null){
                m_textViewquicksearchcontent1.setText(m_mapId+"");
                m_textViewquicksearchcontent2.setText(m_srankMap.numOfNode+"");
                m_textViewquicksearchcontent3.setText(m_srankMap.numOfLink+"");
            }

            m_textViewFilePath = (TextView) findViewById(R.id.textView_FilePath);
            m_textViewFilePath.setText("File Path : " + ROOT_DIR + m_dir);


        }else if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.activity_main_land_textver);

            m_relativeLayoutMain = (RelativeLayout) findViewById(R.id.activity_main_land);
            m_relativeLayoutMain.setOnClickListener(this);
//
            m_editText_mapId = (EditText) findViewById(R.id.editText_mapId_land);
//
            if(m_mapId != -1){
                m_editText_mapId.setText(Integer.toString(m_mapId));
                m_editText_mapId.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(6) }); //입력숫자개수 제한
            }
            m_editText_linkId = (EditText) findViewById(R.id.editText_linkId_land);

            if(m_linkId != -1){
                m_editText_linkId.setText(Integer.toString(m_linkId));
                m_editText_linkId.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(6) }); //입력숫자개수 제한
            }

            if(m_editText_SetDir == null){
                m_editText_SetDir = (EditText) findViewById(R.id.editText_setDir);
                m_editText_SetDir.setFilters(new InputFilter[]{filterAlphaNum});
            }
//
            m_button_MapId = (Button) findViewById(R.id.button_search_land);
            m_button_MapId.setOnClickListener(this);
//
            m_button_SmapHeader = (Button) findViewById(R.id.button_SmapHeader_land);
            m_button_SmapHeader.setOnClickListener(this);

            m_button_VertexList = (Button) findViewById(R.id.button_vertexlist_land);
            m_button_VertexList.setOnClickListener(this);

            m_button_SrankMap = (Button) findViewById(R.id.button_SrankMap_land);
            m_button_SrankMap.setOnClickListener(this);

            m_button_LinkList = (Button) findViewById(R.id.button_linklist_land);
            m_button_LinkList.setOnClickListener(this);

            m_button_NodeList = (Button) findViewById(R.id.button_nodelist_land);
            m_button_NodeList.setOnClickListener(this);

            m_button_MapNode = (Button) findViewById(R.id.button_end_land);
            m_button_MapNode.setOnClickListener(this);

            m_button_SetDir = (Button) findViewById(R.id.button_setDir_land);
            m_button_SetDir.setOnClickListener(this);

            View include7row_nopad = findViewById(R.id.include_7row_nopad_1_land);
            View include7row_nopad_2 = findViewById(R.id.include_7row_nopad_2_land);
            View include7row_nopad_3 = findViewById(R.id.include_7row_nopad_3_land);

            setTextViewId(include7row_nopad, include7row_nopad_2, include7row_nopad_3);

//
//            //textView_title_filepath
//
            SmapHeader smapHeader = m_smrControl.getSmapHeader();
            SrankManager srankManager = m_smrControl.getSrankManager();
//
            m_textViewMapId = (TextView) findViewById(R.id.textView_mapId_land);
            m_textViewMapId.setText("map id :\n(0~" + (srankManager.numOfMesh-1) +")");
//
            linkInfoView();
//
            if(m_srankMap != null){
                m_textViewquicksearchcontent1.setText(m_mapId+"");
                m_textViewquicksearchcontent2.setText(m_srankMap.numOfNode+"");
                m_textViewquicksearchcontent3.setText(m_srankMap.numOfLink+"");
            }
//
            m_textViewFilePath = (TextView) findViewById(R.id.textView_FilePath_land);
            m_textViewFilePath.setText("File Path : \n" + ROOT_DIR + m_dir);

        }

    }

    public void linkInfoView(){

        if(m_linkId != -1 && m_mapId != -1){

            Slink slink = new Slink();
            m_smrControl.getLink(m_mapId, m_linkId, slink);

            m_startNode = slink._startId;
            m_endNode = slink._endId;
            m_vertextIndex = slink._vertexIndex;
            m_numberOfVertex = slink._numOfVertex;

            String str_Drive = null;

            if(slink.drive == 0){
                str_Drive = "양방통행가능";
            }else if(slink.drive == 1){
                str_Drive = "정방통행가능";
            }else if(slink.drive == 2){
                str_Drive = "역방통행가능";
            }else if(slink.drive == 3){
                str_Drive = "양방통행불가능";
            }else{
                str_Drive = "잘못된 값";
            }

            String str_RoadType = null;

            if(slink.roadType == 0){
                str_RoadType = "종별없음";
            }else if(slink.roadType == 1){
                str_RoadType = "고속도로";
            }else if(slink.roadType == 2){
                str_RoadType = "도시고속도로";
            }else if(slink.roadType == 3){
                str_RoadType = "기타종별";
            }else {
                str_RoadType = "잘못된 값";
            }

            String str_LinkAttr = null;

            if(slink.linkAttr == 0){
                str_LinkAttr = "속성없음";
            }else if(slink.linkAttr == 1){
                str_LinkAttr = "고가차도";
            }else if(slink.linkAttr == 2){
                str_LinkAttr = "지하차도";
            }else if(slink.linkAttr == 3){
                str_LinkAttr = "터널";
            }else if(slink.linkAttr == 4){
                str_LinkAttr = "톨게이트";
            }else if(slink.linkAttr == 5){
                str_LinkAttr = "교량";
            }else if(slink.linkAttr == 6){
                str_LinkAttr = "단지 내 도로";
            }else if(slink.linkAttr == 7){
                str_LinkAttr = "휴게소";
            }else{
                str_LinkAttr = "잘못된 값";
            }

            String str_LinkType = null;

            if(slink.linkType == 0){
                str_LinkType = "본선링크";
            }else if(slink.linkType == 1){
                str_LinkType = "램프";
            }else if(slink.linkType == 2){
                str_LinkType = "연결로";
            }else if(slink.linkType == 3){
                str_LinkType = "교차로 내 링크";
            }else {
                str_LinkType = "잘못된 값";
            }

            String str_Divide = null;

            if(slink.divide == 0){
                str_Divide = "본선 비분리";
            }else if(slink.divide == 1){
                str_Divide = "본선 분리";
            }else{
                str_Divide = "잘못된 값";
            }

            if(m_linkId == -1){

                m_textViewquicksearchcontent1_info2.setText("");
                m_textViewquicksearchcontent2_info2.setText("");
                m_textViewquicksearchcontent3_info2.setText("");
                m_textViewquicksearchcontent4_info2.setText("");
                m_textViewquicksearchcontent5_info2.setText("");
                m_textViewquicksearchcontent6_info2.setText("");
                m_textViewquicksearchcontent7_info2.setText("");

                m_textViewquicksearchcontent1_info3.setText("");
                m_textViewquicksearchcontent2_info3.setText("");
                m_textViewquicksearchcontent3_info3.setText("");
                m_textViewquicksearchcontent4_info3.setText("");
                m_textViewquicksearchcontent5_info3.setText("");
                m_textViewquicksearchcontent6_info3.setText("");
                m_textViewquicksearchcontent7_info3.setText("");

            }
            else{

                m_textViewquicksearchcontent1_info2.setText(slink._startId+"");
                m_textViewquicksearchcontent2_info2.setText(slink._endId+"");
                m_textViewquicksearchcontent3_info2.setText(slink._length+"");
                m_textViewquicksearchcontent4_info2.setText(slink._numOfVertex+"");
                m_textViewquicksearchcontent5_info2.setText(slink.laneNum+"");

                m_textViewquicksearchcontent1_info3.setText(slink._vertexIndex+"");
                m_textViewquicksearchcontent2_info3.setText(slink.drive + "(" + str_Drive + ")");
                m_textViewquicksearchcontent3_info3.setText(slink.divide+ "(" + str_Divide + ")");
                m_textViewquicksearchcontent4_info3.setText(slink.linkType + "(" + str_LinkType + ")");
                m_textViewquicksearchcontent5_info3.setText(slink.linkAttr + "(" + str_LinkAttr + ")");
                m_textViewquicksearchcontent6_info3.setText(slink.roadType + "(" + str_RoadType + ")");

            }


        }else{
            m_textViewquicksearchcontent1_info2.setText("");
            m_textViewquicksearchcontent2_info2.setText("");
            m_textViewquicksearchcontent3_info2.setText("");
            m_textViewquicksearchcontent4_info2.setText("");
            m_textViewquicksearchcontent5_info2.setText("");
            m_textViewquicksearchcontent6_info2.setText("");
            m_textViewquicksearchcontent7_info2.setText("");

            m_textViewquicksearchcontent1_info3.setText("");
            m_textViewquicksearchcontent2_info3.setText("");
            m_textViewquicksearchcontent3_info3.setText("");
            m_textViewquicksearchcontent4_info3.setText("");
            m_textViewquicksearchcontent5_info3.setText("");
            m_textViewquicksearchcontent6_info3.setText("");
            m_textViewquicksearchcontent7_info3.setText("");

        }

    }

    public InputFilter filterAlphaNum = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            Pattern ps = Pattern.compile("^[a-zA-Z0-9/]"); // 한글만 ^[ㄱ-ㅎ가-힣]^

            if(!ps.matcher(source).matches()){
                return "";
            }

            return null;
        }
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setTextViewId(View include7row_nopad, View include7row_nopad_2, View include7row_nopad_3){

        m_textViewquicksearchtitle1 = (TextView) include7row_nopad.findViewById(R.id.include_7row_title1_nopad);
        m_textViewquicksearchtitle2 = (TextView) include7row_nopad.findViewById(R.id.include_7row_title2_nopad);
        m_textViewquicksearchtitle3 = (TextView) include7row_nopad.findViewById(R.id.include_7row_title3_nopad);
        m_textViewquicksearchtitle4 = (TextView) include7row_nopad.findViewById(R.id.include_7row_title4_nopad);
        m_textViewquicksearchtitle5 = (TextView) include7row_nopad.findViewById(R.id.include_7row_title5_nopad);
        m_textViewquicksearchtitle6 = (TextView) include7row_nopad.findViewById(R.id.include_7row_title6_nopad);
        m_textViewquicksearchtitle7 = (TextView) include7row_nopad.findViewById(R.id.include_7row_title7_nopad);

        m_textViewquicksearchcontent1 = (TextView) include7row_nopad.findViewById(R.id.include_7row_content1_nopad);
        m_textViewquicksearchcontent2 = (TextView) include7row_nopad.findViewById(R.id.include_7row_content2_nopad);
        m_textViewquicksearchcontent3 = (TextView) include7row_nopad.findViewById(R.id.include_7row_content3_nopad);
        m_textViewquicksearchcontent4 = (TextView) include7row_nopad.findViewById(R.id.include_7row_content4_nopad);
        m_textViewquicksearchcontent5 = (TextView) include7row_nopad.findViewById(R.id.include_7row_content5_nopad);
        m_textViewquicksearchcontent6 = (TextView) include7row_nopad.findViewById(R.id.include_7row_content6_nopad);
        m_textViewquicksearchcontent7 = (TextView) include7row_nopad.findViewById(R.id.include_7row_content7_nopad);

        m_textViewquicksearchtitle1_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_title1_nopad);
        m_textViewquicksearchtitle2_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_title2_nopad);
        m_textViewquicksearchtitle3_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_title3_nopad);
        m_textViewquicksearchtitle4_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_title4_nopad);
        m_textViewquicksearchtitle5_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_title5_nopad);
        m_textViewquicksearchtitle6_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_title6_nopad);
        m_textViewquicksearchtitle7_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_title7_nopad);

        m_textViewquicksearchcontent1_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_content1_nopad);
        m_textViewquicksearchcontent2_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_content2_nopad);
        m_textViewquicksearchcontent3_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_content3_nopad);
        m_textViewquicksearchcontent4_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_content4_nopad);
        m_textViewquicksearchcontent5_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_content5_nopad);
        m_textViewquicksearchcontent6_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_content6_nopad);
        m_textViewquicksearchcontent7_info2 = (TextView) include7row_nopad_2.findViewById(R.id.include_7row_content7_nopad);

        m_textViewquicksearchtitle1_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_title1_nopad);
        m_textViewquicksearchtitle2_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_title2_nopad);
        m_textViewquicksearchtitle3_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_title3_nopad);
        m_textViewquicksearchtitle4_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_title4_nopad);
        m_textViewquicksearchtitle5_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_title5_nopad);
        m_textViewquicksearchtitle6_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_title6_nopad);
        m_textViewquicksearchtitle7_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_title7_nopad);

        m_textViewquicksearchcontent1_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_content1_nopad);
        m_textViewquicksearchcontent2_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_content2_nopad);
        m_textViewquicksearchcontent3_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_content3_nopad);
        m_textViewquicksearchcontent4_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_content4_nopad);
        m_textViewquicksearchcontent5_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_content5_nopad);
        m_textViewquicksearchcontent6_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_content6_nopad);
        m_textViewquicksearchcontent7_info3 = (TextView) include7row_nopad_3.findViewById(R.id.include_7row_content7_nopad);

        m_textViewquicksearchtitle1.setText("map id : ");
        m_textViewquicksearchtitle2.setText("메시 내 노드 개수 : ");
        m_textViewquicksearchtitle3.setText("메시 내 링크 개수 : ");
        m_textViewquicksearchtitle4.setText("");
        m_textViewquicksearchtitle5.setVisibility(View.GONE);
        m_textViewquicksearchtitle6.setVisibility(View.GONE);
        m_textViewquicksearchtitle7.setVisibility(View.GONE);

        m_textViewquicksearchcontent5.setVisibility(View.GONE);
        m_textViewquicksearchcontent6.setVisibility(View.GONE);
        m_textViewquicksearchcontent7.setVisibility(View.GONE);

        m_textViewquicksearchtitle1_info2.setText("시작노드 id : ");
        m_textViewquicksearchtitle2_info2.setText("종료노드 id : ");
        m_textViewquicksearchtitle3_info2.setText("링크 길이 : ");
        m_textViewquicksearchtitle4_info2.setText("보간점 개수 : ");
        m_textViewquicksearchtitle5_info2.setText("차선수 : ");
        m_textViewquicksearchtitle6_info2.setVisibility(View.GONE);
        m_textViewquicksearchtitle7_info2.setVisibility(View.GONE);

        m_textViewquicksearchcontent6_info2.setVisibility(View.GONE);
        m_textViewquicksearchcontent7_info2.setVisibility(View.GONE);

        m_textViewquicksearchtitle1_info3.setText("보간점 인덱스 :");
        m_textViewquicksearchtitle2_info3.setText("링크 통행코드 : ");
        m_textViewquicksearchtitle3_info3.setText("본선 분리 : ");
        m_textViewquicksearchtitle4_info3.setText("링크 종별 : ");
        m_textViewquicksearchtitle5_info3.setText("링크 속성 : ");
        m_textViewquicksearchtitle6_info3.setText("도로 종별 : ");

        m_textViewquicksearchtitle7_info3.setVisibility(View.GONE);
        m_textViewquicksearchcontent7_info3.setVisibility(View.GONE);
    }

}
