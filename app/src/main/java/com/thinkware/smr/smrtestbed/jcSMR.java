package com.thinkware.smr.smrtestbed;


public class jcSMR {

    //추가 JNI함수///////////////////////////////////////////////////////////////////////////
    public native int getSvertex(int map_id, int link_id, int vertex_id, Slink.SVertex info);

    public native int getLink(int map_id, int link_id, Slink info);
    public native int getSlink(int map_id, Slink[] info);
    //public native int getSlink(int map_id, int link_id, Slink info);
    public native int getCrossnode(int map_id, int node_id, Snode.SCrossNode info);
    //public native int getAdjnode(int map_id, int node_id, Snode.SAdjNode info);

    public native int getAdjnode(int map_id, Snode[] info);
    //public native int getAdjnode(int map_id, int node_id, Snode.SAdjNode info);
    public native int getNode(int map_id, Snode[] info);
    public native int getSnode(int map_id, int node_id, Snode info);
    public native int getSmeshInfo(int map_id, SmeshInfo info);
    public native int getSrankMap(int map_id, SrankMap info);
    public native int sidxInsert(int map_id);
    public native String logCall();
    /////////////////////////////////////////////////////////////////////////////////////////

    public native String stringFromJNI();
    public native int intFromJNI(int arr[]) ;
    public native int getSrankManager(SrankManager info) ;
    public native int getSmapHeader(SmapHeader info) ;
    
    public native void init() ;
    public native int setDir(String name) ;
    public native void start() ;
    public native int open() ;
    public native void openSet(int flag) ;
    public native int openManage() ;
    public native int openMeshinfo() ;
    public native int openMeshdata() ;
    public native int openCacheinfo() ;
    public native int readSmrManage() ;
    public native int readSmrMeshinfo() ;
    public native int readCacheinfo(int flag) ;
    public native void closeManage() ;
    public native void closeMeshinfo() ;
    public native void closeCacheinfo() ;
    public native void closeMeshdata() ;
    public native void close() ;
    public native void end() ;
    public native int fileExist(String name) ;
    public native int fileDelete(String name) ;
    public native int delete() ;
    public native int createSmrMeshdata() ;
    public native int writeSmrManage(byte arr[]) ;
    public native int writeSmrMeshinfo(byte arr[]) ;
    public native int writeSmrCacheinfo(byte arr[]) ;
    public native int writeMesh(byte arr[]) ;
    public native int writeMeshdata(byte arr1[], int arr2[], int arr3[], int count) ;
    public native int writeSmrZero() ;
    public native int overwriteSmrCacheinfo() ;
    public native int makeSmrCacheinfo() ;
    public native int mappingInit() ;
    public native int ridxInit() ; 
    public native int sidxMultiGet(int arr[]) ;
    ///[TEST]////////////////////////////////////////
    public native int getNowFetchCount() ;
    public native int testReadNodeList(int map_id) ;
    
    /////////////////////////////////////////////////////////
    
    public native int test1() ;
    public native int test2(int map_id) ;
	public native int test3() ;
	public native int test4() ;
	public native int test5() ;
	public native int test6() ;
	public native int test7() ;

	public native void getRealMesh(int xpos, int ypos) ;
	public native void readMapIdVertexList() ;


	static {
		try 
		{
			//Log.i("jwlib", "before - smrEx Lib");
			System.loadLibrary("smrEx");
			//Log.i("jwlib", "after - smrEx Lib");
		} 
		catch (Exception e) 
		{
			//Log.i("jwlib", "error - smrEx Lib");
			
			//Log.e("SON_D", "error while loading library");
			//Log.e("SON_D", "\t:"+e.toString());
			System.exit(1);
			
		}
	}
}
 