package com.thinkware.smr.smrtestbed;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by honghk on 2017. 2. 9..
 */

public class AlertDialogFragment {

    Context m_context = null;
    int m_resourceId = -1;
    String m_title = null;
    LayoutInflater m_inflater = null;
    View m_dialogView = null;

    public AlertDialogFragment(Context context, int resourceId, String title){

        m_context = context;
        m_resourceId = resourceId;
        m_title = title;
        m_inflater = LayoutInflater.from(m_context);

    }

    public View getDialogView(){

        m_dialogView = m_inflater.inflate(m_resourceId, null);
        return m_dialogView;
    }


    public AlertDialog alertDialongCreate(View dialogView){

        AlertDialog.Builder builder = new AlertDialog.Builder(m_context);
        builder.setTitle(m_title);
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setView(m_dialogView);
        AlertDialog dialog = builder.create();
        return dialog;

    }

}
