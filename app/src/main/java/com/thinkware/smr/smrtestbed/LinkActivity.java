package com.thinkware.smr.smrtestbed;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class LinkActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    private int m_mapId = -1; // 지역변수로 하면 putExtra에서 에러
    private Slink[] m_slinks = null;
    private ListAdapterlink m_listAdapterlink = null;
    private SrankMap m_srankMap = null;
    private Button m_button_linkSearch = null;
    private RelativeLayout m_relativeLayoutLink = null;
    private EditText m_editText_searchNumber = null;

    private String m_filter_type = null;
    private String m_filter_value = null;

    private Spinner m_spinner_option = null;
    private ArrayAdapter<CharSequence> m_adapter = null;

    private Spinner m_spinner_type = null;
    private ArrayAdapter<CharSequence> m_adapter_type = null;

    private InputMethodManager m_inputMethodManager = null;

    @Override
    protected void onResume(){
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_textver);

        initKeyboardControl();

        if(m_relativeLayoutLink == null){
            m_relativeLayoutLink = (RelativeLayout) findViewById(R.id.relativeLayout_link);
            m_relativeLayoutLink.setOnClickListener(this);
        }

        if(m_editText_searchNumber == null){
            m_editText_searchNumber = (EditText) findViewById(R.id.editText_SearchNumber);
            m_editText_searchNumber.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(6) }); //입력숫자개수 제한
        }

        int i = 0;
        int vertex_index_sum = 0;

        if(m_button_linkSearch == null){
            m_button_linkSearch = (Button) findViewById(R.id.button_linkSearch);
            m_button_linkSearch.setOnClickListener(this);
        }

        SmrControl smrControl = new SmrControl(this);

        Intent intent = getIntent();
        m_mapId = intent.getIntExtra("map_id",-1);

        if(m_srankMap == null){
            m_srankMap = smrControl.getSrankMap(m_mapId);
        }

        TextView tvTest = (TextView) findViewById(R.id.textViewLink);
        tvTest.setText("map id : " + m_mapId + " / link cnt : " + m_srankMap.numOfLink);

        int[] vertex_index = new int[m_srankMap.numOfLink];

        m_slinks = smrControl.getSlink(m_mapId);

        if(m_slinks == null){
            return;
        }

        ArrayList<Slink> listItem = new ArrayList<Slink>();
        for (i = 0; i < m_srankMap.numOfLink; i++){
            m_slinks[i].linkId = i;
            listItem.add(m_slinks[i]);
            vertex_index_sum = vertex_index_sum + m_slinks[i]._vertexIndex;
            vertex_index[i] = vertex_index_sum;
        }

        m_listAdapterlink = new ListAdapterlink(this, R.layout.listview_linkitems_textver, listItem);

        ListView listViewLinkList = (ListView) findViewById(R.id.listViewLinkList);
        listViewLinkList.setAdapter(m_listAdapterlink);

        if(m_spinner_option == null){
            m_spinner_option = (Spinner) findViewById(R.id.spinner_option);
        }

        if(m_adapter == null){
            m_adapter = ArrayAdapter.createFromResource(this, R.array.spiner_option_array, android.R.layout.simple_spinner_item);
        }

        if(m_spinner_type == null){
            m_spinner_type = (Spinner) findViewById(R.id.spinner_type);
        }

        m_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        m_spinner_option.setAdapter(m_adapter);
        m_spinner_option.setOnItemSelectedListener(this);

        listViewLinkList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){

                int linkid = m_listAdapterlink.m_arrSlink.get(position).linkId;

                if(m_slinks[linkid]._numOfVertex == 0){
                    Toast.makeText(getApplicationContext(), "링크 번호 : " + linkid + " 보간점 개수가 0개 입니다.", Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(getApplicationContext(), "링크 번호 : " + linkid, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), VertexActivity.class);

                intent.putExtra("map_id", m_mapId);
                intent.putExtra("link_id", linkid);
                intent.putExtra("vertex_index", m_slinks[linkid]._vertexIndex);

                startActivity(intent);
            }
        });

    }

    class ListAdapterlink extends BaseAdapter{

        private Context m_context = null;
        private LayoutInflater m_inflater = null;
        private ArrayList<Slink> m_arrSlink = null;
        private ArrayList<Slink> m_arrSlink_filter = null;
        private int m_layout;
        public int m_linkId = 0;

        public ListAdapterlink(Context context, int layout_id, ArrayList<Slink> arrSlink){
            m_context = context;
            m_layout = layout_id;
            m_arrSlink = arrSlink;

            m_arrSlink_filter = new ArrayList<Slink>();
            m_arrSlink_filter.addAll(m_arrSlink);

            m_inflater = LayoutInflater.from(m_context);
        }

        @Override
        public int getCount(){
            return m_arrSlink.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            if(convertView == null){
                convertView = m_inflater.inflate(m_layout, parent, false);
            }
            convertView.setMinimumHeight(1000);

            String str_Drive = null;

            if(m_arrSlink.get(position).drive == 0){
                str_Drive = "양방통행가능";
            }else if(m_arrSlink.get(position).drive == 1){
                str_Drive = "정방통행가능";
            }else if(m_arrSlink.get(position).drive == 2){
                str_Drive = "역방통행가능";
            }else if(m_arrSlink.get(position).drive == 3){
                str_Drive = "양방통행불가능";
            }else{
                str_Drive = "잘못된 값";
            }

            String str_RoadType = null;

            if(m_arrSlink.get(position).roadType == 0){
                str_RoadType = "종별없음";
            }else if(m_arrSlink.get(position).roadType == 1){
                str_RoadType = "고속도로";
            }else if(m_arrSlink.get(position).roadType == 2){
                str_RoadType = "도시고속도로";
            }else if(m_arrSlink.get(position).roadType == 3){
                str_RoadType = "기타종별";
            }else {
                str_RoadType = "잘못된 값";
            }

            String str_LinkAttr = null;

            if(m_arrSlink.get(position).linkAttr == 0){
                str_LinkAttr = "속성없음";
            }else if(m_arrSlink.get(position).linkAttr == 1){
                str_LinkAttr = "고가차도";
            }else if(m_arrSlink.get(position).linkAttr == 2){
                str_LinkAttr = "지하차도";
            }else if(m_arrSlink.get(position).linkAttr == 3){
                str_LinkAttr = "터널";
            }else if(m_arrSlink.get(position).linkAttr == 4){
                str_LinkAttr = "톨게이트";
            }else if(m_arrSlink.get(position).linkAttr == 5){
                str_LinkAttr = "교량";
            }else if(m_arrSlink.get(position).linkAttr == 6){
                str_LinkAttr = "단지 내 도로";
            }else if(m_arrSlink.get(position).linkAttr == 7){
                str_LinkAttr = "휴게소";
            }else{
                str_LinkAttr = "잘못된 값";
            }

            String str_LinkType = null;

            if(m_arrSlink.get(position).linkType == 0){
                str_LinkType = "본선링크";
            }else if(m_arrSlink.get(position).linkType == 1){
                str_LinkType = "램프";
            }else if(m_arrSlink.get(position).linkType == 2){
                str_LinkType = "연결로";
            }else if(m_arrSlink.get(position).linkType == 3){
                str_LinkType = "교차로 내 링크";
            }else {
                str_LinkType = "잘못된 값";
            }

            String str_Divide = null;

            if(m_arrSlink.get(position).divide == 0){
                str_Divide = "본선 비분리";
            }else if(m_arrSlink.get(position).divide == 1){
                str_Divide = "본선 분리";
            }else{
                str_Divide = "잘못된 값";
            }

            View linklistrow1 = convertView.findViewById(R.id.linklistitmerow1);
            View linklistrow2 = convertView.findViewById(R.id.linklistitmerow2);
            View linklistrow3 = convertView.findViewById(R.id.linklistitmerow3);
            View linklistrow4 = convertView.findViewById(R.id.linklistitmerow4);


            TextView Row1_Col1_Title = (TextView) linklistrow1.findViewById(R.id.title_col1);
            TextView Row1_Col2_Title = (TextView) linklistrow1.findViewById(R.id.title_col2);
            TextView Row1_Col3_Title = (TextView) linklistrow1.findViewById(R.id.title_col3);

            TextView Row2_Col1_Title = (TextView) linklistrow2.findViewById(R.id.title_col1);
            TextView Row2_Col2_Title = (TextView) linklistrow2.findViewById(R.id.title_col2);
            TextView Row2_Col3_Title = (TextView) linklistrow2.findViewById(R.id.title_col3);

            TextView Row3_Col1_Title = (TextView) linklistrow3.findViewById(R.id.title_col1);
            TextView Row3_Col2_Title = (TextView) linklistrow3.findViewById(R.id.title_col2);
            TextView Row3_Col3_Title = (TextView) linklistrow3.findViewById(R.id.title_col3);

            TextView Row4_Col1_Title = (TextView) linklistrow4.findViewById(R.id.title_col1);
            TextView Row4_Col2_Title = (TextView) linklistrow4.findViewById(R.id.title_col2);
            TextView Row4_Col3_Title = (TextView) linklistrow4.findViewById(R.id.title_col3);


            TextView Row1_Col1_Content = (TextView) linklistrow1.findViewById(R.id.content_col1);
            TextView Row1_Col2_Content = (TextView) linklistrow1.findViewById(R.id.content_col2);
            TextView Row1_Col3_Content = (TextView) linklistrow1.findViewById(R.id.content_col3);

            TextView Row2_Col1_Content = (TextView) linklistrow2.findViewById(R.id.content_col1);
            TextView Row2_Col2_Content = (TextView) linklistrow2.findViewById(R.id.content_col2);
            TextView Row2_Col3_Content = (TextView) linklistrow2.findViewById(R.id.content_col3);

            TextView Row3_Col1_Content = (TextView) linklistrow3.findViewById(R.id.content_col1);
            TextView Row3_Col2_Content = (TextView) linklistrow3.findViewById(R.id.content_col2);
            TextView Row3_Col3_Content = (TextView) linklistrow3.findViewById(R.id.content_col3);

            TextView Row4_Col1_Content = (TextView) linklistrow4.findViewById(R.id.content_col1);
            TextView Row4_Col2_Content = (TextView) linklistrow4.findViewById(R.id.content_col2);
            TextView Row4_Col3_Content = (TextView) linklistrow4.findViewById(R.id.content_col3);


            Row1_Col1_Title.setText("link id");
            Row1_Col1_Content.setText(m_arrSlink.get(position).linkId+"");

            Row1_Col2_Title.setText("보간점 개수");
            Row1_Col2_Content.setText(m_arrSlink.get(position)._numOfVertex+"");

            Row1_Col3_Title.setText("본선분리");
            Row1_Col3_Content.setText(m_arrSlink.get(position).divide + "(" + str_Divide + ")");

            Row2_Col1_Title.setText("시작노드 id");
            Row2_Col1_Content.setText(m_arrSlink.get(position)._startId+"");

            Row2_Col2_Title.setText("보간점 인덱스");
            Row2_Col2_Content.setText(m_arrSlink.get(position)._vertexIndex+"");

            Row2_Col3_Title.setText("링크종별");
            Row2_Col3_Content.setText(m_arrSlink.get(position).linkType + "(" + str_LinkType + ")");

            Row3_Col1_Title.setText("종료노드 id");
            Row3_Col1_Content.setText(m_arrSlink.get(position)._endId+"");

            Row3_Col2_Title.setText("차선수");
            Row3_Col2_Content.setText(m_arrSlink.get(position).laneNum+"");

            Row3_Col3_Title.setText("링크속성");
            Row3_Col3_Content.setText(m_arrSlink.get(position).linkAttr + "(" + str_LinkAttr + ")");

            Row4_Col1_Title.setText("링크길이");
            Row4_Col1_Content.setText(m_arrSlink.get(position)._length+"");

            Row4_Col2_Title.setText("링크 통행코드");
            Row4_Col2_Content.setText(m_arrSlink.get(position).drive + "(" + str_Drive + ")");

            Row4_Col3_Title.setText("도로종별");
            Row4_Col3_Content.setText(m_arrSlink.get(position).roadType + "(" + str_RoadType + ")");

//
//
//            Row1_Col1_Title.setText("link id");
//            Row1_Col1_Content.setText(m_arrSlink.get(position).linkId);
//
//            TextView m_textViewIndex = (TextView) convertView.findViewById(R.id.textViewIndex);
//
//            m_textViewIndex.setText("link id \n" + m_arrSlink.get(position).linkId);
//
//            TextView m_textViewStartId = (TextView) convertView.findViewById(R.id.textViewStartId);
//            m_textViewStartId.setText("시작노드 id\n" + m_arrSlink.get(position)._startId);
//
//            TextView m_textViewEndId = (TextView) convertView.findViewById(R.id.textViewEndId);
//            m_textViewEndId.setText("종료노드 id\n" + m_arrSlink.get(position)._endId);
//
//            TextView m_textViewLength = (TextView) convertView.findViewById(R.id.textViewLength);
//            m_textViewLength.setText("링크길이\n" + m_arrSlink.get(position)._length);
//
//            TextView m_textViewNumOfVertex = (TextView) convertView.findViewById(R.id.textViewNunOfVertex);
//            m_textViewNumOfVertex.setText("보간점 개수\n" + m_arrSlink.get(position)._numOfVertex);
//
//            TextView m_textViewVertexIndex = (TextView) convertView.findViewById(R.id.textViewVertexIndex);
//            m_textViewVertexIndex.setText("보간점 인덱스\n" + m_arrSlink.get(position)._vertexIndex);
//
//            TextView m_textViewLaneNum = (TextView) convertView.findViewById(R.id.textViewLaneNum);
//            m_textViewLaneNum.setText("차선수\n" + m_arrSlink.get(position).laneNum);
//
//            TextView m_textViewDrive = (TextView) convertView.findViewById(R.id.textViewDrive);
//            m_textViewDrive.setText("링크 통행코드\n" + m_arrSlink.get(position).drive + "(" + str_Drive + ")");
//
//            TextView m_textViewDivide = (TextView) convertView.findViewById(R.id.textViewDivide);
//            m_textViewDivide.setText("본선분리\n" + m_arrSlink.get(position).divide + "(" + str_Divide + ")");
//
//            TextView m_textViewLinkType = (TextView) convertView.findViewById(R.id.textViewLinkType);
//            m_textViewLinkType.setText("링크종별\n" + m_arrSlink.get(position).linkType + "(" + str_LinkType + ")");
//
//            TextView m_textViewLinkAttr = (TextView) convertView.findViewById(R.id.textViewLinkAttr);
//            m_textViewLinkAttr.setText("링크속성\n" + m_arrSlink.get(position).linkAttr + "(" + str_LinkAttr + ")");
//
//            TextView m_textViewRoadType = (TextView) convertView.findViewById(R.id.textViewRoadType);
//            m_textViewRoadType.setText("도로종별\n" + m_arrSlink.get(position).roadType + "(" + str_RoadType + ")");

            return convertView;
        }

        @Override
        public Object getItem(int position){
            return m_arrSlink.get(position).linkId ;
        }

        @Override
        public long getItemId(int postion){
            return postion;
        }

        public void filter(){

            m_arrSlink.clear();

            if(m_filter_type.equals("선택없음") || m_filter_type.equals("보간점 개수") || m_filter_type.equals("링크길이")){
                m_arrSlink.addAll(m_arrSlink_filter);

                if(m_filter_type.equals("보간점 개수")){


                    if(!m_filter_value.equals("선택없음")){
                        Collections.sort(m_arrSlink, m_comparator_numOfVertex);

                        if (m_filter_value.equals("DESC")) {
                            Collections.reverse(m_arrSlink);
                        }
                    }

                }else if(m_filter_type.equals("링크길이")){

                    if(!m_filter_value.equals("선택없음")){
                        Collections.sort(m_arrSlink, m_comparator_length);

                        if (m_filter_value.equals("DESC")) {
                            Collections.reverse(m_arrSlink);
                        }
                    }
                }

            }else{

                int select = -1;

                if(m_filter_type.equals("링크 통행코드")){

                    if(m_filter_value.equals("양방통행가능")){
                        select = 0;
                    }else if(m_filter_value.equals("정방통행가능")){
                        select = 1;
                    }else if(m_filter_value.equals("역방통행가능")){
                        select = 2;
                    }else{
                        select = 3;
                    }

                    for(Slink slink : m_arrSlink_filter){

                        if(select == slink.drive){
                            m_arrSlink.add(slink);
                        }
                    }
                }else if(m_filter_type.equals("도로종별")){

                    if(m_filter_value.equals("종별없음")){
                        select = 0;
                    }else if(m_filter_value.equals("고속도로")){
                        select = 1;
                    }else if(m_filter_value.equals("도시고속도로")){
                        select = 2;
                    }else if(m_filter_value.equals("기타종별")){
                        select = 3;
                    }

                    for(Slink slink : m_arrSlink_filter){

                        if(select == slink.roadType){
                            m_arrSlink.add(slink);
                        }

                    }
                }else if(m_filter_type.equals("본선분리")){

                    if(m_filter_value.equals("본선 비분리")){
                        select = 0;
                    }else if(m_filter_value.equals("본선 분리")){
                        select = 1;
                    }

                    for(Slink slink : m_arrSlink_filter){

                        if(select == slink.divide){

                            m_arrSlink.add(slink);
                        }

                    }
                }else if(m_filter_type.equals("링크종별")){

                    if(m_filter_value.equals("본선링크")){
                        select = 0;
                    }else if(m_filter_value.equals("램프")){
                        select = 1;
                    }else if(m_filter_value.equals("연결로")){
                        select = 2;
                    }else if(m_filter_value.equals("교차로 내 링크")){
                        select = 3;
                    }

                    for(Slink slink : m_arrSlink_filter){

                        if(select == slink.linkType){
                            m_arrSlink.add(slink);
                        }

                    }
                }else if(m_filter_type.equals("링크속성")){

                    if(m_filter_value.equals("속성없음")){
                        select = 0;
                    }else if(m_filter_value.equals("고가차도")){
                        select = 1;
                    }else if(m_filter_value.equals("지하차도")){
                        select = 2;
                    }else if(m_filter_value.equals("터널")){
                        select = 3;
                    }else if(m_filter_value.equals("톨게이트")){
                        select = 4;
                    }else if(m_filter_value.equals("교량")){
                        select = 5;
                    }else if(m_filter_value.equals("단지내도로")){
                        select = 6;
                    }else if(m_filter_value.equals("휴게소")){
                        select = 7;
                    }

                    for(Slink slink : m_arrSlink_filter){

                        if(select == slink.linkAttr){
                            m_arrSlink.add(slink);
                        }

                    }
                }else if(m_filter_type.equals("차선수")){

                    select = Integer.parseInt(m_filter_value);

                    for(Slink slink : m_arrSlink_filter){

                        if(select == slink.laneNum){

                            m_arrSlink.add(slink);
                        }

                    }

                }else {

                    if(m_editText_searchNumber.getText().length() == 0 || m_editText_searchNumber.getText() == null || Integer.parseInt(m_editText_searchNumber.getText().toString()) < 0 ){
                        Toast.makeText(getApplicationContext(), "잘못된 값입니다. 다시입력해주세요", Toast.LENGTH_SHORT).show();
                        m_arrSlink.addAll(m_arrSlink_filter);
                    }else {
                        if(m_filter_type.equals("시작노드id")){

                            for(Slink slink : m_arrSlink_filter){

                                if(Integer.parseInt(m_editText_searchNumber.getText().toString()) == slink._startId){
                                    m_arrSlink.add(slink);
                                }
                            }
                        }else if(m_filter_type.equals("종료노드id")){

                            for(Slink slink : m_arrSlink_filter){

                                if(Integer.parseInt(m_editText_searchNumber.getText().toString()) == slink._endId){
                                    m_arrSlink.add(slink);
                                }
                            }
                        }else{

                            for(Slink slink : m_arrSlink_filter){

                                if(Integer.parseInt(m_editText_searchNumber.getText().toString()) == slink.linkId){
                                    m_arrSlink.add(slink);
                                }//if
                            }//for

                        }//else
                    }//else

                }//else


            }

            notifyDataSetChanged();
        }
    }

    public void logCall(String str_log){

        FragmentManager fragmentManager = getSupportFragmentManager();
        LogDialogFragment logDialogFragment = new LogDialogFragment();
        logDialogFragment.setActivity(this);
        logDialogFragment.LogSet(str_log);
        logDialogFragment.show(fragmentManager, "log");
    }

    public void onClick(View v){

        if(v == null){
            return;

        }else if(v == m_button_linkSearch){

            m_listAdapterlink.filter();
            hideKeyboard();
        }

    }

    public void onItemSelected(AdapterView<?> parent, View v, int pos, long id){

        if(parent == m_spinner_option){

            m_spinner_type.setVisibility(View.VISIBLE);
            m_editText_searchNumber.setVisibility(View.INVISIBLE);
            if(parent.getItemAtPosition(pos).equals("도로종별")){
                m_adapter_type = ArrayAdapter.createFromResource(this, R.array.spiner_option_roadType_array, android.R.layout.simple_spinner_item);

            }else if(parent.getItemAtPosition(pos).equals("링크 통행코드")){
                m_adapter_type = ArrayAdapter.createFromResource(this, R.array.spiner_option_drive_array, android.R.layout.simple_spinner_item);

            }else if(parent.getItemAtPosition(pos).equals("본선분리")){
                m_adapter_type = ArrayAdapter.createFromResource(this, R.array.spiner_option_divide_array, android.R.layout.simple_spinner_item);

            }else if(parent.getItemAtPosition(pos).equals("링크종별")){
                m_adapter_type = ArrayAdapter.createFromResource(this, R.array.spiner_option_linkType_array, android.R.layout.simple_spinner_item);

            }else if(parent.getItemAtPosition(pos).equals("링크속성")){
                m_adapter_type = ArrayAdapter.createFromResource(this, R.array.spiner_option_linkAttr_array, android.R.layout.simple_spinner_item);

            }else if(parent.getItemAtPosition(pos).equals("차선수")){
                m_adapter_type = ArrayAdapter.createFromResource(this, R.array.spiner_option_laneNume_array, android.R.layout.simple_spinner_item);

            }else if(parent.getItemAtPosition(pos).equals("선택없음")){
                m_adapter_type = ArrayAdapter.createFromResource(this, R.array.spiner_option_default_array, android.R.layout.simple_spinner_item);

            }else if(parent.getItemAtPosition(pos).equals("보간점 개수") || parent.getItemAtPosition(pos).equals("링크길이")){
                m_adapter_type = ArrayAdapter.createFromResource(this, R.array.spiner_option_sort_array, android.R.layout.simple_spinner_item);

            }else{
                m_adapter_type = ArrayAdapter.createFromResource(this, R.array.spiner_option_default_array, android.R.layout.simple_spinner_item);
                m_spinner_type.setVisibility(View.INVISIBLE);
                m_editText_searchNumber.setVisibility(View.VISIBLE);
            }
            m_adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            m_spinner_type.setAdapter(m_adapter_type);
            m_spinner_type.setOnItemSelectedListener(this);

            m_filter_type = parent.getItemAtPosition(pos).toString();

        }else if(parent == m_spinner_type){

            m_filter_value = parent.getItemAtPosition(pos).toString();

        }else{
            Toast.makeText(getApplicationContext(), "View 선택없음", Toast.LENGTH_SHORT).show();
        }

    }

    public void onNothingSelected(AdapterView<?> parent){

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //[KeyBoard Function]///////////////////////////////////////////////////////////////////////////////////////////

    private void initKeyboardControl(){
        m_inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    private void hideKeyboard(){
        m_inputMethodManager.hideSoftInputFromWindow(m_editText_searchNumber.getWindowToken(),0);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private final static Comparator<Slink> m_comparator_numOfVertex = new Comparator<Slink>() {

        private final Collator collator = Collator.getInstance();

        @Override
        public int compare(Slink slink1, Slink slink2) {

            if(slink1._numOfVertex < slink2._numOfVertex){
                return 1;
            }else if(slink1._numOfVertex > slink2._numOfVertex){
                return -1;
            }else{
                return 0;
            }

        }
    };

    private final static Comparator<Slink> m_comparator_length = new Comparator<Slink>() {

        private final Collator collator = Collator.getInstance();

        @Override
        public int compare(Slink slink1, Slink slink2) {

            if(slink1._length < slink2._length){
                return 1;
            }else if(slink1._length > slink2._length){
                return -1;
            }else{
                return 0;
            }

        }
    };

}
