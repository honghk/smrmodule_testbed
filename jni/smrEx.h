#ifndef _SMR_EX_H_
#define _SMR_EX_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <jni.h>

//extern void sMR_init(void) ;        // only Main One call function
//extern void sMR_start(void) ;       // only Main call function


extern void g_set(void) ;

JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getNode( JNIEnv* env, jobject, jint map_id, jobjectArray info);
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getSvertex( JNIEnv* env, jobject, jint map_id, jint link_id, jint vertex_id, jobject info);

JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getLink( JNIEnv* env, jobject, jint map_id, jint link_id, jobject info);
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getSlink( JNIEnv* env, jobject, jint map_id, jobjectArray info);
//JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getSlink( JNIEnv* env, jobject, jint map_id, jint link_id, jobject info);
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getCrossnode( JNIEnv* env, jobject, jint map_id, jint node_id, jobject info);

JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getAdjnode( JNIEnv* env, jobject, jint map_id, jobjectArray info);
//JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getAdjnode( JNIEnv* env, jobject, jint map_id, jint node_id, jobject info);
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getSnode( JNIEnv* env, jobject, jint map_id, jint node_id, jobject info);
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getSmeshInfo( JNIEnv* env, jobject, jint map_id, jobject info);
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getSrankMap( JNIEnv* env, jobject, jint map_id, jobject info);
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_sidxInsert( JNIEnv* env, jobject , jint map_id);
JNIEXPORT jstring JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_logCall( JNIEnv* env, jobject thiz );
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
JNIEXPORT jstring JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_stringFromJNI( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_intFromJNI( JNIEnv* env, jobject thiz, jintArray arr ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getSmapHeader( JNIEnv* env, jclass obj, jobject info) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getSrankManager( JNIEnv* env, jclass obj, jobject info) ;
///[TEST]////////////////////////////////////////////////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getNowFetchCount( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_testReadNodeList( JNIEnv* env, jobject thiz, jint map_id ) ;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_init( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_setDir( JNIEnv * env, jobject jobj, jstring name ) ;
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_start( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_open( JNIEnv* env, jobject thiz ) ;
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_openSet( JNIEnv* env, jobject thiz, jint flag ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_openManage( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_openMeshinfo( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_openMeshdata( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_openCacheinfo( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_readSmrManage( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_readSmrMeshinfo( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_readCacheinfo( JNIEnv* env, jobject thiz, jint flag ) ;
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_closeManage( JNIEnv* env, jobject thiz ) ;
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_closeMeshinfo( JNIEnv* env, jobject thiz ) ;
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_closeCacheinfo( JNIEnv* env, jobject thiz ) ;
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_closeMeshdata( JNIEnv* env, jobject thiz ) ;
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_close( JNIEnv* env, jobject thiz ) ;
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_end( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_fileExist( JNIEnv * env, jobject jobj, jstring name ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_fileDelete( JNIEnv * env, jobject jobj, jstring name ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_delete( JNIEnv * env, jobject jobj ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_createSmrMeshdata( JNIEnv* env, jobject thiz ) ;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_writeSmrManage( JNIEnv* env, jobject obj, jbyteArray arr) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_writeSmrMeshinfo( JNIEnv* env, jobject obj, jbyteArray arr) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_writeSmrCacheinfo( JNIEnv* env, jobject obj, jbyteArray arr) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_writeMesh( JNIEnv* env, jobject obj, jbyteArray arr) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_writeMeshdata( JNIEnv* env, jobject obj,
		jbyteArray arr1, jintArray arr2, jintArray arr3, jint count) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_writeSmrZero( JNIEnv* env, jobject obj) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_overwriteSmrCacheinfo( JNIEnv* env, jobject obj) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_makeSmrCacheinfo( JNIEnv* env, jobject obj) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_mappingInit( JNIEnv* env, jobject obj) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_ridxInit( JNIEnv* env, jobject obj) ;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_sidxMultiGet(JNIEnv *env, jobject obj, jintArray arr) ;
///[SAMPLE]//////////////////////////////////////////////////////////////////////////////////////////////////
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_readMapIdVertexList( JNIEnv* env, jobject thiz ) ;
//JNIEXPORT void JNICALL Java_com_thinkware_smr_jcSMR_getRealMesh( JNIEnv* env, jobject thiz ) ;
JNIEXPORT void JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_getRealMesh( JNIEnv* env, jobject thiz, jint xpos, jint ypos ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_test1( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_test2( JNIEnv* env, jobject thiz, jint map_id ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_test3( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_test4( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_test5( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_test6( JNIEnv* env, jobject thiz ) ;
JNIEXPORT jint JNICALL Java_com_thinkware_smr_smrtestbed_jcSMR_test7( JNIEnv* env, jobject thiz ) ;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifdef __cplusplus
}
#endif



#endif //_SMR_EX_H_
