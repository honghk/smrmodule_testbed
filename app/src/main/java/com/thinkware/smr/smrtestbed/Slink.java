package com.thinkware.smr.smrtestbed;

/**
 * Created by honghk on 2016. 12. 13..
 */

public class Slink {

    int linkId;

    short _startId;             // 시작 노드 ID
    short _endId;               // 종료 노드 ID
    short _length;              // 링크의 길이 (meter)
    short _attr;            	// 링크 속성
    short _numOfVertex;     	// 링크 내 보간점 개수
    short _vertexIndex;     	// 해당 보간점 리스트의 INDEX

    int laneNum;
    int drive;
    int divide;
    int linkType;
    int linkAttr;//
    int roadType;

    SVertex[] sVertex;      //textViewIndex

    public void SVertex(int length){//
        int i = 0;

        sVertex = new SVertex[length];

        for(i = 0; i < length; i++) {
            sVertex[i] = new SVertex();
        }
    }
    //////////// 보간점 ////////////
    class SVertex{
        short offLon;
        short offLat;
        int lon;
        int lat;
    }
    //////////////////////////////
}
