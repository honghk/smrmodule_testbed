package com.thinkware.smr.smrtestbed;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

/**
 * Created by honghk on 2016. 12. 8..
 */

public class HttpDataServer {

    private final static String TAG = "HttpDataServer";

    public final static int SUCCESS                 = 0;
    public final static int CON_TIME_OUT			= 5000;
    public final static int READ_TIME_OUT			= 5000;
    public final static int ERR_HTTP_TCP			= 1000 ;	//소켓 통신 실패
    public final static int ERR_HTTP_EMPTY_DATA		= 1001 ;

    private byte[] m_byte_data = null;

    public byte[] readData() throws InterruptedException{
        return m_byte_data;
    }

    public int connectSmrData(String smr_url){

        int ret = SUCCESS;

        URL url = null;
        URLConnection urlConnection = null;
        InputStream inputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = null;

        try{
            url = new URL(smr_url);
            urlConnection = url.openConnection();

            urlConnection.setConnectTimeout(CON_TIME_OUT);
            urlConnection.setReadTimeout(READ_TIME_OUT);

            urlConnection.setRequestProperty("Accept-Encoding", "gzip, deflate");
            urlConnection.setRequestProperty("Referer", "thinkware.co.kr");
            urlConnection.setRequestProperty("Connection", "keep-alive");

            String cont_enc = urlConnection.getContentEncoding();

            if(cont_enc != null && cont_enc.equalsIgnoreCase("gzip")){
                inputStream = new GZIPInputStream(urlConnection.getInputStream());
            }else{
                inputStream = urlConnection.getInputStream();
            }

            byte[] buf = new byte[8192]; // 8192byte 정도의 입출력 버퍼가 유효
            int len = 0;
            byteArrayOutputStream = new ByteArrayOutputStream();
            while((len = inputStream.read(buf)) > 0){
                byteArrayOutputStream.write(buf, 0, len);
            }

            m_byte_data = byteArrayOutputStream.toByteArray();

            if(byteArrayOutputStream != null){
                byteArrayOutputStream.close();
                byteArrayOutputStream = null;
            }
            if(inputStream != null){
                inputStream.close();
                inputStream = null;
            }
            if(urlConnection != null){
                urlConnection = null;
            }
            if(url != null){
                url = null;
            }

            if((m_byte_data==null) || (m_byte_data.length == 0)){
                Log.e(TAG, "ERR: connect() get empty data") ;
                ret = ERR_HTTP_EMPTY_DATA ;
            }

        }catch(Exception exc){
            Log.e(TAG, "ERR : connectSmrData::Exeption : " + exc);
            ret = ERR_HTTP_TCP;

            try{
                if(byteArrayOutputStream != null){
                    byteArrayOutputStream.close();
                    byteArrayOutputStream = null;
                }
                if(inputStream != null){
                    inputStream.close();
                    inputStream = null;
                }
                if(urlConnection != null){
                    urlConnection = null;
                }
                if(url != null){
                    url = null;
                }
            }catch(IOException ioexc){
                Log.e(TAG, "ERR : connectSmrData::Exeption(close) : " + ioexc);
            }

        }
        return ret;

    }

}
