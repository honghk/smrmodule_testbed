/* ===========================================================================
 *
 *
 * ===========================================================================
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <stdarg.h>
#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
//#include <set>
//#include <map>
#include "sMRman.h"


//#pragma pack(1)

//int g_x = 0 ;

////////////////////////////////////////////////////////////
pthread_rwlock_t smr_rwlock ;
////////////////////////////////////////////////////////////
char m_dir[FILE_LENGTH] ;
FILE *f_smr_manage ;
FILE *f_smr_meshinfo ;
FILE *f_smr_cacheinfo ;
FILE *f_smr_meshdata ;

int m_manage_checksum ;
int m_meshinfo_checksum ;
int m_max_mesh_size ;		//2013.06.27

SMAP_HEADER					m_map_header ;		//µµ¿± °ü¸® Å×ÀÌºí Çì´õ
SRANK_MANAGER				m_rank_manager ;	//µµ¿± °ü¸® Á¤º¸
SRANK_MAP					*m_rank_map ;		//µµ¿± À§Ä¡ Á¤º¸ ·¹ÄÚµå 
SMESH_INFO					*m_mesh_info ;		//µµ¿± ÆÐÅ°Áö Á¤º¸ Å×ÀÌºí

int 						*m_cache_info ;		//µµ¿± cache Á¤º¸ Å×ÀÌºí

REAL_MAPP					*m_mapping ;
////////////////////////////////////////////////////////////
//std::set<int> sidx ;
static int					sidx[SIDX_SIZE+1] ; // map_id
pthread_rwlock_t			sidx_rwlock ;
////////////////////////////////////////////////////////////
R_IDX 						*r_idx ;
int 						r_idx_count ;
////////////////////////////////////////////////////////////

int smr_first = 0 ;	//ÃßÈÄ°ËÅä
int smr_open = 0 ;
////////////////////////////////////////////////////////////////////////////////////////////////
char str_log[150] = "";
////[OUTER SOURCE]//////////////////////////////////////////////////////////////////////////////

char* sMR_get_log()
{
    return str_log;
}

///[ETC]/////////////////////////////////////////////////////////////////

void sMR_init(void)			// only Main One call function
{
	smr_first = 0 ;
	smr_open = 0 ;
}

void sMR_start(void)        // only Main call function
{
	if(!smr_first) {
//		D("HEAR 0\n") ;

		pthread_rwlock_init(&smr_rwlock, NULL) ;
		pthread_rwlock_init(&sidx_rwlock, NULL) ;

		i_smr_create() ; // 변수 초기화
		sidx_clear() ;  //set clear // sidx 모두 -1로 초기화

		smr_first = 1 ; // 시작 되었으므로 1로 설정
	}
	D("sMR_start()") ;
}

void sMR_end(void)          // only Main call function
{
	if(smr_first) {

		i_smr_close() ;  //file close // 파일 모두 fclose
		i_smr_destruct() ;
		sidx_clear() ;  //set clear
		//2013.06.27
		ridx_end() ; //r_idx 초기화

		pthread_rwlock_destroy(&smr_rwlock) ;
		pthread_rwlock_destroy(&sidx_rwlock) ;

		smr_first = 0 ;
	}
	D("sMR_end()\n") ;
}

int sMR_open(char *dir)
{
	int ret = NO_ERR ;
	int flag = 0 ;		
	pthread_rwlock_wrlock(&smr_rwlock);

	if(!smr_open) {
		if(dir != NULL) i_smr_set_dir(dir) ; 	//directory setting
		ret = i_smr_open() ;   	//files open // filename/FILENAME_SMR_MANAGE하여 파일 오픈 
		if(ret == ERR_OPEN_SMR_CACHEINFO) { // chashinfo에러 이면 flag 
			flag = ERR_OPEN_SMR_CACHEINFO ;
		}
		else if(ret) goto error_exit ; // 에러코드 있으면 goto문 타고 아니면 진

		ret = i_smr_read_smr_manage() ; // SMAP_HEADER + SRANK_MANAGER + SRANK_MAP * numOfmesh
		if(ret != NO_ERR) goto error_exit ;

		ret = i_smr_read_smr_meshinfo() ; // SMAP_INFO * numOfmesh
		if(ret != NO_ERR) goto error_exit ;

		ret = i_smr_mapping_init() ; // REAL_MAPP m_mapping 
		if(ret != NO_ERR) goto error_exit ;
		//2013.06.27
		ret = ridx_init(m_max_mesh_size) ; // ridx 초기화
		if(ret != NO_ERR) goto error_exit ;

		ret = i_smr_read_smr_cacheinfo(flag) ; // m_cache_info <- f_smr_cacheinfo(read) 
		if(ret != NO_ERR) goto error_exit ;

		smr_open = 1 ;
	}

error_exit :
	D("sMR_open(%s)->ret[%d]\n", dir, ret) ;
	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

void sMR_open_set(int flag)
{
	pthread_rwlock_wrlock(&smr_rwlock);
	smr_open = flag ;
	pthread_rwlock_unlock(&smr_rwlock);
}

int sMR_open_manage(void) // r옵션으로 fopen
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	if(!smr_open) 
		ret = i_smr_reopen_manage() ;

	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

int sMR_open_meshinfo(void)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	if(!smr_open) 
		ret = i_smr_reopen_meshinfo() ;
	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

int sMR_open_meshdata(void)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	if(!smr_open) 
		ret = i_smr_reopen_meshdata() ;

	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

int sMR_open_cacheinfo(void)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	if(!smr_open) 
		ret = i_smr_reopen_cacheinfo() ;

	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

void sMR_close_manage(void) 
{
	pthread_rwlock_wrlock(&smr_rwlock);
	i_smr_close_manage() ;
	pthread_rwlock_unlock(&smr_rwlock);
}

void sMR_close_meshinfo(void) 
{
	pthread_rwlock_wrlock(&smr_rwlock);
	i_smr_close_meshinfo() ;
	pthread_rwlock_unlock(&smr_rwlock);
}	

void sMR_close_cacheinfo(void) 
{	
	pthread_rwlock_wrlock(&smr_rwlock);
	i_smr_close_cacheinfo() ;
	pthread_rwlock_unlock(&smr_rwlock);
}

void sMR_close_meshdata(void) 
{
	pthread_rwlock_wrlock(&smr_rwlock);
	i_smr_close_meshdata() ;
	pthread_rwlock_unlock(&smr_rwlock);
}

void sMR_close(void)
{
	pthread_rwlock_wrlock(&smr_rwlock);

	if(smr_open) {

	//	i_smr_mapping_clear() ;
		i_smr_close() ;  //file close
		i_smr_destruct() ; // 구조체, 체크섬, dirName 초기화
		sidx_clear() ;  //sidx -1로 memset
		//2013.06.27
		ridx_end() ; // r_idx 초기화
		
		smr_open = 0 ;
	}

	D("sMR_close()\n") ;

	pthread_rwlock_unlock(&smr_rwlock);
}

int sMR_file_exist(char *fileName)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	ret = i_smr_file_exist(fileName) ;

	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

int sMR_file_delete(char *fileName)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	ret = i_smr_file_delete(fileName) ;

	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

int sMR_delete(void)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	if(smr_open) {
		D("ERR: smr open(%d)\n", smr_open) ;
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_file_exist(const_cast<char *>(FILENAME_SMR_MANAGE)) ;
	if(ret == 0)  {
		ret = i_smr_file_delete(const_cast<char *>(FILENAME_SMR_MANAGE)) ;
		if(ret != NO_ERR) goto error_exit ;
	}
	ret = i_smr_file_exist(const_cast<char *>(FILENAME_SMR_MESHINFO)) ;
	if(ret == 0) {
		ret = i_smr_file_delete(const_cast<char *>(FILENAME_SMR_MESHINFO)) ;
		if(ret != NO_ERR) goto error_exit ;
	}
	ret = i_smr_file_exist(const_cast<char *>(FILENAME_SMR_MESHDATA)) ;
	if(ret == 0) {
		ret = i_smr_file_delete(const_cast<char *>(FILENAME_SMR_MESHDATA)) ;
		if(ret != NO_ERR) goto error_exit ;
	}
	ret = i_smr_file_exist(const_cast<char *>(FILENAME_SMR_CACHEINFO)) ;
	if(ret == 0) {
		ret = i_smr_file_delete(const_cast<char *>(FILENAME_SMR_CACHEINFO)) ;
		if(ret != NO_ERR) goto error_exit ;
	}

error_exit:
	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

///[READ]////////////////////////////////////////////////////////////////

int sMR_read_smr_manage(void)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	if(!smr_open) 
		ret = i_smr_read_smr_manage() ; // SMAP_HEADER + SRANK_MANAGER + SRANK_MAP * numOfmesh

	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

int sMR_read_smr_meshinfo(void)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	if(!smr_open) 
		ret = i_smr_read_smr_meshinfo() ;// SMAP_INFO * numOfmesh

	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

int sMR_read_smr_cacheinfo(int flag)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock);

	if(!smr_open) 
		ret = i_smr_read_smr_cacheinfo(flag) ; // m_cache_info <- f_smr_cacheinfo(read) // 마지막 배열 체크섬

	pthread_rwlock_unlock(&smr_rwlock);
	return ret ;
}

///[READ]////////////////////////////////////////////////////////////////
int sMR_read_node_count(int map_id, int *node_count)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) { // smr이 열려있는지
		ret = ERR_INVALID_OPERATION ;
		sprintf(str_log, "ERR: sMR_read_node_count::ERR_INVALID_OPERATION\n");
		goto error_exit ;
	}

	ret = i_smr_read_node_count(map_id, node_count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_link_count(int map_id, int *link_count)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		sprintf(str_log, "sMR_read_link_count::ERR_INVALID_OPERATION\n");
		goto error_exit ;
	}

	ret = i_smr_read_link_count(map_id, link_count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_mesh_checksum(int map_id, int *check_sum)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_mesh_checksum(map_id, check_sum) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_node_list(int map_id, int node_count, SNODE *s_node_list)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		sprintf(str_log, "sMR_read_node_list::ERR_INVALID_OPERATION\n");
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_node_list(map_id, node_count, s_node_list) ; // 파라메타 확

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_node(int map_id, int node_id, SNODE *s_node)
{

	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	D("ERR: sMR_read_node::i_smr_exist_smesh_info ret[%d] : \n", ret) ;
	if(ret == NO_ERR)
		ret = i_smr_read_node(map_id, node_id, s_node) ;
        D("ERR: sMR_read_node::i_smr_read_node ret[%d] : \n", ret) ;
error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_crossnode(int map_id, SNODE *s_node, SCROSSNODE *s_crossnode)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_crossnode(map_id, s_node, s_crossnode) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_crossnode(int map_id, int node_id, SCROSSNODE *s_crossnode, int *s_crossnode_count)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_crossnode(map_id, node_id, s_crossnode, s_crossnode_count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_adjnode(int map_id, SNODE *s_node, SADJNODE *s_adjnode)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_adjnode(map_id, s_node, s_adjnode) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_adjnode(int map_id, int node_id, SADJNODE *s_adjnode)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_adjnode(map_id, node_id, s_adjnode) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_link(int map_id, int link_id, SLINK *s_link)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		sprintf(str_log, "sMR_read_link::ERR_INVALID_OPERATION\n");
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_link(map_id, link_id, s_link) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_link_list(int map_id, int link_count, SLINK *s_link_list)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
	    sprintf(str_log, "sMR_read_link_list::ERR_INVALID_OPERATION\n");
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_link_list(map_id, link_count, s_link_list) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_mapid_vertex_list(int map_id, SVERTEX **s_vertex_list, int *vertex_count)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_mapid_vertex_list(map_id, s_vertex_list, vertex_count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

//[2013.06.24 BEGIN]/////////
int sMR_read_mapid_vertex_count(int map_id, int *vertex_count)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_mapid_vertex_count(map_id, vertex_count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_mapid_vertex_data(int map_id, SVERTEX *vertex_list)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_mapid_vertex_data(map_id, vertex_list) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}
//[2013.06.24  END ]/////////

int sMR_read_vertex_list(int map_id, int link_id, SVERTEX **s_vertex_list, int *vertex_count)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
	    sprintf(str_log, "sMR_read_vertex_list::ERR_INVALID_OPERATION\n");
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_vertex_list(map_id, link_id, s_vertex_list, vertex_count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

//[2013.06.24 BEGIN]/////////
int sMR_read_vertex_count(int map_id, int link_id, int *vertex_count)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_vertex_count(map_id, link_id, vertex_count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_vertex_data(int map_id, int link_id, SVERTEX *vertex_list)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_vertex_data(map_id, link_id, vertex_list) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}
//[2013.06.24  END ]/////////

int	sMR_read_start_end_vertex_list(int map_id, int link_id, SVERTEX **s_vertex_list, int *vertex_count) 
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_start_end_vertex_list(map_id, link_id, s_vertex_list, vertex_count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}
//[2013.06.24 BEGIN]/////////
int	sMR_read_start_end_vertex_count(int map_id, int link_id, int *vertex_count) 
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_start_end_vertex_count(map_id, link_id, vertex_count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int	sMR_read_start_end_vertex_data(int map_id, int link_id, SVERTEX *vertex_list) 
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if(ret == NO_ERR)
		ret = i_smr_read_start_end_vertex_data(map_id, link_id, vertex_list) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}
//[2013.06.24  END ]/////////

int sMR_read_mesh(int map_id, char *buf, int buf_size)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if((ret == NO_ERR) || (ret == ERR_EMPTY_DATA))      //empty data¶óµµ ÀÐ´Â´Ù.
		ret = i_smr_read_mesh(map_id, buf, buf_size) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}

int sMR_read_mesh(int map_id, char *buf, int ds_mesh, int buf_size)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_exist_smesh_info(map_id) ;
	if((ret == NO_ERR) || (ret == ERR_EMPTY_DATA))      //empty data¶óµµ ÀÐ´Â´Ù.
		ret = i_smr_read_mesh(map_id, buf, ds_mesh, buf_size) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND)
		sidx_insert(map_id) ;

	return ret ;
}


///[WRITE]///////////////////////////////////////////////////////////////
int sMR_write_smr_manage(char *buf, int buf_size, int *file_size)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	ret = i_smr_write_smr_manage(buf, buf_size, file_size) ; // arr로 넘어온 내용 smrmanage에 쓰
	if(ret != NO_ERR) goto error_exit ;
	ret = i_smr_reopen_manage() ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;
	return ret ;
}

int sMR_write_smr_meshinfo(char *buf, int buf_size, int *file_size)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	ret = i_smr_write_smr_meshinfo(buf, buf_size, file_size) ;
	if(ret != NO_ERR) goto error_exit ;
	ret = i_smr_reopen_meshinfo() ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	return ret ;
}

int sMR_write_smr_cacheinfo(char *buf, int buf_size, int *file_size)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	ret = i_smr_write_smr_cacheinfo(buf, buf_size, file_size) ;
	if(ret != NO_ERR) goto error_exit ;
	ret = i_smr_reopen_cacheinfo() ;       //cacheinfo ´Ù½Ã ¿¬´Ù.

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	return ret ;
}

int sMR_overwrite_smr_cacheinfo(void)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	ret = i_smr_overwrite_smr_cacheinfo() ; // m_cache_info마지막 주소값에 m_cache_info체크섬을 넣어줌
	if(ret != NO_ERR) goto error_exit ;
	ret = i_smr_reopen_cacheinfo() ;       //cacheinfo ´Ù½Ã ¿¬´Ù. // 다시 fopen r옵션으로 열어줌

error_exit :
	pthread_rwlock_unlock(&smr_rwlock) ;

	return ret ;
}

int sMR_make_smr_cacheinfo(void)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	ret = i_smr_make_smr_cacheinfo() ;
	if(ret != NO_ERR) goto error_exit ;
	ret = i_smr_reopen_cacheinfo() ;       //cacheinfo ´Ù½Ã ¿¬´Ù.

error_exit :
	pthread_rwlock_unlock(&smr_rwlock) ;

	return ret ;
}

int sMR_write_mesh(char *buf, int buf_size, int *file_end, int *file_size)
{ //sMR_write_mesh(NULL, 0, &file_end, &file_size) ;
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	ret = i_smr_write_mesh(buf, buf_size, file_end, file_size) ;
	if(ret != NO_ERR) goto error_exit ;
	ret = i_smr_reopen_meshdata() ;	// meshdata ´Ù½Ã ¿¬´Ù.¤

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;
	return ret ;
}

void remove_item(int idx, char *buf, int *buf_size, int *p_map_id, int *p_offset, int *p_offset_cnt)
{ // remove_item(      i,       buf,     &buf_size,       get_idx,     get_offset,        &count) ;
	int remove_size = 0 ;
	int moving_size = 0 ;
	int offset = 0 ;
	int i = 0 ;
	char *ptr1 = NULL ;
	char *ptr2 = NULL ;
	int temp_idx = 0 ;
// remove_item(i, buf, &buf_size, get_idx, get_offset, &count) ;
	int init_size = *buf_size ;
	int init_cnt = *p_offset_cnt ;

	offset = p_offset[idx] ;
//
	if(idx == (init_cnt - 1)) {     //last idx
		remove_size = init_size - p_offset[init_cnt - 1] ; // init_cnt = *p_offset_cnt ; 
	}
	else {
		remove_size = p_offset[idx+1] - p_offset[idx] ;
	}
	moving_size = (init_size - p_offset[idx]) - remove_size ;

//	D("DEBUG: remove_item() ==> init_size[%d]:remove_size[%d] moving_size[%d]\n",
//			init_size, remove_size, moving_size) ;

	ptr1 = &buf[offset] ;
	ptr2 = &buf[offset+remove_size] ;

	memcpy(ptr1, ptr2, moving_size) ;

	temp_idx = idx ;
	for(i = 0 ; i < (init_cnt - idx - 1) ; i++) {
		p_offset[temp_idx] = p_offset[temp_idx+1] - remove_size ;
		p_map_id[temp_idx] = p_map_id[temp_idx+1] ;
//		D("DEBUG: [%d] map_id[%d] offset[%d]\n", i, p_map_id[temp_idx], p_offset[temp_idx]) ;
		temp_idx += 1 ;
	}

	*p_offset_cnt = (init_cnt - 1) ;
	*buf_size = (init_size - remove_size) ;
}

int sMR_write_meshdata(char *buf, int buf_size, int *file_end, int *file_size,
		int *get_idx, int *get_offset, int count)
{//sMR_write_meshdata((char *buf1, len1, &file_end, &file_size,   buf2,          buf3,       (int)count) ;
	            //jbyteArray arr1,                         jintArray arr2, jintArray arr3, jint count)
	int ret = NO_ERR ;
	int i = 0 ;
	int offset = 0 ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	///2013.07.09 UPDATE START//
	if((buf == NULL) || (buf_size <= 0) || (get_idx == NULL) || (get_offset == NULL) || (count <= 0)) {
		D("ERR: parma is invaild\n") ;
		sprintf(str_log, "sMR_write_meshdata::ERR: parma is invaild\n") ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

	for(i = 0 ; i < count ; ) { // 이미 존재 한다면 
		ret = i_smr_exist_smesh_info(get_idx[i]) ;
		if((ret == NO_ERR) || (ret == ERR_EMPTY_DATA)) {	// if m_cache info [map_id] exist
			D("ERR: [%d] map_id[%d] is exist\n", i, get_idx[i]) ;/////////// NO ERR /////
			remove_item(i, buf, &buf_size, get_idx, get_offset, &count) ;
			continue ;
		}
		i++ ;
	}
	///2013.07.09 UPDATE END//

	//mesh data write
	ret = i_smr_write_mesh(buf, buf_size, file_end, file_size) ;
	if(ret != NO_ERR) goto error_exit ;
	ret = i_smr_reopen_meshdata() ;	// meshdata ´Ù½Ã ¿¬´Ù.¤

	//¸Þ¸ð¸® cache¿¡ ¹Ý¿µ
	for(i = 0 ; i < count ; i++) {
		offset = *file_end + get_offset[i] ;
		i_smr_set_smesh_info(get_idx[i], offset) ;
	}
	//¸Þ¸ð¸® cache => file
	ret = i_smr_overwrite_smr_cacheinfo() ;
	if(ret != NO_ERR) goto error_exit ;
	// cache file open
	ret = i_smr_reopen_cacheinfo() ;
	//sidx delete
	sidx_multi_delete(get_idx, count) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;
	return ret ;
}



int sMR_write_smr_zero(void)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock) ;
	i_smr_close() ;      //±âÁ¸ ÀÏ±â¸ðµå ÆÄÀÏ ´Ý°í
	ret = i_smr_write_smr_zero() ;
	i_smr_open() ;       //´Ù½Ã ÀÐ±â¸ðµå·Î ¿­°í
	pthread_rwlock_unlock(&smr_rwlock) ;

	return ret ;
}

///[SET]/////////////////////////////////////////////////////////////////
int sMR_set_dir(char *dirPath)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;
	ret = i_smr_set_dir(dirPath) ;
	pthread_rwlock_unlock(&smr_rwlock) ;

	return ret ;
}

int sMR_set_smesh_info(int map_id, int ds)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	ret = i_smr_exist_smesh_info(map_id) ;

	if(ret == ERR_NOT_FOUND) {
		ret = i_smr_set_smesh_info(map_id, ds) ;
	}
	pthread_rwlock_unlock(&smr_rwlock) ;

	return ret ;
}

///[GET]/////////////////////////////////////////////////////////////////

int sMR_mapping_init(void) 
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock) ;

	ret = i_smr_mapping_init() ; 

	pthread_rwlock_unlock(&smr_rwlock) ;
	return ret ;
}

int sMR_ridx_init(void) 
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock) ;
	if(m_max_mesh_size <= 0) {
		ret = ERR_UNKNOWN ;
		D("ERR: m_max_mesh_size(%d)\n", m_max_mesh_size) ;
		sprintf(str_log, "sMR_ridx_init::ERR: m_max_mesh_size(%d)\n", m_max_mesh_size) ;
		goto error_exit ;
	}

	ret = ridx_init(m_max_mesh_size) ; // r_idx 초기화
error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;
	return ret ;
}

int sMR_get_real_mesh(int xpos, int ypos, REAL_MESH_INFO *real_mesh_list, int *count) 
{// sMR_get_real_mesh(    xpos,     ypos,                           info,     &count) ;
	int ret = NO_ERR ;
	int i = 0 ;
	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		goto error_exit ;
	}

	ret = i_smr_get_real_mesh(xpos, ypos, real_mesh_list, count) ;
	if(ret) goto error_exit ;

	for(i = 0 ; i < *count ; i++) {
		if(real_mesh_list[i].meshId > -1) {
			if(real_mesh_list[i].flag == ERR_NOT_FOUND) {
				sidx_insert(real_mesh_list[i].meshId) ;
			}
		}
	}
error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;
	return ret ;
}

void sMR_get_now_fetch_count(int *fetch_count)
{
	pthread_rwlock_wrlock(&smr_rwlock) ;
	i_smr_get_now_fetch_count(fetch_count) ;
	pthread_rwlock_unlock(&smr_rwlock) ;
}

int sMR_get_srank_map(int map_id, SRANK_MAP *srank_map)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		sprintf(str_log, "sMR_get_srank_map::ERR_INVALID_OPERATION\n");
		goto error_exit ;
	}

	ret = i_smr_get_srank_map(map_id, srank_map) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;
	return ret ;
}

int sMR_get_smesh_info(int map_id, SMESH_INFO *smesh_info)
{
	int ret = NO_ERR ;

	pthread_rwlock_wrlock(&smr_rwlock) ;

	if(!smr_open) {
		ret = ERR_INVALID_OPERATION ;
		sprintf(str_log, "sMR_get_smesh_info::ERR_INVALID_OPERATION\n");
		goto error_exit ;
	}

	ret = i_smr_get_smesh_info(map_id, smesh_info) ;

error_exit:
	pthread_rwlock_unlock(&smr_rwlock) ;

	if(ret == ERR_NOT_FOUND) {
		sidx_insert(map_id) ;
	}

	return ret ;
}

void sMR_get_smap_header(SMAP_HEADER *smap_header)
{
	pthread_rwlock_wrlock(&smr_rwlock) ;
	i_smr_get_smap_header(smap_header) ;
	pthread_rwlock_unlock(&smr_rwlock) ;
}

void sMR_get_srank_manager(SRANK_MANAGER *srank_manager)
{
	pthread_rwlock_wrlock(&smr_rwlock) ;
	i_smr_get_srank_manager(srank_manager) ;
	pthread_rwlock_unlock(&smr_rwlock) ;
}

void sMR_get_version(int *version)
{
	pthread_rwlock_wrlock(&smr_rwlock) ;
	i_smr_get_version(version) ;
	pthread_rwlock_unlock(&smr_rwlock) ;
}

////[SIDX SOURCE]///////////////////////////////////////////////////////////////////////////////
int sidx_insert(int in) // map_id
{
	int ret = NO_ERR ;
	int i = 0 ;
	pthread_rwlock_wrlock(&sidx_rwlock);

//	if(sidx.size() > SIDX_SIZE) {
//		D("ERR: limit\n") ;
//		ret = ERR_LIMIT ;
//		goto error_exit ;
//	}
//	sidx.insert(in) ;
	if(in < 0) {
	    sprintf(str_log, "sidx_insert::ERR_BAD_PARAM\n");
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

	if(sidx[SIDX_SIZE-1] > 0) {
	    sprintf(str_log, "sidx_insert::ERR_LIMIT\n");
		ret = ERR_LIMIT ;
		goto error_exit ;
	}

	if(m_rank_manager.numOfMesh < in) {
	    sprintf(str_log, "sidx_insert::ERR_BAD_PARAM : input map_id[%d]: max map_id[%d]\n", in, m_rank_manager.numOfMesh );
	    ret = ERR_BAD_PARAM ;
	    goto error_exit;
	}

	for(i = 0 ; i < SIDX_SIZE ; i++) {
		if(sidx[i] == in) break ;	//2013.06.20 update

		if(sidx[i] < 0) { 
			sidx[i] = in ;
			D("sidx[%d] : %d\n",i , sidx[i]) ;
			break ;
		}
	}

error_exit :
	pthread_rwlock_unlock(&sidx_rwlock);
	return ret ;
}

int sidx_cmp(int *a, int *b) { return ((*b) - (*a)) ; }

int sidx_delete(int in)
{
	int ret = NO_ERR ;
	int i = 0 ;
	pthread_rwlock_wrlock(&sidx_rwlock);

//	sidx.erase(in) ;
	for(i = 0 ; i < SIDX_SIZE ; i++) {
		if(sidx[i] == in) sidx[i] = -1 ;
	}

	qsort(sidx, SIDX_SIZE, sizeof(int), (int (*)(const void *, const void *)) sidx_cmp) ;

	pthread_rwlock_unlock(&sidx_rwlock);
	return ret ;
}

int sidx_multi_insert(int *in_list, int count) // malloc으로 처리 가능한지 분석
{
	int ret = NO_ERR ;
	int i = 0 ;
	int j = 0 ;

	pthread_rwlock_wrlock(&sidx_rwlock);

//	for(i = 0 ; i < count ; i++) {
//		if(sidx.size() > SIDX_SIZE) {
//			D("ERR: limit\n") ;
//			ret = ERR_LIMIT ;
//			goto error_exit ;
//		}
//		sidx.insert(in_list[i]) ;
//	}

	for(j = 0 ; j < count ; j++) {
		if(sidx[SIDX_SIZE-1] > 0) {
			ret = ERR_LIMIT ;
			goto error_exit ;
		}

		for(i = 0 ; i < SIDX_SIZE ; i++) {
			if(sidx[i] == in_list[j]) break ; 	// 2013.06.20 update

			if(sidx[i] < 0) {
				sidx[i] = in_list[j] ;
				break ;
			}
		}
	}

error_exit :
	pthread_rwlock_unlock(&sidx_rwlock);
	return ret ;

}

int sidx_multi_get(int **out_list, int *count)
{
	int ret = NO_ERR ;
	int flag = 0 ;
	int i = 0 ;
	int n = 0 ;
	int cnt = 0 ;
	int *temp_list = NULL ;
	
	int	temp_sidx[SIDX_SIZE+1] ; // SIDX_SIZE 50

	pthread_rwlock_wrlock(&sidx_rwlock);

	memcpy(temp_sidx, sidx, sizeof(sidx)) ; 

	pthread_rwlock_unlock(&sidx_rwlock);


	for(i = 0 ; i < SIDX_SIZE ; i++) { // 0~49 
		if(temp_sidx[i] >= 0) cnt++ ;
		else break ;
	}
//	printf("debug cnt[%d]\n", cnt) ;

	if((temp_list = (int *) malloc (sizeof(int) * cnt)) == NULL) { // 할당과 동시에 예외처리
		D("ERR: memory alloc fail\n") ;
		sprintf(str_log, "sidx_multi_get::ERR: memory alloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(temp_list, 0, sizeof(int) * cnt) ; // 초기화 

	pthread_rwlock_wrlock(&smr_rwlock) ;	// 2013.06.20 update

	n = 0 ;
	for(i = 0 ; i < cnt ; i++) {
		flag = i_smr_exist_smesh_info(temp_sidx[i]) ; // m_cache_info[ temp_sidx[i] ] < 0 
		if(flag == ERR_NOT_FOUND) {
			temp_list[n] = temp_sidx[i] ;
			D("debug sidx_multi_get::받아올 sidx[%d] = %d \n", n, temp_list[n]) ;
			n++ ;
		}
	}

	pthread_rwlock_unlock(&smr_rwlock) ;	// 2013.06.20 update

	*count = n ;
	*out_list = temp_list ;

error_exit :
	return ret ;
}

int sidx_multi_delete(int *in_list, int count)
{
	int ret = NO_ERR ;

	int i = 0 ;
	int j = 0 ;

	pthread_rwlock_wrlock(&sidx_rwlock);

//	for(i = 0 ; i < count ; i++)
//		sidx.erase(in_list[i]) ;

	for(j = 0 ; j < count ; j++) {
		for(i = 0 ; i < SIDX_SIZE ; i++) {
			if(in_list[j] == sidx[i]) sidx[i] = -1 ;
		}
	}
	qsort(sidx, SIDX_SIZE, sizeof(int), (int (*)(const void *, const void *)) sidx_cmp) ;

	pthread_rwlock_unlock(&sidx_rwlock);
	return ret ;
}

int sidx_init(void) 
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&sidx_rwlock);

	memset(sidx, -1, sizeof(sidx)) ;

	pthread_rwlock_unlock(&sidx_rwlock);
	return ret ;
}

int sidx_clear(void)
{
	int ret = NO_ERR ;
	pthread_rwlock_wrlock(&sidx_rwlock);

//	sidx.clear() ;
	memset(sidx, -1, sizeof(sidx)) ;

	pthread_rwlock_unlock(&sidx_rwlock);
	return ret ;
}

////[INNER SOURCE]///////////////////////////////////////////////////////////////////////////////

int i_smr_get_real_width_count(void) 
{
	return (m_rank_manager.right - m_rank_manager.left) / m_rank_manager.meshWidth ;
}

int i_smr_get_real_height_count(void) 
{
	return (m_rank_manager.top - m_rank_manager.bottom) / m_rank_manager.meshHeight ;
}

int i_smr_mapping_cmp(REAL_MAPP *a, REAL_MAPP *b)
{
	if(a->realMeshId > b->realMeshId) return 1 ;
	else if(a->realMeshId < b->realMeshId) return -1 ;
	else return 0 ;
}

int i_smr_mapping_init(void)
{
	int ret = NO_ERR ;
	int i = 0 ;

	int width_count = 0 ;
	int height_count = 0 ;
	int real_meshid = 0 ;
//	std::map<int, int>::iterator it ; //debug

	width_count = i_smr_get_real_width_count() ; // 가로 갯수( 오른쪽 - 왼쪽 / 넓이 )
	height_count = i_smr_get_real_height_count() ;

//	m_mapping.clear() ;
//	m_mapping.reserve(m_rank_manager.numOfMesh) ;
	if((m_mapping = (REAL_MAPP *) malloc (m_rank_manager.numOfMesh * sizeof(REAL_MAPP))) == NULL) {
		D("ERR: memory alloc fail\n") ;
		sprintf(str_log, "i_smr_mapping_init::ERR: memory alloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(m_mapping, 0, sizeof(REAL_MAPP) * m_rank_manager.numOfMesh) ;

	for(i = 0 ; i < m_rank_manager.numOfMesh ; i++) {

		real_meshid = ( (m_rank_map[i].left - m_rank_manager.left) / m_rank_manager.meshWidth)
			+ ( (m_rank_map[i].bottom - m_rank_manager.bottom) / m_rank_manager.meshHeight) * width_count ;
	//	D("[%d] left[%d] bottom[%d]=>real[%d]\n", i, m_rank_map[i].left, m_rank_map[i].bottom, real_meshid) ;

	//	m_mapping.insert(std::pair<int, int>(real_meshid, i)) ;
		m_mapping[i].realMeshId = real_meshid ; // 솔트 기준
		m_mapping[i].meshId = i ;
	}

//	for(i = 0 ; i < m_rank_manager.numOfMesh ; i++) {
//		D("[%d] left[%d] bottom[%d]=>real[%d] mesh_idx[%d]\n", 
//			i, m_rank_map[i].left, m_rank_map[i].bottom, m_mapping[i].realMeshId, m_mapping[i].meshId) ;
//	}
 
	qsort(m_mapping, m_rank_manager.numOfMesh, 
			sizeof(REAL_MAPP), (int (*)(const void *, const void *)) i_smr_mapping_cmp) ;
	//DEBUG
//	D("=================================================================\n") ;
//	for(i = 0 ; i < m_rank_manager.numOfMesh ; i++) {
//		D("m_mapping[%d]-> realMeshId[%d] meshId[%d]\n", i, m_mapping[i].realMeshId, m_mapping[i].meshId) ;
//	}


error_exit: 
	return ret ;
}

int i_smr_mapping_clear(void)
{
	if(m_mapping != NULL) free(m_mapping) ;
	m_mapping = NULL ;
	return NO_ERR ;
}

int i_smr_get_real_mesh(int xpos, int ypos, REAL_MESH_INFO *real_mesh_list, int *count)
{
	int ret = NO_ERR ;
	int i = 0 ;
	int cnt = 0 ;
	int width_count = 0 ;
	int height_count = 0 ;
	int real_meshid = 0 ;
	int max_mesh = 0 ;
	int last_row_startmesh = 0 ;
//	std::map<int, int>::iterator it ;
	REAL_MAPP *ptr = NULL ;
	REAL_MAPP temp ;
	REAL_MESH_INFO temp_mesh_list[9] ;

	int is_first_col = 0 ;
	int is_last_col = 0 ;
	int is_first_row = 0 ;
	int is_last_row = 0 ;

	SMESH_INFO smesh_info ;

	if(( m_rank_manager.left > xpos ) || ( xpos >= m_rank_manager.right) 
			|| ( m_rank_manager.bottom > ypos ) || ( ypos >= m_rank_manager.top)) {
		D("ERR: param error(x:%d)(y:%d)\n", xpos, ypos) ;
		return  ERR_BAD_PARAM ;
	}

	width_count = i_smr_get_real_width_count() ; // (오른쪽-왼쪽 / 넓이) 메쉬 전체
	height_count = i_smr_get_real_height_count() ; //메쉬 전체의 상단 하단

	real_meshid = ((xpos - m_rank_manager.left)/m_rank_manager.meshWidth)
		+ ((ypos - m_rank_manager.bottom)/m_rank_manager.meshHeight)*width_count ;

	max_mesh = width_count * height_count ;
	last_row_startmesh = width_count * (height_count - 1) ;

	if((real_meshid % width_count) == 0 ) is_first_col = 1 ;
	if(((real_meshid + 1) % width_count) == 0) is_last_col = 1 ; 
	if((0 <= real_meshid) && ( real_meshid < width_count)) is_first_row = 1 ;
	if((last_row_startmesh <= real_meshid) && (real_meshid < max_mesh)) is_last_row = 1 ;

	memset(temp_mesh_list, 0, sizeof(REAL_MESH_INFO) * 9) ;

//	D("real_meshid[%d]==>first_col[%d] last_col[%d] first_row[%d] last_row[%d]\n", 
//		real_meshid, is_first_col, is_last_col, is_first_row, is_last_row) ;

	if(is_first_col == 1) {
		if(is_first_row == 1) {
			temp_mesh_list[0].realMeshId = -1, temp_mesh_list[0].meshId = -1 ;
			temp_mesh_list[1].realMeshId = -1, temp_mesh_list[1].meshId = -1  ;
			temp_mesh_list[2].realMeshId = -1, temp_mesh_list[2].meshId = -1  ;
			temp_mesh_list[3].realMeshId = -1, temp_mesh_list[3].meshId = -1  ;
			temp_mesh_list[4].realMeshId = real_meshid ;
			temp_mesh_list[5].realMeshId = real_meshid + 1;
			temp_mesh_list[6].realMeshId = -1, temp_mesh_list[6].meshId = -1  ;
			temp_mesh_list[7].realMeshId = real_meshid + (1 * width_count) ;
			temp_mesh_list[8].realMeshId = real_meshid + (1 * width_count) + 1 ;
			cnt = 9 ;
		}
		else if(is_last_row == 1) {
			temp_mesh_list[0].realMeshId = -1, temp_mesh_list[0].meshId = -1 ;
			temp_mesh_list[1].realMeshId = real_meshid - (1 * width_count) ;
			temp_mesh_list[2].realMeshId = real_meshid - (1 * width_count) + 1 ;
			temp_mesh_list[3].realMeshId = -1, temp_mesh_list[0].meshId = -1 ;
			temp_mesh_list[4].realMeshId = real_meshid ;
			temp_mesh_list[5].realMeshId = real_meshid + 1;
			temp_mesh_list[6].realMeshId = -1, temp_mesh_list[6].meshId = -1 ;
			temp_mesh_list[7].realMeshId = -1, temp_mesh_list[7].meshId = -1 ;
			temp_mesh_list[8].realMeshId = -1, temp_mesh_list[8].meshId = -1 ;
			cnt = 9 ;
		}
		else {
			temp_mesh_list[0].realMeshId = -1, temp_mesh_list[0].meshId = -1 ;
			temp_mesh_list[1].realMeshId = real_meshid - (1 * width_count) ;
			temp_mesh_list[2].realMeshId = real_meshid - (1 * width_count) + 1 ;
			temp_mesh_list[3].realMeshId = -1, temp_mesh_list[3].meshId = -1 ;
			temp_mesh_list[4].realMeshId = real_meshid ;
			temp_mesh_list[5].realMeshId = real_meshid + 1;
			temp_mesh_list[6].realMeshId = -1, temp_mesh_list[6].meshId = -1 ;
			temp_mesh_list[7].realMeshId = real_meshid + (1 * width_count) ;
			temp_mesh_list[8].realMeshId = real_meshid + (1 * width_count) + 1 ;
			cnt = 9 ;
		}
	}
	else if(is_last_col == 1) {
		if(is_first_row == 1) {
			temp_mesh_list[0].realMeshId = -1, temp_mesh_list[0].meshId = -1 ;
			temp_mesh_list[1].realMeshId = -1, temp_mesh_list[1].meshId = -1 ;
			temp_mesh_list[2].realMeshId = -1, temp_mesh_list[2].meshId = -1 ;
			temp_mesh_list[3].realMeshId = real_meshid - 1;
			temp_mesh_list[4].realMeshId = real_meshid ;
			temp_mesh_list[5].realMeshId = -1, temp_mesh_list[5].meshId = -1 ;
			temp_mesh_list[6].realMeshId = real_meshid + (1 * width_count) - 1 ;
			temp_mesh_list[7].realMeshId = real_meshid + (1 * width_count) ;
			temp_mesh_list[8].realMeshId = -1, temp_mesh_list[8].meshId = -1 ;
			cnt = 9 ;
		}
		else if(is_last_row == 1) {
			temp_mesh_list[0].realMeshId = real_meshid - (1 * width_count) - 1 ;
			temp_mesh_list[1].realMeshId = real_meshid - (1 * width_count) ;
			temp_mesh_list[2].realMeshId = -1, temp_mesh_list[2].meshId = -1 ;
			temp_mesh_list[3].realMeshId = real_meshid - 1;
			temp_mesh_list[4].realMeshId = real_meshid ;
			temp_mesh_list[5].realMeshId = -1, temp_mesh_list[5].meshId = -1 ;
			temp_mesh_list[6].realMeshId = -1, temp_mesh_list[6].meshId = -1 ;
			temp_mesh_list[7].realMeshId = -1, temp_mesh_list[7].meshId = -1 ;
			temp_mesh_list[8].realMeshId = -1, temp_mesh_list[8].meshId = -1 ;
			cnt = 9 ;
		}
		else {
			temp_mesh_list[0].realMeshId = real_meshid - (1 * width_count) - 1 ;
			temp_mesh_list[1].realMeshId = real_meshid - (1 * width_count) ;
			temp_mesh_list[2].realMeshId = -1, temp_mesh_list[2].meshId = -1 ;
			temp_mesh_list[3].realMeshId = real_meshid - 1 ;
			temp_mesh_list[4].realMeshId = real_meshid ;
			temp_mesh_list[5].realMeshId = -1, temp_mesh_list[5].meshId = -1 ;
			temp_mesh_list[6].realMeshId = real_meshid + (1 * width_count) - 1 ;
			temp_mesh_list[7].realMeshId = real_meshid + (1 * width_count) ;
			temp_mesh_list[8].realMeshId = -1, temp_mesh_list[8].meshId = -1 ;
			cnt = 9 ;
		}
	}
	else {
		if(is_first_row == 1) {
			temp_mesh_list[0].realMeshId = -1, temp_mesh_list[0].meshId = -1 ;
			temp_mesh_list[1].realMeshId = -1, temp_mesh_list[1].meshId = -1 ;
			temp_mesh_list[2].realMeshId = -1, temp_mesh_list[2].meshId = -1 ;
			temp_mesh_list[3].realMeshId = real_meshid - 1 ;
			temp_mesh_list[4].realMeshId = real_meshid ;
			temp_mesh_list[5].realMeshId = real_meshid + 1 ;
			temp_mesh_list[6].realMeshId = real_meshid + (1 * width_count) - 1 ;
			temp_mesh_list[7].realMeshId = real_meshid + (1 * width_count) ;
			temp_mesh_list[8].realMeshId = real_meshid + (1 * width_count) + 1 ;
			cnt = 9 ;
		}
		else if(is_last_row == 1) {
			temp_mesh_list[0].realMeshId = real_meshid - (1 * width_count) - 1 ;
			temp_mesh_list[1].realMeshId = real_meshid - (1 * width_count) ;
			temp_mesh_list[2].realMeshId = real_meshid - (1 * width_count) + 1 ;
			temp_mesh_list[3].realMeshId = real_meshid - 1 ;
			temp_mesh_list[4].realMeshId = real_meshid ;
			temp_mesh_list[5].realMeshId = real_meshid + 1 ;
			temp_mesh_list[6].realMeshId = -1, temp_mesh_list[6].meshId = -1 ;
			temp_mesh_list[7].realMeshId = -1, temp_mesh_list[7].meshId = -1 ;
			temp_mesh_list[8].realMeshId = -1, temp_mesh_list[8].meshId = -1 ;
			cnt = 9 ;
		}
		else {
			temp_mesh_list[0].realMeshId = real_meshid - (1 * width_count) - 1 ;
			temp_mesh_list[1].realMeshId = real_meshid - (1 * width_count) ;
			temp_mesh_list[2].realMeshId = real_meshid - (1 * width_count) + 1 ;
			temp_mesh_list[3].realMeshId = real_meshid - 1 ;
			temp_mesh_list[4].realMeshId = real_meshid ;
			temp_mesh_list[5].realMeshId = real_meshid + 1 ;
			temp_mesh_list[6].realMeshId = real_meshid + (1 * width_count) - 1 ;
			temp_mesh_list[7].realMeshId = real_meshid + (1 * width_count) ;
			temp_mesh_list[8].realMeshId = real_meshid + (1 * width_count) + 1 ;
			cnt = 9 ;
		}
	}

	for(i = 0 ; (i < cnt) && (i < *count) ; i++) {
		if(temp_mesh_list[i].realMeshId == -1) continue ;

		memset(&temp, 0, sizeof(REAL_MAPP)) ;
		temp.realMeshId = temp_mesh_list[i].realMeshId ;
		temp.meshId = temp_mesh_list[i].meshId ;

		ptr = (REAL_MAPP *) bsearch((const void *)&temp, (const void *) m_mapping, 
				m_rank_manager.numOfMesh, sizeof(REAL_MAPP), 
				(int (*)(const void *, const void *)) i_smr_mapping_cmp) ;
		if(ptr != NULL) { //찾았으면 
			temp_mesh_list[i].meshId = ptr->meshId ;
			memset(&smesh_info, 0, sizeof(SMESH_INFO)) ;
			ret = i_smr_get_smesh_info(ptr->meshId, &smesh_info) ;
			if(ret == ERR_NOT_FOUND) {
				temp_mesh_list[i].flag = ERR_NOT_FOUND ;
			}
		}
		else {
			temp_mesh_list[i].meshId = -1 ;
		}
		ptr = NULL ;
	}

	*count = cnt ;
	memcpy(real_mesh_list, temp_mesh_list, cnt * sizeof(REAL_MESH_INFO)) ;

	return NO_ERR ;
}

int i_smr_exist_smesh_info(int map_id) // ((ret == NO_ERR) || (ret == ERR_EMPTY_DATA)) 
{
//	std::map<int, SMESH_INFO>::iterator it = m_mesh_info.find(map_id) ;
//	if(it ==  m_mesh_info.end()) return ERR_NOT_FOUND ;		//ÇØ´ç mesh Á¤º¸°¡ ¾ø´Ù.
//	if(it->second.ds == 4) return ERR_EMPTY_DATA ;			//ÇØ´ç mesh´Â Á¸ÀçÇÏ³ª, À¯È¿µ¥ÀÌÅÍ(node,link, vertex)°¡ ¾øÀ½
	int ret = NO_ERR ;

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) {
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_exist_smesh_info::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}
//	printf("map_id[%d]==>m_cach_info[%d]->[%d]\n", map_id, map_id, m_cache_info[map_id]) ;
//	D("DEBUG_: m_cache_info[%d] --> [%d]\n", map_id, m_cache_info[map_id]) ;

//	if(m_cache_info[map_id] == 0) {
//	if(m_cache_info[map_id].size == 0) {
	if(m_cache_info[map_id] < 0) { // -1로 초기화 해서?
		D("ERR: not found map_id[%d]=>[%d]\n", map_id, m_cache_info[map_id]) ;
		sprintf(str_log, "i_smr_exist_smesh_info::ERR: not found map_id[%d]=>[%d]\n", map_id, m_cache_info[map_id]) ;
		ret = ERR_NOT_FOUND ;
		goto error_exit ;
	}
	if(m_mesh_info[map_id].size == 4) {
		ret = ERR_EMPTY_DATA ;
		sprintf(str_log, "i_smr_exist_smesh_info::ERR::ERR_EMPTY_DATA");
		goto error_exit ;
	}
error_exit :
	return ret ;
}

void i_smr_get_now_fetch_count(int *fetch_count)
{
	int i = 0 ;
	int n = 0 ;
	for(i = 0 ; i < m_rank_manager.numOfMesh ; i++) {
		if(m_cache_info[i] >= 0) n++ ;
	}
	*fetch_count = n ;
}

int i_smr_get_srank_map(int map_id, SRANK_MAP *srank_map) 
{
	int ret = NO_ERR ;

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) {
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh);
		sprintf(str_log, "i_smr_get_srank_map::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh);
		ret = ERR_BAD_PARAM;
		goto error_exit;
	}

	memcpy(srank_map, &m_rank_map[map_id], sizeof(SRANK_MAP)) ;
	
error_exit:
	return ret ;
}

int i_smr_get_smesh_info(int map_id, SMESH_INFO *smesh_info)
{
	int ret = NO_ERR ;

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) {
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_get_smesh_info::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

//	if(m_cache_info[map_id] == 0) {
//	if(m_cache_info[map_id].size == 0) {
	if(m_cache_info[map_id] < 0) {
		D("ERR: not found map_id[%d]=>[%d]\n", map_id, m_cache_info[map_id]) ;
		sprintf(str_log, "i_smr_get_smesh_info::ERR: not found map_id[%d]=>[%d]\n", map_id, m_cache_info[map_id]) ;
		ret = ERR_NOT_FOUND ;
		goto error_exit ;
	}

	memcpy(smesh_info, &m_mesh_info[map_id], sizeof(SMESH_INFO)) ;
//	D("smesh_info.ds[%d] cache_info.ds[%d]\n", smesh_info->ds, m_cache_info[map_id]) ;
	smesh_info->ds = m_cache_info[map_id] ;		// cache¿¡ ÀÖ´Â ds·Î ÀÔ·ÂÇØ¾ß ÇÑ´Ù.

error_exit:
	return ret ;
}

void i_smr_get_smap_header(SMAP_HEADER *smap_header)
{
	memcpy(smap_header, &m_map_header, sizeof(SMAP_HEADER)) ;
}

void i_smr_get_srank_manager(SRANK_MANAGER *srank_manager)
{
	memcpy(srank_manager, &m_rank_manager, sizeof(SRANK_MANAGER)) ;
}

void i_smr_get_version(int *version) 
{
	*version = atoi(m_map_header.version) ;
}

//int i_smr_set_smesh_info(SMESH_INFO *smesh_info)
//int i_smr_set_smesh_info(int map_id)
int i_smr_set_smesh_info(int map_id, int ds)
{
	int ret = NO_ERR ;

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) {
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

	m_cache_info[map_id] = ds ;

error_exit:
	return ret ;
}

int i_smr_reopen_manage(void)
{
	char file_name[FILE_LENGTH] = "" ;

	if(f_smr_manage != NULL) fclose(f_smr_manage) ;
	f_smr_manage = NULL ;
	
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MANAGE) ;
//	D("file_smr_manage[%s]\n", file_name) ;
		
	if ((f_smr_manage = fopen(file_name, "r")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		sprintf(str_log, "ERR: i_smr_reopen_manage::fopen(%s) fail\n", file_name);
		return ERR_FILE_OPEN ;
	}
	return NO_ERR ;
}

int i_smr_reopen_meshinfo(void)
{
	char file_name[FILE_LENGTH] = "" ;

	if(f_smr_meshinfo != NULL) fclose(f_smr_meshinfo) ;
	f_smr_meshinfo = NULL ;
	
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHINFO) ;
//	D("file_smr_meshinfo[%s]\n", file_name) ;

	if ((f_smr_meshinfo = fopen(file_name, "r")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		sprintf(str_log, "ERR: i_smr_reopen_meshinfo::ERR: fopen(%s) fail\n", file_name) ;
		return ERR_FILE_OPEN ;
	}
	return NO_ERR ;
}

int i_smr_reopen_cacheinfo(void)
{
	char file_name[FILE_LENGTH] = "" ;

	if(f_smr_cacheinfo != NULL) fclose(f_smr_cacheinfo) ;
	f_smr_cacheinfo = NULL ;	

	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_CACHEINFO) ;
//	D("file_smr_cacheinfo[%s]\n", file_name) ;

	if ((f_smr_cacheinfo = fopen(file_name, "r")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		sprintf(str_log, "i_smr_reopen_cacheinfo::ERR: fopen(%s) fail\n", file_name) ;
		return ERR_FILE_OPEN ;
	}
	return NO_ERR ;
}

int i_smr_reopen_meshdata(void)
{
	char file_name[FILE_LENGTH] = "" ;
	if(f_smr_meshdata != NULL) fclose(f_smr_meshdata) ;
	f_smr_meshdata = NULL ;
	
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHDATA) ;
//	D("file_smr_meshdata[%s]\n", file_name) ;

	if ((f_smr_meshdata = fopen(file_name, "r")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		sprintf(str_log, "i_smr_reopen_meshdata::ERR: fopen(%s) fail\n", file_name) ;
		return ERR_FILE_OPEN ;
	}
	return NO_ERR ;
}

// FILE(SMR_MESHDATA)
int i_smr_overwrite_smr_cacheinfo(void) // m_cache_info마지막 주소값에 m_cache_info체크섬을 넣어줌
{
	int ret = NO_ERR ;
	int mesh_cnt = 0 ;
	int check_sum = 0 ;
	int file_size = 0 ;

	mesh_cnt = m_rank_manager.numOfMesh ;
	check_sum = (int) i_smr_check_sum((unsigned short *) m_cache_info, (mesh_cnt * sizeof(int))) ;

	m_cache_info[mesh_cnt] = check_sum ;
//	D("DEBUG: check_sum[%d]\n", check_sum) ;
	ret = i_smr_write_smr_cacheinfo((char *)m_cache_info, ((mesh_cnt+1) * sizeof(int)), &file_size) ;

	return ret ;
}

int i_smr_write_mesh(char *buf, int buf_size, int *file_end, int *file_size)		
{
	int ret = NO_ERR ;
	struct stat file_stat ;
	char file_smr_meshdata[FILE_LENGTH] = "" ;
	int offset = 0 ;

	if(f_smr_meshdata != NULL) fclose(f_smr_meshdata) ;
	f_smr_meshdata = NULL ;

	snprintf(file_smr_meshdata, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHDATA) ;
//	D("file_smr_meshdata[%s]\n", file_smr_meshdata) ;
	if ((f_smr_meshdata = fopen(file_smr_meshdata, "a")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_smr_meshdata) ;
		sprintf(str_log, "i_smr_write_mesh::ERR: fopen(%s) fail\n", file_smr_meshdata) ;
		return ERR_FILE_OPEN ;
	}

	memset(&file_stat, 0, sizeof(file_stat)) ;
	if (0 == fstat(fileno(f_smr_meshdata), &file_stat)) {
		offset = (int) file_stat.st_size ; // a 옵션으로 열었을 떄 file_size
		*file_end = offset ;
	}
	else {
		D("ERR: write fail\n") ;
		sprintf(str_log, "i_smr_write_mesh::ERR: write fail\n") ;
		ret = ERR_WRITE_FAIL ;
		fclose(f_smr_meshdata) ;
		f_smr_meshdata = NULL ;
		goto error_exit ;
	}

	fwrite(buf, buf_size, 1, f_smr_meshdata) ;

	fflush(f_smr_meshdata) ;
	fclose(f_smr_meshdata) ;
	f_smr_meshdata = NULL ;

	if ((f_smr_meshdata = fopen(file_smr_meshdata, "r")) == NULL) {
		D("ERR: fopen(%s)\n", file_smr_meshdata) ;
		sprintf(str_log, "i_smr_write_mesh::ERR: fopen(%s)\n", file_smr_meshdata) ;
		return ERR_FILE_OPEN ;
	}
//	D("file_smr_meshdata[%s]\n", file_smr_meshdata) ;
	
	memset(&file_stat, 0, sizeof(file_stat)) ;
	if (0 == fstat(fileno(f_smr_meshdata), &file_stat)) {
		if((offset + buf_size) != (int) file_stat.st_size) {
			D("ERR: write fail(offset:%d buf_size:%d -> file size[%d]\n", 
					offset, buf_size, (int)file_stat.st_size) ;

            sprintf(str_log, "i_smr_write_mesh::ERR: write fail(offset:%d buf_size:%d -> file size[%d]\n",
                                                					offset, buf_size, (int)file_stat.st_size) ;
			ret = ERR_WRITE_FAIL ;
			fclose(f_smr_meshdata) ;
			f_smr_meshdata = NULL ;

			goto error_exit ;
		}
		*file_size = file_stat.st_size ;
	}
	else {
		D("ERR: Bad file handle\n") ;
		sprintf(str_log, "i_smr_write_mesh::ERR: Bad file handle\n") ;
		ret = ERR_BAD_HANDLE ;
		fclose(f_smr_meshdata) ;
		f_smr_meshdata = NULL ;

		goto error_exit ;
	}

error_exit:
	if(f_smr_meshdata != NULL) fclose(f_smr_meshdata) ;
	f_smr_meshdata = NULL ;

	return ret ;
}

int	i_smr_write_smr_manage(char *buf, int buf_size, int *file_size) 
{
	int ret = NO_ERR ;
	struct stat file_stat ;
	char file_smr_manage[FILE_LENGTH] = "" ;

	if(f_smr_manage != NULL) fclose(f_smr_manage ) ;
	f_smr_manage = NULL ;

	snprintf(file_smr_manage, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MANAGE) ;
//	D("file_smr_manage[%s]\n", file_smr_manage) ;
	if ((f_smr_manage = fopen(file_smr_manage, "w")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_smr_manage) ;
		sprintf(str_log, "ERR: i_smr_write_smr_manage::fopen(%s) fail\n", file_smr_manage);
		return ERR_FILE_OPEN ;
	}
	fwrite(buf, buf_size, 1, f_smr_manage) ;

	fflush(f_smr_manage) ;
	fclose(f_smr_manage) ;
	f_smr_manage = NULL ;

	if ((f_smr_manage = fopen(file_smr_manage, "r")) == NULL) {
		D("ERR: fopen(%s)\n", file_smr_manage) ;
		sprintf(str_log, "ERR: i_smr_write_smr_manage::fopen(%s)\n", file_smr_manage);
		return ERR_FILE_OPEN ;
	}
	
	if (0 == fstat(fileno(f_smr_manage), &file_stat)) {
		if(buf_size != (int) file_stat.st_size) {
			D("ERR: write fail(buf_size:%d -> file size[%d]\n", buf_size, (int)file_stat.st_size) ;
			sprintf(str_log, "ERR: i_smr_write_smr_manage::write fail(buf_size:%d -> file size[%d]\n", buf_size, (int)file_stat.st_size);
			ret = ERR_WRITE_FAIL ;
			fclose(f_smr_manage) ;
			f_smr_manage = NULL ;

			goto error_exit ;
		}
		*file_size = file_stat.st_size ;
	}
	else {
		D("ERR: Bad file handle\n") ;
		sprintf(str_log, "i_smr_write_smr_manage::ERR: Bad file handle\n");
		ret = ERR_BAD_HANDLE ;
		fclose(f_smr_manage) ;
		f_smr_manage = NULL ;

		goto error_exit ;
	}

error_exit:
	if(f_smr_manage != NULL) fclose(f_smr_manage) ;
	f_smr_manage = NULL ;

	return ret ;

}

int i_smr_write_smr_meshinfo(char *buf, int buf_size, int *file_size) 
{
	int ret = NO_ERR ;
	struct stat file_stat ;
	char file_smr_meshinfo[FILE_LENGTH] = "" ;

	if(f_smr_meshinfo != NULL) fclose(f_smr_meshinfo) ;
	f_smr_meshinfo = NULL ;

	snprintf(file_smr_meshinfo, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHINFO) ;
//	D("file_smr_meshinfo[%s]\n", file_smr_meshinfo) ;
	if ((f_smr_meshinfo = fopen(file_smr_meshinfo, "w")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_smr_meshinfo) ;
		sprintf(str_log, "i_smr_write_smr_meshinfo::ERR: fopen(%s) fail\n", file_smr_meshinfo) ;
		return ERR_FILE_OPEN ;
	}
	fwrite(buf, buf_size, 1, f_smr_meshinfo) ;

	fflush(f_smr_meshinfo) ;
	fclose(f_smr_meshinfo) ;
	f_smr_meshinfo = NULL ;

	if ((f_smr_meshinfo = fopen(file_smr_meshinfo, "r")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_smr_meshinfo) ;
		sprintf(str_log, "i_smr_write_smr_meshinfo::ERR: fopen(%s) fail\n", file_smr_meshinfo) ;
		return ERR_FILE_OPEN ;
	}
	
	if (0 == fstat(fileno(f_smr_meshinfo), &file_stat)) {
		if(buf_size != (int) file_stat.st_size) {
			D("ERR: write fail(buf_size:%d -> file size[%d]\n", buf_size, (int)file_stat.st_size) ;
			sprintf(str_log, "i_smr_write_smr_meshinfo::ERR: write fail(buf_size:%d -> file size[%d]\n", buf_size, (int)file_stat.st_size) ;
			ret = ERR_WRITE_FAIL ;
			fclose(f_smr_meshinfo) ;
			f_smr_meshinfo = NULL ;

			goto error_exit ;
		}
		*file_size = file_stat.st_size ;
	}
	else {
		D("ERR: Bad file handle\n") ;
		sprintf(str_log, "i_smr_write_smr_meshinfo::ERR: Bad file handle\n") ;
		ret = ERR_BAD_HANDLE ;
		fclose(f_smr_meshinfo) ;
		f_smr_meshinfo = NULL ;

		goto error_exit ;
	}

error_exit:
	if(f_smr_meshinfo != NULL) fclose(f_smr_meshinfo) ;
	f_smr_meshinfo = NULL ;

	return ret ;
}

int i_smr_write_smr_cacheinfo(char *buf, int buf_size, int *file_size) //overwrite시에 numofMesh+1 이 넘어옴
{
	int ret = NO_ERR ;
	struct stat file_stat ;
	char file_name[FILE_LENGTH] = "" ;

//	D("HEAR(5)\n") ;	

	if(f_smr_cacheinfo != NULL) fclose(f_smr_cacheinfo) ;
	f_smr_cacheinfo = NULL ;

	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_CACHEINFO) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_cacheinfo = fopen(file_name, "w")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		sprintf(str_log, "i_smr_write_smr_cacheinfo::ERR: fopen(%s) fail\n", file_name) ;
		return ERR_FILE_OPEN ;
	}
	fwrite(buf, buf_size, 1, f_smr_cacheinfo) ;

	fflush(f_smr_cacheinfo) ;
	fclose(f_smr_cacheinfo) ;
	f_smr_cacheinfo = NULL ;

	if ((f_smr_cacheinfo = fopen(file_name, "r")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		sprintf(str_log, "i_smr_write_smr_cacheinfo::ERR: fopen(%s) fail\n", file_name) ;
		return ERR_FILE_OPEN ;
	}
	
	if (0 == fstat(fileno(f_smr_cacheinfo), &file_stat)) {
		if(buf_size != (int) file_stat.st_size) {
			D("ERR: write fail(buf_size:%d -> file size[%d]\n", buf_size, (int)file_stat.st_size) ;
			sprintf(str_log, "i_smr_write_smr_cacheinfo::ERR: write fail(buf_size:%d -> file size[%d]\n", buf_size, (int)file_stat.st_size) ;
			ret = ERR_WRITE_FAIL ;
			fclose(f_smr_cacheinfo) ;
			f_smr_cacheinfo = NULL ;

			goto error_exit ;
		}
		*file_size = file_stat.st_size ;
	}
	else {
		D("ERR: Bad file handle\n") ;
		sprintf(str_log, "i_smr_write_smr_cacheinfo::ERR: Bad file handle\n") ;
		ret = ERR_BAD_HANDLE ;
		fclose(f_smr_cacheinfo) ;
		f_smr_cacheinfo = NULL ;

		goto error_exit ;
	}
//	D("HEAR(6)\n") ;	

error_exit:
	if(f_smr_cacheinfo != NULL) fclose(f_smr_cacheinfo) ;
	f_smr_cacheinfo = NULL ;

	return ret ;
}

int	i_smr_make_smr_cacheinfo(void) 
{
	int ret = NO_ERR ;
	int mesh_cnt = 0 ;
	int check_sum = 0 ;
	int file_size = 0 ;
	int i = 0 ;

	if(m_cache_info != NULL) free(m_cache_info) ;
	m_cache_info = NULL ;

	if((m_cache_info = (int *) malloc (sizeof(int) * (m_rank_manager.numOfMesh + 1))) == NULL) {
		D("ERR: malloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}

	mesh_cnt = m_rank_manager.numOfMesh ;
	memset(m_cache_info, 0, ((mesh_cnt+1) * sizeof(int)) ) ;	
	for(i = 0 ; i < mesh_cnt ; i++) {
		m_cache_info[i] = m_mesh_info[i].ds ;
	}
	check_sum = (int) i_smr_check_sum((unsigned short *) m_cache_info, (mesh_cnt * sizeof(int))) ;
	m_cache_info[i] = check_sum ;

	ret = i_smr_write_smr_cacheinfo((char *)m_cache_info, ((mesh_cnt+1) * sizeof(int)), &file_size) ;

error_exit :
	return ret ;
}

int i_smr_write_smr_zero(void) 
{
	char file_name[FILE_LENGTH] = "" ;

	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MANAGE) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_manage = fopen(file_name, "w")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		return ERR_FILE_OPEN ;
	}
	fclose(f_smr_manage) ;

	memset(file_name, 0, sizeof(file_name)) ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHINFO) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_meshinfo = fopen(file_name, "w")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		return ERR_FILE_OPEN ;
	}
	fclose(f_smr_meshinfo) ;

	memset(file_name, 0, sizeof(file_name)) ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_CACHEINFO) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_cacheinfo = fopen(file_name, "w")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		return ERR_FILE_OPEN ;
	}
	fclose(f_smr_cacheinfo) ;

	memset(file_name, 0, sizeof(file_name)) ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHDATA) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_meshdata = fopen(file_name, "w")) == NULL) {
		D("ERR: fopen(%s) fail\n", file_name) ;
		return ERR_FILE_OPEN ;
	}
	fclose(f_smr_meshdata) ;
	
	return NO_ERR ;
}

int i_smr_read_start_end_vertex_list(int map_id, int link_id, SVERTEX **s_vertex_list, int *vertex_count)
{
	int ret = NO_ERR ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	SLINK s_link ;
	SNODE start_node, end_node;

	SVERTEX *total_vertex_list = NULL ;
	int total_cnt = 0 ;

	int ds_vertex = 0 ;
	int i = 0 ;
	short x = 0, y = 0 ;

	memset(&s_link, 0, sizeof(SLINK)) ;
	ret = i_smr_read_link(map_id, link_id, &s_link) ;
	if(ret != NO_ERR) goto error_exit ;

	memset(&start_node, 0, sizeof(SNODE)) ;
	ret = i_smr_read_node(map_id, s_link._startId, &start_node) ;
	if(ret != NO_ERR) goto error_exit ;

	memset(&end_node, 0, sizeof(SNODE)) ;
	ret = i_smr_read_node(map_id, s_link._endId, &end_node) ;
	if(ret != NO_ERR) goto error_exit ;

	total_cnt = s_link._numOfVertex + 2 ;	//_startId + _numOfVertex + _endId 

	if((total_vertex_list = (SVERTEX *) malloc (total_cnt * sizeof(SVERTEX))) == NULL) {
		D("ERR: malloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(total_vertex_list, 0, total_cnt * sizeof(SVERTEX)) ;

	//first
	total_vertex_list[0].offLon = start_node.offLon ;
	total_vertex_list[0].offLat = start_node.offLat ;
	//middle
//	ds_vertex = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetVertex + s_link._vertexIndex * sizeof(SVERTEX) ;
//	fseek(f_smr_meshdata, ds_vertex, SEEK_SET) ;
//	fread(&total_vertex_list[1], sizeof(SVERTEX) * s_link._numOfVertex, 1, f_smr_meshdata) ;
	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;
	ptr = r_idx[entry].data ;
	ds_vertex = sizeof(int) + m_rank_map[map_id].offsetVertex + s_link._vertexIndex * sizeof(SVERTEX) ;
	memcpy(&total_vertex_list[1], &ptr[ds_vertex], sizeof(SVERTEX) * s_link._numOfVertex) ;

	for(i = 1 ; i < s_link._numOfVertex+1 ; i++) {	 //i´Â 1ºÎÅÍ ½ÃÀÛÇÑ´Ù.
		x = 0 ;
		y = 0 ;
		if(i == 1) {
			x = start_node.offLon + total_vertex_list[i].offLon ;
			y = start_node.offLat + total_vertex_list[i].offLat ;
		}
		else {
			x = total_vertex_list[i-1].offLon + total_vertex_list[i].offLon ;
			y = total_vertex_list[i-1].offLat + total_vertex_list[i].offLat ;
		}
		total_vertex_list[i].offLon = x ;
		total_vertex_list[i].offLat = y ;
	}
	//last
	total_vertex_list[total_cnt-1].offLon = end_node.offLon ;
	total_vertex_list[total_cnt-1].offLat = end_node.offLat ;

	*s_vertex_list = total_vertex_list ;
	*vertex_count = total_cnt ;

error_exit:
	if(ret != NO_ERR) {
		if(total_vertex_list != NULL) free(total_vertex_list) ;
		total_vertex_list = NULL ;
	}

	return ret ;
}

///[2013.06.24 BEGIN]///////////////////////////
int i_smr_read_start_end_vertex_count(int map_id, int link_id, int *vertex_count)
{
	int ret = NO_ERR ;
	SLINK s_link ;

	int total_cnt = 0 ;

	memset(&s_link, 0, sizeof(SLINK)) ;
	ret = i_smr_read_link(map_id, link_id, &s_link) ;
	if(ret != NO_ERR) goto error_exit ;

	total_cnt = s_link._numOfVertex + 2 ;	//_startId + _numOfVertex + _endId 
	*vertex_count = total_cnt ;

error_exit:

	return ret ;
}

int i_smr_read_start_end_vertex_data(int map_id, int link_id, SVERTEX *vertex_list)
{
	int ret = NO_ERR ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	SLINK s_link ;
	SNODE start_node, end_node;

	int total_cnt = 0 ;

	int ds_vertex = 0 ;
	int i = 0 ;
	short x = 0, y = 0 ;

	memset(&s_link, 0, sizeof(SLINK)) ;
	ret = i_smr_read_link(map_id, link_id, &s_link) ;
	if(ret != NO_ERR) goto error_exit ;

	memset(&start_node, 0, sizeof(SNODE)) ;
	ret = i_smr_read_node(map_id, s_link._startId, &start_node) ;
	if(ret != NO_ERR) goto error_exit ;

	memset(&end_node, 0, sizeof(SNODE)) ;
	ret = i_smr_read_node(map_id, s_link._endId, &end_node) ;
	if(ret != NO_ERR) goto error_exit ;

	total_cnt = s_link._numOfVertex + 2 ;	//_startId + _numOfVertex + _endId 

	//first
	vertex_list[0].offLon = start_node.offLon ;
	vertex_list[0].offLat = start_node.offLat ;
	//middle
//	ds_vertex = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetVertex + s_link._vertexIndex * sizeof(SVERTEX) ;
//	fseek(f_smr_meshdata, ds_vertex, SEEK_SET) ;
//	fread(&vertex_list[1], sizeof(SVERTEX) * s_link._numOfVertex, 1, f_smr_meshdata) ;
	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;
	ptr = r_idx[entry].data ;

	ds_vertex = sizeof(int) + m_rank_map[map_id].offsetVertex + s_link._vertexIndex * sizeof(SVERTEX) ;
	memcpy(&vertex_list[1], &ptr[ds_vertex], sizeof(SVERTEX) * s_link._numOfVertex) ;

	for(i = 1 ; i < s_link._numOfVertex+1 ; i++) {	 //i´Â 1ºÎÅÍ ½ÃÀÛÇÑ´Ù.
		x = 0 ;
		y = 0 ;
		if(i == 1) {
			x = start_node.offLon + vertex_list[i].offLon ;
			y = start_node.offLat + vertex_list[i].offLat ;
		}
		else {
			x = vertex_list[i-1].offLon + vertex_list[i].offLon ;
			y = vertex_list[i-1].offLat + vertex_list[i].offLat ;
		}
		vertex_list[i].offLon = x ;
		vertex_list[i].offLat = y ;
	}
	//last
	vertex_list[total_cnt-1].offLon = end_node.offLon ;
	vertex_list[total_cnt-1].offLat = end_node.offLat ;

error_exit:

	return ret ;
}

///[2013.06.24  END ]///////////////////////////



int i_smr_read_mapid_vertex_list(int map_id, SVERTEX **s_vertex_list, int *vertex_count)
{
	int ret = NO_ERR ;
	int i = 0 ;
	int link_cnt = 0 ;
	SLINK *s_link_list = NULL ;

	SVERTEX *total_vertex_list = NULL ;
	int total_cnt = 0 ;
	SVERTEX *temp_list = NULL ;
	int temp_cnt = 0 ;
	int offset = 0 ;

	if((*s_vertex_list != NULL) || (*vertex_count != 0)) {
		D("ERR: Param wrong\n") ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

	ret = i_smr_read_link_count(map_id, &link_cnt) ;
	if(ret) goto error_exit ;
//	D("link_cnt[%d]\n", link_cnt) ;
	if(link_cnt == 0) {
		*s_vertex_list = NULL ;
		*vertex_count = 0 ;
		
		ret = ERR_EMPTY_DATA ;
		goto error_exit ;
	}
	////////////////////////////////////////////////////////////////////

	if((s_link_list = (SLINK *) malloc (link_cnt * sizeof(SLINK))) == NULL) {
		D("ERR: malloc fail\n") ;
		*s_vertex_list = NULL ;
		*vertex_count = 0 ;

		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(s_link_list, 0, link_cnt * sizeof(SLINK)) ;

	ret = i_smr_read_link_list(map_id, link_cnt, s_link_list) ;
	if(ret) {
		*s_vertex_list = NULL ;
		*vertex_count = 0 ;
		goto error_exit ;
	}

	for(i = 0 ; i < link_cnt ; i++) {
	//	D("map_id[%d] link_id[%d] ==> startId[%d]endId[%d]length[%d]numOfVertex[%d]vertexIndex[%d]\n",
	//			map_id, i,
	//			s_link_list[i]._startId, s_link_list[i]._endId,
	//			s_link_list[i]._length, s_link_list[i]._numOfVertex, s_link_list[i]._vertexIndex) ;
	//	D("    laneNum(%d)drive(%d)divide(%d)linkType(%d)linkAttr(%d)roadType(%d)\n",
	//			s_link_list[i].laneNum(), s_link_list[i].drive(), s_link_list[i].divide(),
	//			s_link_list[i].linkType(), s_link_list[i].linkAttr(), s_link_list[i].roadType()) ;

		total_cnt += s_link_list[i]._numOfVertex ;
	}

	if(total_cnt == 0) {
		*s_vertex_list = NULL ;
		*vertex_count = 0 ;

		ret = ERR_EMPTY_DATA ;
		goto error_exit ;
	}

	if((total_vertex_list = (SVERTEX *) malloc (total_cnt * sizeof(SVERTEX))) == NULL) {
		D("ERR: malloc fail\n") ;
		*s_vertex_list = NULL ;
		*vertex_count = 0 ;

		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(total_vertex_list, 0, total_cnt * sizeof(SVERTEX)) ;

	for(i = 0 ; i < link_cnt ; i++) {
		ret = i_smr_read_vertex_list(map_id, i, &temp_list, &temp_cnt) ;
		if(ret) {
			if(total_vertex_list != NULL) free(total_vertex_list) ;
			total_vertex_list = NULL ;

			*s_vertex_list = NULL ;
			*vertex_count = 0 ;

			goto error_exit ;
		}

	//	for(j = 0 ; j < vertex_cnt ; j++) {
	//		printf("    vertex[%d]==>offLon[%d]offLat[%d]\n",
	//				j, s_vertex_list[j].offLon, s_vertex_list[j].offLat) ;
	//	}
		memcpy(&total_vertex_list[offset], temp_list, temp_cnt * sizeof(SVERTEX)) ;
		offset += temp_cnt ;

		if(temp_list != NULL) free(temp_list) ;
		temp_list = NULL ;

		temp_cnt = 0 ;
	}
	*vertex_count = total_cnt ;
	*s_vertex_list = total_vertex_list ;

error_exit:
	if(ret != NO_ERR) {
		if(total_vertex_list != NULL) free(total_vertex_list) ;
		total_vertex_list = NULL ;
	}

	if(temp_list != NULL) free(temp_list) ;
	temp_list = NULL ;

	if(s_link_list != NULL) free(s_link_list) ;
	s_link_list = NULL ;

	return ret ;
}

////[2013.06.24 BEGIN]////////////////////
int i_smr_read_mapid_vertex_count(int map_id, int *vertex_count)
{
	int ret = NO_ERR ;
	int i = 0 ;
	int link_cnt = 0 ;
	SLINK *s_link_list = NULL ;

	int total_cnt = 0 ;

	ret = i_smr_read_link_count(map_id, &link_cnt) ;
	if(ret) goto error_exit ;
	if(link_cnt == 0) {
		ret = ERR_EMPTY_DATA ;
		goto error_exit ;
	}
	////////////////////////////////////////////////////////////////////

	if((s_link_list = (SLINK *) malloc (link_cnt * sizeof(SLINK))) == NULL) {
		D("ERR: malloc fail\n") ;

		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(s_link_list, 0, link_cnt * sizeof(SLINK)) ;

	ret = i_smr_read_link_list(map_id, link_cnt, s_link_list) ;
	if(ret) goto error_exit ;

	for(i = 0 ; i < link_cnt ; i++) {
		total_cnt += s_link_list[i]._numOfVertex ;
	}

	if(total_cnt == 0) {
		ret = ERR_EMPTY_DATA ;
		goto error_exit ;
	}
	*vertex_count = total_cnt ;

error_exit:
	if(s_link_list != NULL) free(s_link_list) ;
	s_link_list = NULL ;

	return ret ;
}

int i_smr_read_mapid_vertex_data(int map_id, SVERTEX *vertex_list)
{
	int ret = NO_ERR ;
	int i = 0 ;
	int link_cnt = 0 ;

	SVERTEX *temp_list = NULL ;
	int temp_cnt = 0 ;
	int offset = 0 ;

	ret = i_smr_read_link_count(map_id, &link_cnt) ;
	if(ret) goto error_exit ;
	if(link_cnt == 0) {
		ret = ERR_EMPTY_DATA ;
		goto error_exit ;
	}
	////////////////////////////////////////////////////////////////////

	for(i = 0 ; i < link_cnt ; i++) {
		ret = i_smr_read_vertex_list(map_id, i, &temp_list, &temp_cnt) ;
		if(ret) goto error_exit ;

		memcpy(&vertex_list[offset], temp_list, temp_cnt * sizeof(SVERTEX)) ;
		offset += temp_cnt ;

		if(temp_list != NULL) free(temp_list) ;
		temp_list = NULL ;

		temp_cnt = 0 ;
	}

error_exit:
	if(temp_list != NULL) free(temp_list) ;
	temp_list = NULL ;

	return ret ;
}

////[2013.06.24  END ]////////////////////


//smrmeshdata.bin ¿¡¼­ SVERTEX ¸®½ºÆ®¸¦ ÀÐ¾îµå¸°´Ù.
int i_smr_read_vertex_list(int map_id, int link_id, SVERTEX **s_vertex_list, int *vertex_count)
{	//i_smr_read_vertex_list(map_id, i, &temp_list, &temp_cnt)
	int ret = NO_ERR ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	SLINK s_link ;
	SNODE s_node ;
	SVERTEX *temp_list = NULL ;
	int ds_vertex = 0 ;
	int i = 0 ;
	short x = 0, y = 0 ;

	memset(&s_link, 0, sizeof(SLINK)) ;
	memset(&s_node, 0, sizeof(SNODE)) ;

	ret = i_smr_read_link(map_id, link_id, &s_link) ;
	if(ret != NO_ERR) goto error_exit ;

	ret = i_smr_read_node(map_id, s_link._startId, &s_node) ;
	if(ret != NO_ERR) goto error_exit ;

	if((temp_list = (SVERTEX *) malloc (sizeof(SVERTEX) * s_link._numOfVertex)) == NULL) {
		D("ERR: memory alloc fail(%d)\n", sizeof(SVERTEX) * s_link._numOfVertex) ;
		sprintf(str_log, "i_smr_read_vertex_list::ERR: memory alloc fail(%d)\n", sizeof(SVERTEX) * s_link._numOfVertex) ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(temp_list, 0, sizeof(SVERTEX) * s_link._numOfVertex) ;

//	ds_vertex = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetVertex + s_link._vertexIndex * sizeof(SVERTEX) ;
//	fseek(f_smr_meshdata, ds_vertex, SEEK_SET) ;
//	fread(temp_list, sizeof(SVERTEX) * s_link._numOfVertex, 1, f_smr_meshdata) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;
	ptr = r_idx[entry].data ;

	ds_vertex = sizeof(int) + m_rank_map[map_id].offsetVertex + s_link._vertexIndex * sizeof(SVERTEX) ;
	memcpy(temp_list, &ptr[ds_vertex], sizeof(SVERTEX) * s_link._numOfVertex) ;

	for(i = 0 ; i < s_link._numOfVertex ; i++) {
		x = 0 ;
		y = 0 ;
		if(i == 0) {
			x = s_node.offLon + temp_list[i].offLon ;
			y = s_node.offLat + temp_list[i].offLat ;
		}
		else {
			x = temp_list[i-1].offLon + temp_list[i].offLon ;
			y = temp_list[i-1].offLat + temp_list[i].offLat ;
		}
		temp_list[i].offLon = x ;
		temp_list[i].offLat = y ;
	}
	*s_vertex_list = temp_list ;
	*vertex_count = s_link._numOfVertex ;

error_exit:
	if(ret != NO_ERR) {
		if(temp_list != NULL) free(temp_list) ;
		temp_list = NULL ;
	}

	return ret ;
}

///[2013.06.24 BEGIN]//////////////////////////////
int i_smr_read_vertex_count(int map_id, int link_id, int *vertex_count)
{
	int ret = NO_ERR ;
	SLINK s_link ;

	memset(&s_link, 0, sizeof(SLINK)) ;

	ret = i_smr_read_link(map_id, link_id, &s_link) ;
	if(ret != NO_ERR) goto error_exit ;

	*vertex_count = s_link._numOfVertex ;
error_exit:

	return ret ;
}

int i_smr_read_vertex_data(int map_id, int link_id, SVERTEX *vertex_list)
{
	int ret = NO_ERR ;
    char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	SLINK s_link ;
	SNODE s_node ;
	int ds_vertex = 0 ;
	int i = 0 ;
	short x = 0, y = 0 ;

	memset(&s_link, 0, sizeof(SLINK)) ;
	memset(&s_node, 0, sizeof(SNODE)) ;

	ret = i_smr_read_link(map_id, link_id, &s_link) ;
	if(ret != NO_ERR) goto error_exit ;

	ret = i_smr_read_node(map_id, s_link._startId, &s_node) ;
	if(ret != NO_ERR) goto error_exit ;

//	ds_vertex = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetVertex + s_link._vertexIndex * sizeof(SVERTEX) ;
//	fseek(f_smr_meshdata, ds_vertex, SEEK_SET) ;
//	fread(vertex_list, sizeof(SVERTEX) * s_link._numOfVertex, 1, f_smr_meshdata) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

	ptr = r_idx[entry].data ;

	ds_vertex = sizeof(int) + m_rank_map[map_id].offsetVertex + s_link._vertexIndex * sizeof(SVERTEX) ;
	memcpy(vertex_list, &ptr[ds_vertex], sizeof(SVERTEX) * s_link._numOfVertex) ;

	for(i = 0 ; i < s_link._numOfVertex ; i++) {
		x = 0 ;
		y = 0 ;
		if(i == 0) {
			x = s_node.offLon + vertex_list[i].offLon ;
			y = s_node.offLat + vertex_list[i].offLat ;
		}
		else {
			x = vertex_list[i-1].offLon + vertex_list[i].offLon ;
			y = vertex_list[i-1].offLat + vertex_list[i].offLat ;
		}
		vertex_list[i].offLon = x ;
		vertex_list[i].offLat = y ;
	}

error_exit:

	return ret ;
}

///[2013.06.24  END ]//////////////////////////////


//smrmeshdata.bin ¿¡¼­ SLINK¸¦ ÀÐ¾îµå¸°´Ù.
int i_smr_read_link(int map_id, int link_id, SLINK *s_link)
{
	int ret = NO_ERR ;
	int ds_link = 0 ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	if(m_rank_map == NULL || m_cache_info == NULL) {
		D("ERR: m_rank_map or m_cache_info is null\n") ;
		sprintf(str_log, "i_smr_read_link::ERR: m_rank_map or m_cache_info is null\n") ;
		ret = ERR_NULL ;
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) {
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_read_link::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

//	ds_link = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetLink + link_id * sizeof(SLINK) ;
//	fseek(f_smr_meshdata, ds_link, SEEK_SET) ;
//	fread((SLINK *)s_link, sizeof(SLINK), 1, f_smr_meshdata) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

	ptr = r_idx[entry].data ;

	ds_link = sizeof(int) + m_rank_map[map_id].offsetLink + link_id * sizeof(SLINK) ;
	memcpy((SLINK *)s_link, &ptr[ds_link], sizeof(SLINK)) ;

error_exit:
	return ret ;
}

//smrmeshdata.bin¿¡¼­ SLINK ¸®½ºÆ®¸¦ ÀÐ¾îµå¸°´Ù.
int i_smr_read_link_list(int map_id, int link_count, SLINK *s_link_list)
{
	int ret = NO_ERR ;
	int ds_link = 0 ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	if(m_rank_map == NULL || m_cache_info == NULL) {
		D("ERR: m_rank_map or m_cache_info is null\n") ;
		sprintf(str_log, "i_smr_read_link_list::ERR: m_rank_map or m_cache_info is null\n") ;
		ret = ERR_NULL ;
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) {
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_read_link_list::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

//	ds_link = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetLink ;
//	fseek(f_smr_meshdata, ds_link, SEEK_SET) ;
//	fread(s_link_list, link_count * sizeof(SLINK), 1, f_smr_meshdata) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

	ptr = r_idx[entry].data ;

	ds_link = sizeof(int) + m_rank_map[map_id].offsetLink ;
	memcpy(s_link_list, &ptr[ds_link], link_count * sizeof(SLINK)) ;

error_exit:
	return ret ;
}

//smrmeshdata.bin ¿¡¼­ SADJNODE¸¦ ÀÐ¾îµå¸°´Ù.
int i_smr_read_adjnode(int map_id, int node_id, SADJNODE *s_adjnode)
{
	int ret = NO_ERR ;

	SNODE snode ;
	memset(&snode, 0, sizeof(SNODE)) ;

	ret = i_smr_read_node(map_id, node_id, &snode) ;
	if(ret) goto error_exit ;

	if(snode.isAdjNode() != 1) { 	//adjnode°¡ ¾Æ´Ï¶ó¸é
		D("ERR: Invalid data - map_id[%d] node_id[%d] isAdjNode(%d)\n", map_id, node_id, snode.isAdjNode()) ;
		sprintf(str_log, "i_smr_read_adjnode::ERR: Invalid data - map_id[%d] node_id[%d] isAdjNode(%d)\n", map_id, node_id, snode.isAdjNode()) ;
		ret = ERR_INVALID_DATA ;
		goto error_exit ;
	}
	ret = i_smr_read_adjnode(map_id, &snode, s_adjnode) ;

error_exit:
	return ret ;
}

//smrmeshdata.bin ¿¡¼­ SADJNODE¸¦ ÀÐ¾îµå¸°´Ù.
int i_smr_read_adjnode(int map_id, SNODE *s_node, SADJNODE *s_adjnode) 
{
	int ret = NO_ERR ;
	int ds_adjnode = 0 ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	if(m_rank_map == NULL || m_cache_info == NULL) {
		D("ERR: m_rank_map or m_cache_info is null\n") ;
		sprintf(str_log, "i_smr_read_adjnode::ERR: m_rank_map or m_cache_info is null\n") ;
		ret = ERR_NULL ;
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) {
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_read_adjnode::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

//	ds_adjnode = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetAdjnode + s_node->getExNodeDS() ;
//	fseek(f_smr_meshdata, ds_adjnode, SEEK_SET) ;
//	fread((SADJNODE *)s_adjnode, sizeof(SADJNODE), 1, f_smr_meshdata) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

	ptr = r_idx[entry].data ;

	ds_adjnode = sizeof(int) + m_rank_map[map_id].offsetAdjnode + s_node->getExNodeDS() ;
	memcpy((SADJNODE *)s_adjnode, &ptr[ds_adjnode], sizeof(SADJNODE)) ;

error_exit:
	return ret ;
}

//smrmeshdata.bin ¿¡¼­ SCROSSNODE¸¦ ÀÐ¾îµå¸°´Ù.
int i_smr_read_crossnode(int map_id, int node_id, SCROSSNODE *s_crossnode, int *s_crossnode_count)
{
	int ret = NO_ERR ;
	SNODE snode ;

	memset(&snode, 0, sizeof(SNODE)) ;

	ret = i_smr_read_node(map_id, node_id, &snode) ;
	if(ret) goto error_exit ;

	if(snode.isAdjNode() != 0) {		//  crossnode°¡ ¾Æ´Ï¶ó¸é
		D("ERR: Invalid data - map_id[%d] node_id[%d] isAdjNode(%d)\n", map_id, node_id, snode.isAdjNode()) ;
		sprintf(str_log, "ERR: Invalid data - map_id[%d] node_id[%d] isAdjNode(%d)\n", map_id, node_id, snode.isAdjNode()) ;
		ret = ERR_INVALID_DATA ;
		goto error_exit ;
	}
	ret = i_smr_read_crossnode(map_id, &snode, s_crossnode) ;
	*s_crossnode_count = snode.getnumofJCLink() ;

error_exit:
	return ret ;
}

int i_smr_read_crossnode(int map_id, SNODE *s_node, SCROSSNODE *s_crossnode)
{
	int ret = NO_ERR ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	char buf[CROSSNODE_MAX_SIZE] = "" ;
	int num_of_jclink = 0 ;
	int ds_crossnode = 0 ;
	int read_size = 0 ;

	if(s_node == NULL) {
		D("ERR: s_node is null\n") ;
		sprintf(str_log, "i_smr_read_crossnode::ERR: s_node is null\n") ;
		ret = ERR_INVALID_DATA ;
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) {
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_read_crossnode::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

//	num_of_jclink = s_node->getnumofJCLink() ;
//	ds_crossnode = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetCrossnode + s_node->getExNodeDS() ; 
//	read_size = s_crossnode->size(num_of_jclink) ; 
//	fseek(f_smr_meshdata, ds_crossnode, SEEK_SET) ;
//	fread(buf, read_size, 1, f_smr_meshdata) ;
//	s_crossnode->setData(buf, num_of_jclink) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

	ptr = r_idx[entry].data ;

	num_of_jclink = s_node->getnumofJCLink() ;
	ds_crossnode = sizeof(int) + m_rank_map[map_id].offsetCrossnode + s_node->getExNodeDS() ;
	read_size = s_crossnode->size(num_of_jclink) ;
	memcpy(buf, &ptr[ds_crossnode], read_size) ;
	s_crossnode->setData(buf, num_of_jclink) ;

error_exit:

	return ret ;
}


//smrmeshdata.bin ¿¡¼­ SNODE¸¦ ÀÐ¾îµå¸°´Ù.
int i_smr_read_node(int map_id, int node_id, SNODE *s_node) 
{
	int ret = NO_ERR ;
	int ds_node = 0 ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	if(m_rank_map == NULL || m_cache_info == NULL) {
		D("ERR: i_smr_read_node::m_rank_map is null\n") ;
		sprintf(str_log, "i_smr_read_node::ERR: i_smr_read_node::m_rank_map is null\n") ;
		ret = ERR_NULL ;	
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) { 
		D("ERR: i_smr_read_nodebad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_read_node::ERR: i_smr_read_nodebad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

//	ds_node = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetNode + node_id * sizeof(SNODE);
//	fseek(f_smr_meshdata, ds_node, SEEK_SET) ;
//	fread((SNODE *)s_node, sizeof(SNODE), 1, f_smr_meshdata) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

	ptr = r_idx[entry].data ;

	ds_node = sizeof(int) + m_rank_map[map_id].offsetNode + node_id * sizeof(SNODE) ;
	memcpy((SNODE *)s_node, &ptr[ds_node], sizeof(SNODE)) ;

error_exit:

	return ret ;
}

//smrmeshdata.bin ¿¡¼­ SNODE list¸¦ ÀÐ¾îµå¸°´Ù.
//int read_node_list(int map_id, int *node_count, SNODE **s_node_list)
int i_smr_read_node_list(int map_id, int node_count, SNODE *s_node_list)
{
	int ret = NO_ERR ;
	int ds_node = 0 ;
	char *ptr = NULL ;	//2013.06.27
	int entry = 0 ;		//2013.06.27

	if(m_rank_map == NULL || m_cache_info == NULL) {
		D("ERR: m_rank_map or m_cache_info is null\n") ;
		sprintf(str_log, "i_smr_read_node_list::ERR: m_rank_map or m_cache_info is null\n") ;
		ret = ERR_NULL ;	
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) { 
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_read_node_list::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ; 
	}

	if(node_count < 0 || node_count > m_rank_map[map_id].numOfNode || s_node_list == NULL) {
		D("ERR: bad param [%d] or s_node_list is null\n", node_count) ;
		sprintf(str_log, "i_smr_read_node_list::ERR: bad param [%d] or s_node_list is null\n", node_count) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

//	ds_node = m_cache_info[map_id] + sizeof(int) + m_rank_map[map_id].offsetNode ;
//	fseek(f_smr_meshdata, ds_node, SEEK_SET) ;
//	fread(s_node_list, node_count * sizeof(SNODE), 1, f_smr_meshdata) ;

	ptr = r_idx[entry].data ;
	ds_node = sizeof(int) + m_rank_map[map_id].offsetNode ;
	memcpy(s_node_list, &ptr[ds_node], node_count * sizeof(SNODE)) ;

error_exit :

	return ret ;
}

int i_smr_read_mesh_checksum(int map_id, int *check_sum)
{
	int ret = NO_ERR ;
	int ds_checksum = 0 ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	if(m_rank_map == NULL || m_cache_info == NULL) {
		D("ERR: m_rank_map or m_cache_info is null\n") ;
		ret = ERR_NULL ;	
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) { 
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ; 
	}

//	ds_checksum = m_cache_info[map_id] ;
//	fseek(f_smr_meshdata, ds_checksum, SEEK_SET) ;
//	fread((int *)check_sum, sizeof(int), 1, f_smr_meshdata) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

	ptr = r_idx[entry].data ;
	ds_checksum = 0 ;
	memcpy((int *)check_sum, &ptr[ds_checksum], sizeof(int)) ;

error_exit:

	return ret ;
}

int i_smr_read_mesh_ridx(int map_id, int entry)
{
	int ret = NO_ERR ;
	int ds_mesh = 0 ;
	int size = 0 ;
	int check_sum = 0 ;
	int read_sum = 0 ;

	char *buf = r_idx[entry].data ;

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) { 
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_read_mesh_ridx::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ; 
	}

	ds_mesh = m_cache_info[map_id] ;
	size = m_mesh_info[map_id].size ;

	fseek(f_smr_meshdata, ds_mesh, SEEK_SET) ;
	fread((char *)buf, size, 1, f_smr_meshdata) ;

	memcpy(&read_sum, buf, sizeof(int)) ;
	check_sum = i_smr_check_sum((short unsigned int*)&buf[4], size - sizeof(int)) ;
	if(read_sum != check_sum) {
		D("ERR: CHECKSUM FAIL - i_smr_read_mesh_ridx(%d) check[%d]:read[%d]\n", map_id, check_sum, read_sum) ;
		sprintf(str_log, "i_smr_read_mesh_ridx::ERR: CHECKSUM FAIL - i_smr_read_mesh_ridx(%d) check[%d]:read[%d]\n", map_id, check_sum, read_sum) ;
		ret = ERR_CHECK_SUM ;
		m_cache_info[map_id] = -1 ;		//if checksum fail then m_cache_info = -1 because re download

		r_idx[entry].order = 0 ;
		r_idx[entry].map_id = -1 ;		//2013.07.02
		r_idx[entry].size = 0 ;
		memset(r_idx[entry].data, 0, m_max_mesh_size) ;

		goto error_exit ;
	}
	r_idx[entry].map_id = map_id ;
	r_idx[entry].size = size ;

error_exit:

	return ret ;
}

int i_smr_read_mesh(int map_id, char *buf, int buf_size)
{
	int ret = NO_ERR ;
	int ds_mesh = 0 ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	if(m_rank_map == NULL || m_cache_info == NULL) {
		D("ERR: m_rank_map or m_cache_info is null\n") ;
		ret = ERR_NULL ;	
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) { 
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ; 
	}

//	ds_mesh = m_cache_info[map_id] ;
//	fseek(f_smr_meshdata, ds_mesh, SEEK_SET) ;
//	fread((char *)buf, buf_size, 1, f_smr_meshdata) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

	ptr = r_idx[entry].data ;
	ds_mesh = 0 ;
	memcpy((char *)buf, &ptr[ds_mesh], buf_size) ;

error_exit:

	return ret ;

}

int i_smr_read_mesh(int map_id, char *buf, int ds_mesh, int buf_size)
{
	int ret = NO_ERR ;
	char *ptr = NULL ;      //2013.06.27
	int entry = 0 ;         //2013.06.27

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) { 
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ; 
	}

//	fseek(f_smr_meshdata, ds_mesh, SEEK_SET) ;
//	fread((char *)buf, buf_size, 1, f_smr_meshdata) ;

	ret = ridx_get(map_id, &entry) ;
//	printf("ridx_get(%d)=>entry[%d]: ret[%d]\n", map_id, entry, ret) ;
	if(ret == ERR_NOT_FOUND) {
		ret = ridx_set_entry(&entry) ;
	//	printf("ridx_set_entry():entry[%d]==>ret[%d]\n", entry, ret) ;
		if(ret != NO_ERR) goto error_exit ;
		ret = i_smr_read_mesh_ridx(map_id, entry) ;
		if(ret != NO_ERR) goto error_exit ;
	}
//	printf("entry[%d]\n", entry) ;

	ptr = r_idx[entry].data ;
	memcpy((char *)buf, &ptr[ds_mesh], buf_size) ;

error_exit:

	return ret ;

}

int i_smr_read_link_count(int map_id, int *link_count)
{
	int ret = NO_ERR ;

	if(m_rank_map == NULL) {
		D("ERR: m_rank_map is null\n") ;
		sprintf(str_log, "i_smr_read_link_count::ERR: m_rank_map is null\n") ;
		ret = ERR_NULL ;	
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) { 
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_read_link_count::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ; 
	}

	*link_count = m_rank_map[map_id].numOfLink;

	//m_rank_manager¿¡´Â °³¼öÁ¤º¸´Â ÀÖÀ¸³ª, ½ÇÁ¦Á¤º¸´Â ¾ÆÁ÷¼­¹ö¿¡¼­ ¹Þ¾Æ¿ÀÁö ¸øÇÑ °æ¿ì
	if(m_cache_info[map_id] < 0) {
		D("ERR: not found map_id[%d]=>[%d]\n", map_id, m_cache_info[map_id]) ;
		sprintf(str_log, "i_smr_read_link_count::ERR: not found map_id[%d]=>[%d]\n", map_id, m_cache_info[map_id]) ;
		ret = ERR_NOT_FOUND ;
		goto error_exit ;
	}

error_exit:

	return ret ;
}


int i_smr_read_node_count(int map_id, int *node_count)
{
	int ret = NO_ERR ;

	if(m_rank_map == NULL) {
		D("ERR: m_rank_map is null\n") ;
		sprintf(str_log, "i_smr_read_node_count::ERR: m_rank_map is null\n") ;
		ret = ERR_NULL ;	
		goto error_exit ;
	}

	if(map_id < 0 || map_id >= m_rank_manager.numOfMesh) { 
		D("ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		sprintf(str_log, "i_smr_read_node_count::ERR: bad param - map_id[%d]: numOfMesh[%d]\n", map_id, m_rank_manager.numOfMesh) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ; 
	}

	*node_count = m_rank_map[map_id].numOfNode ;

	//m_rank_manager¿¡´Â °³¼öÁ¤º¸´Â ÀÖÀ¸³ª, ½ÇÁ¦Á¤º¸´Â ¾ÆÁ÷¼­¹ö¿¡¼­ ¹Þ¾Æ¿ÀÁö ¸øÇÑ °æ¿ì
	if(m_cache_info[map_id] < 0) {
		D("ERR: not found map_id[%d]=>[%d]\n", map_id, m_cache_info[map_id]) ;
		sprintf(str_log, "i_smr_read_node_count::ERR: not found map_id[%d]=>[%d]\n", map_id, m_cache_info[map_id]) ;
		ret = ERR_NOT_FOUND ;
		goto error_exit ;
	}

error_exit:

	return ret ;
}

////debug//////////////////////////////////////////////////////////////////////////////////
void debug_setting_cache_info(int flag)
{
	int i = 0 ;

	if(flag == 1) {
		for(i = 0 ; i < m_rank_manager.numOfMesh; i++) {
		//	m_cache_info[i].meshId = m_mesh_info[i].meshId ;
		//	m_cache_info[i].ds = m_mesh_info[i].ds ;
		//	m_cache_info[i].size = m_mesh_info[i].size ;
			m_cache_info[i] = m_mesh_info[i].ds ;
		}
	}
	else {
	//	memset(m_cache_info, 0, m_rank_manager.numOfMesh * sizeof(SMESH_INFO)) ;
		memset(m_cache_info, -1, m_rank_manager.numOfMesh * sizeof(int)) ;
	}
}

void debug_copy_cache_info(int *d_cache_info) 
{
	memcpy(m_cache_info, d_cache_info, sizeof(int) * (m_rank_manager.numOfMesh+1)) ;
}
////debug//////////////////////////////////////////////////////////////////////////////////

int i_smr_read_smr_cacheinfo(int flag)
{
	int ret = NO_ERR ;
	int file_size = 0 ;
	int check_sum = 0 ;
	struct stat file_stat ;

	if(m_cache_info != NULL) free(m_cache_info) ;
	m_cache_info = NULL ;

//	D("DEBUG: m_rank_manager.numOfMesh[%d]\n", m_rank_manager.numOfMesh) ;

//	if((m_cache_info = (int *) malloc (sizeof(int) * m_rank_manager.numOfMesh)) == NULL) {
	// checksum°ø°£À» È®º¸ÇÑ´Ù. - cache_info ¸ÇµÚ¿¡ checksumÀÔ·ÂÇÒ °ø°£À» È®º¸ÇÑ´Ù.
	if((m_cache_info = (int *) malloc (sizeof(int) * (m_rank_manager.numOfMesh + 1))) == NULL) {
		D("ERR: malloc fail\n") ;
		sprintf(str_log, "i_smr_read_smr_cacheinfo::ERR: malloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}

//	memset(m_cache_info, -1, sizeof(int) * m_rank_manager.numOfMesh) ;	
	// ÃÊ±â¿¡´Â checksum°ø°£Æ÷ÇÔ -1·Î ÇÑ´Ù.
	memset(m_cache_info, -1, sizeof(int) * (m_rank_manager.numOfMesh+1)) ;	

	if(flag == ERR_OPEN_SMR_CACHEINFO) {	//ÆÄÀÏÀÌ ¾ø¾î¼­ ¿¡·¯°¡ ¹ß»ýÇÑ °æ¿ì¿¡ ºóÆÄÀÏ »ý¼º
	//	ret = i_smr_write_smr_cacheinfo((char *)m_cache_info, sizeof(int) * m_rank_manager.numOfMesh, &file_size) ;
		check_sum = (int) i_smr_check_sum((unsigned short *) m_cache_info, (m_rank_manager.numOfMesh * sizeof(int))) ;
		m_cache_info[m_rank_manager.numOfMesh] = check_sum ; // 마지막 배열에 체크섬 저장
		ret = i_smr_write_smr_cacheinfo((char *)m_cache_info, sizeof(int) * (m_rank_manager.numOfMesh+1), &file_size) ;
	}  // f_smr_chcheinfo = NULL 상태  // 에러코드 나왔을 때 체크썸을 쓰기 위한 동작
	else {
		////////////////////////////////////////////////////////////////////
		if(0 == fstat(fileno(f_smr_cacheinfo), &file_stat)) {
			if((int)file_stat.st_size != (int)(sizeof(int) * (m_rank_manager.numOfMesh+1))) {
				D("ERR: file invalid : file_size(%d)/(%d)\n", 
					(int)file_stat.st_size, (sizeof(int) * (m_rank_manager.numOfMesh+1))) ;

                sprintf(str_log, "i_smr_read_smr_cacheinfo::ERR: file invalid : file_size(%d)/(%d)\n",
                                                        (int)file_stat.st_size, (sizeof(int) * (m_rank_manager.numOfMesh+1))) ;
				ret = ERR_SMR_CACHEINFO_DATA ;
				goto error_exit ;
			}
		}
		else {
			D("ERR: file invalid\n") ;
			sprintf(str_log, "i_smr_read_smr_cacheinfo::ERR: file invalid\n") ;
			ret = ERR_SMR_CACHEINFO_HANDLE ;
			goto error_exit ;
		}
		////////////////////////////////////////////////////////////////////


		fseek(f_smr_cacheinfo, 0, SEEK_SET) ;
	//	fread(m_cache_info, sizeof(int) * m_rank_manager.numOfMesh, 1, f_smr_cacheinfo) ;
		fread(m_cache_info, sizeof(int) * (m_rank_manager.numOfMesh+1), 1, f_smr_cacheinfo) ;

		check_sum = (int) i_smr_check_sum((unsigned short *) m_cache_info, (m_rank_manager.numOfMesh * sizeof(int))) ;
		if(check_sum != m_cache_info[m_rank_manager.numOfMesh]) {
			D("ERR: m_cache_info check sum fail: check_sum[%d] vs m_cache_info check_sum[%d]\n", 
					check_sum, m_cache_info[m_rank_manager.numOfMesh]) ;
            sprintf(str_log, "i_smr_read_smr_cacheinfo::ERR: m_cache_info check sum fail: check_sum[%d] vs m_cache_info check_sum[%d]\n",
                                                                check_sum, m_cache_info[m_rank_manager.numOfMesh]) ;
			ret = ERR_SMR_CACHEINFO_CHECKSUM ;
		}
	}

error_exit :

	return ret ;
}

int i_smr_read_smr_meshinfo(void)
{
	int ret = NO_ERR ;
	int i = 0 ;
	int check_sum = 0 ;
	int temp_check_sum = 0 ;
	struct stat file_stat ;
	
	SMESH_INFO temp ;
	int offset = 0 ;
	int smr_mesh_info_size = 0 ;
	char *info = NULL ;

	///DEBUG
	int total_size = 0 ;

	//////////////////////////////////////////////////////////////////////
	if(0 == fstat(fileno(f_smr_meshinfo), &file_stat)) {
		if((int)file_stat.st_size != (int)(sizeof(int) + SMESH_INFO_SIZE * m_rank_manager.numOfMesh)) {
			D("ERR: file invalid : file_size(%d)/(%d)\n", 
					(int)file_stat.st_size, sizeof(int) + (SMESH_INFO_SIZE * m_rank_manager.numOfMesh)) ;
			sprintf(str_log, "i_smr_read_smr_meshinfo::ERR: file invalid : file_size(%d)/(%d)\n",
                              	(int)file_stat.st_size, sizeof(int) + (SMESH_INFO_SIZE * m_rank_manager.numOfMesh)) ;

			ret = ERR_SMR_MESHINFO_DATA ;
			goto error_exit ;
		}
	}
	else {
		D("ERR: file invalid\n") ;
		sprintf(str_log, "i_smr_read_smr_meshinfo::ERR: file invalid\n") ;
		ret = ERR_SMR_MESHINFO_HANDLE ;
		goto error_exit ;
	}
	//////////////////////////////////////////////////////////////////////

	fseek(f_smr_meshinfo, 0, SEEK_SET) ;
	fread(&check_sum, sizeof(int), 1, f_smr_meshinfo) ; //4바이트 체크
//	D("check_sum[%d]\n", check_sum) ;

	m_meshinfo_checksum = check_sum ;

	smr_mesh_info_size = m_rank_manager.numOfMesh * SMESH_INFO_SIZE ;		// SMESH_INFO one pack size is 10
	// 체크섬 뺀거
	if((info = (char *) malloc (m_rank_manager.numOfMesh * SMESH_INFO_SIZE)) == NULL) { 
		D("ERR: malloc fail\n") ;
		sprintf(str_log, "i_smr_read_smr_meshinfo::ERR: malloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(info, 0, m_rank_manager.numOfMesh * SMESH_INFO_SIZE) ;
	fread(info, m_rank_manager.numOfMesh * SMESH_INFO_SIZE, 1, f_smr_meshinfo) ; 

	temp_check_sum = i_smr_check_sum((short unsigned int*) info, m_rank_manager.numOfMesh * SMESH_INFO_SIZE) ;
	if(check_sum != temp_check_sum) {
		D("ERR: not match check sum[%d] vs cal_check_sum[%d]\n", check_sum, temp_check_sum) ;
		sprintf(str_log, "i_smr_read_smr_meshinfo::ERR: not match check sum[%d] vs cal_check_sum[%d]\n", check_sum, temp_check_sum) ;
		//	ret = ERR_NOTMATCH_CHECKSUM ;
		ret = ERR_SMR_MESHINFO_CHECKSUM ;
		goto error_exit ;
	}
//	D("temp_check_sum[%d]\n", temp_check_sum) ;

	if(m_mesh_info != NULL) free(m_mesh_info) ;
	m_mesh_info = NULL ; // 포인터 초기화 

	m_max_mesh_size = 0 ;

	if((m_mesh_info = (SMESH_INFO *) malloc (sizeof(SMESH_INFO) * m_rank_manager.numOfMesh)) == NULL) {
		D("ERR: malloc fail\n") ;
		sprintf(str_log, "i_smr_read_smr_meshinfo::ERR: malloc fail\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(m_mesh_info, 0, sizeof(SMESH_INFO) * m_rank_manager.numOfMesh) ;

	if(m_rank_manager.numOfMesh > 0) {
		for(i = 0 ; i < m_rank_manager.numOfMesh ; i++) {
			memset(&temp, 0, sizeof(temp)) ;

			memcpy(&temp.meshId, &info[offset], 2) ;
			offset += 2 ;
			memcpy(&temp.ds, &info[offset], 4) ;
			offset += 4 ;
			memcpy(&temp.size, &info[offset], 4) ;
			offset += 4 ;

			if(temp.size > m_max_mesh_size) m_max_mesh_size = temp.size ;	//get max mesh size in meshs

			memcpy(&m_mesh_info[i], &temp, sizeof(SMESH_INFO)) ;

			total_size += temp.size ;
		}
	}
//	D("smrmeshdata total size[%d]\n", total_size) ;
	////////////////////

error_exit :
	if(info != NULL) free(info) ;
	info = NULL ;

	return ret ;
}

//smrmanage.bin ¿¡¼­ SMAP_HEADER / SRANK_MANAGER / SRANK_MAP ¸¦ ÀÐ¾îµå¸°´Ù.
int i_smr_read_smr_manage(void)
{
	int ret = NO_ERR ;

	int offset = 0 ;
	char *buf = NULL ;
	int buf_size = 0 ;

//	int i = 0 ;
	int check_sum = 0 ;
	int temp_sum = 0 ;

	struct stat file_stat ;

	if(0 == fstat(fileno(f_smr_manage), &file_stat)) {
		buf_size = (int)file_stat.st_size ;	// 파일 전체 사이즈 (바이트 수)
		if(buf_size <= 4) {		// sizeof(checksum)ÀÌ 4º¸´Ù ÀÛ´Ù¸é
			D("ERR: file invalid : file_size(%d)\n", buf_size) ;
			sprintf(str_log, "ERR: file invalid : file_size(%d)\n", buf_size);
		//	ret = ERR_INVALID_DATA ;
			ret = ERR_SMR_MANAGE_DATA ;
			goto error_exit ;
		}
	//	D("buf_size[%d]\n", buf_size) ;
		// f_smr_manage ÀüÃ¼ µ¥ÀÌÅÍ¸¦ ÀÐ¾î¿Â´Ù.
		if((buf = (char *) malloc (buf_size)) == NULL) { 
			D("ERR: memory alloc fail\n") ;
			sprintf(str_log, "ERR: memory alloc fail\n");
			ret = ERR_MALLOC_FAIL ;
			goto error_exit ;
		}
		memset(buf, 0, buf_size) ;
		fseek(f_smr_manage, 0, SEEK_SET) ;
		fread(buf, buf_size, 1, f_smr_manage) ;

		memcpy(&check_sum, &buf[offset], sizeof(int)) ; // 첫 4바이트 체크섬
		offset += sizeof(int) ; // 첫 4바이트 checksum

		temp_sum = i_smr_check_sum((unsigned short *) &buf[offset], buf_size - sizeof(int)) ;
		if(check_sum != temp_sum) {
			D("ERR: file invalid : check_sum[%d] vs temp_sum[%d]\n", check_sum, temp_sum) ;
			sprintf(str_log, "ERR: file invalid : check_sum[%d] vs temp_sum[%d]\n", check_sum, temp_sum);
		//	ret = ERR_CHECK_SUM ;
			ret = ERR_SMR_MANAGE_CHECKSUM ;
			goto error_exit ;
		}

		m_manage_checksum = check_sum ;
	}
	else {
		D("ERR: file invalid\n") ;
		sprintf(str_log, "ERR: file invalid\n");
	//	ret = ERR_BAD_HANDLE ;
		ret = ERR_SMR_MANAGE_HANDLE ;
		goto error_exit ;
	}
	//checksum 확인
	memset(&m_map_header, 0, sizeof(SMAP_HEADER)) ; // 1byte 변수가 아닐때 0이 아닌 수로 초기화하면 원하지 않는값이 들어감
	memset(&m_rank_manager, 0, sizeof(SRANK_MANAGER)) ;
	if(m_rank_map != NULL) free(m_rank_map) ;
	m_rank_map = NULL ;

	memcpy(&m_map_header, &buf[offset], sizeof(m_map_header)) ;
	offset += sizeof(m_map_header) ;

	///[DEBUG]//////
//	D("version[%.16s] volumeId[%.16s] systemId[%.16s] createDate[%.16s] updateDate[%.16s] "
//		"centerLon[%d] centerLat[%d]\n",
//		m_map_header.version, m_map_header.volumeId, m_map_header.systemId,
//		m_map_header.createdDate, m_map_header.updatedDate,
//		m_map_header.centerLon, m_map_header.centerLat) ;

	memcpy(&m_rank_manager, &buf[offset], sizeof(m_rank_manager)) ;
	offset += sizeof(m_rank_manager) ;
//	D("numOfMesh[%d] meshWidth[%d] meshHeight[%d] left[%d] bottom[%d] right[%d] top[%d]\n",
//		m_rank_manager.numOfMesh, m_rank_manager.meshWidth, m_rank_manager.meshHeight,
//		m_rank_manager.left, m_rank_manager.bottom, 
//		m_rank_manager.right, m_rank_manager.top) ;

	if(m_rank_manager.numOfMesh > 0) {
		if((m_rank_map = (SRANK_MAP *) malloc (sizeof(SRANK_MAP) * m_rank_manager.numOfMesh)) == NULL) { 
			D("ERR: malloc fail\n") ;
			sprintf(str_log, "ERR: malloc fail\n");
			ret = ERR_MALLOC_FAIL ;
			goto error_exit ;
		}
		memset(m_rank_map, 0, sizeof(SRANK_MAP) * m_rank_manager.numOfMesh) ;
		fread(m_rank_map, sizeof(SRANK_MAP) * m_rank_manager.numOfMesh, 1, f_smr_manage) ; // 위에서 커서 이동 되는듯
		memcpy(m_rank_map, &buf[offset], sizeof(SRANK_MAP) * m_rank_manager.numOfMesh) ;
		offset += sizeof(SRANK_MAP) * m_rank_manager.numOfMesh ;
		
	//	D("sizeof(%d) SRANK_MAP-->count(%d)total(%d)\n", sizeof(SRANK_MAP), 
	//		m_rank_manager.numOfMesh, m_rank_manager.numOfMesh * sizeof(SRANK_MAP)) ;
	}
error_exit:
	if(buf != NULL) free(buf) ;
	buf = NULL ;

	return ret ;
}

int i_smr_re_open(void)
{
	int ret = NO_ERR ;

	ret = i_smr_close() ;
	if(ret == NO_ERR) ret = i_smr_open() ;

	return ret ;
}


int i_smr_open_manage(void)
{
	char file_name[FILE_LENGTH] = "" ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MANAGE) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_manage = fopen(file_name, "r")) == NULL) return ERR_OPEN_SMR_MANAGE ;
	return NO_ERR ;
}

int i_smr_open_meshinfo(void)
{
	char file_name[FILE_LENGTH] = "" ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHINFO) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_meshinfo = fopen(file_name, "r")) == NULL) return ERR_OPEN_SMR_MESHINFO ;
	return NO_ERR ;
}

int i_smr_open_meshdata(void)
{
	char file_name[FILE_LENGTH] = "" ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHDATA) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_meshdata = fopen(file_name, "r")) == NULL) return ERR_OPEN_SMR_MESHDATA ;
	return NO_ERR ;
}

int i_smr_open_cacheinfo(void)
{
	char file_name[FILE_LENGTH] = "" ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_CACHEINFO) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_cacheinfo = fopen(file_name, "r")) == NULL) return ERR_OPEN_SMR_CACHEINFO ;
	return NO_ERR ;
}


int i_smr_open(void)		
{
	char file_name[FILE_LENGTH] = "" ;

	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MANAGE) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_manage = fopen(file_name, "r")) == NULL) return ERR_OPEN_SMR_MANAGE ;

	memset(file_name, 0, FILE_LENGTH) ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHINFO) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_meshinfo = fopen(file_name, "r")) == NULL) return ERR_OPEN_SMR_MESHINFO ;

	memset(file_name, 0, FILE_LENGTH) ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_MESHDATA) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_meshdata = fopen(file_name, "r")) == NULL) return ERR_OPEN_SMR_MESHDATA ;

	memset(file_name, 0, FILE_LENGTH) ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, FILENAME_SMR_CACHEINFO) ;
//	D("file_name[%s]\n", file_name) ;
	if ((f_smr_cacheinfo = fopen(file_name, "r")) == NULL) return ERR_OPEN_SMR_CACHEINFO ;

	return NO_ERR ;
}

void i_smr_close_manage(void)
{
	if(f_smr_manage != NULL) fclose(f_smr_manage) ;
	f_smr_manage = NULL ;
}

void i_smr_close_meshinfo(void)
{
	if(f_smr_meshinfo != NULL) fclose(f_smr_meshinfo) ;
	f_smr_meshinfo = NULL ;
}

void i_smr_close_cacheinfo(void)
{
	if(f_smr_cacheinfo != NULL) fclose(f_smr_cacheinfo) ;
	f_smr_cacheinfo = NULL ;
}

void i_smr_close_meshdata(void)
{
	if(f_smr_meshdata != NULL) fclose(f_smr_meshdata) ;
	f_smr_meshdata = NULL ;
}

int i_smr_close(void)
{
	if(f_smr_manage != NULL) fclose(f_smr_manage) ;
	f_smr_manage = NULL ;
	if(f_smr_meshinfo != NULL) fclose(f_smr_meshinfo) ;
	f_smr_meshinfo = NULL ;
	if(f_smr_cacheinfo != NULL) fclose(f_smr_cacheinfo) ;
	f_smr_cacheinfo = NULL ;
	if(f_smr_meshdata != NULL) fclose(f_smr_meshdata) ;
	f_smr_meshdata = NULL ;

	return NO_ERR ;
}

int i_smr_file_exist(char *fileName)
{
	int ret = NO_ERR ;
	
	char file_name[FILE_LENGTH] = "" ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, fileName) ;
//	D("file_name[%s]\n", file_name) ;

	ret = access(file_name, F_OK) ;	//ÆÄÀÏÀÌ Á¸ÀçÇÏ´Â°¡ // 파일이 읽기 가능한 파일인지 검사 F_OK 파일 존재 여부(0리턴 정상)

    if(ret!=NO_ERR)
    {
        sprintf(str_log, "i_smr_file_exist::file_name [%s] not exist", fileName) ;
    }
	return ret ;
}

int i_smr_file_delete(char *fileName)
{
	int ret = NO_ERR ;
	char file_name[FILE_LENGTH] = "" ;
	snprintf(file_name, FILE_LENGTH, "%s/%s", m_dir, fileName) ;

	ret = remove(fileName) ;
	return ret ;
}

int i_smr_set_dir(char *dirPath)
{
	memset(m_dir, 0, FILE_LENGTH) ; 
	snprintf(m_dir, FILE_LENGTH, "%s", dirPath) ;
//	D("m_dir[%s]\n", dirPath) ;
    //sprintf(str_log, "m_dir[%s]", dirPath) ;

	return NO_ERR ;
}

int i_smr_get_dir(char *dirPath)
{
	strncpy(dirPath, m_dir, FILE_LENGTH) ;
	return NO_ERR ;
}

void i_smr_create(void)
{
	memset(m_dir, 0, FILE_LENGTH) ;
	f_smr_manage = NULL ;
	f_smr_meshinfo = NULL ;
	f_smr_cacheinfo = NULL ;
	f_smr_meshdata = NULL ;

	m_manage_checksum = 0 ;
	m_meshinfo_checksum = 0 ; 
	m_max_mesh_size = 0 ; // 초기화 홍현기///////////////////////////
	memset(&m_map_header, 0, sizeof(SMAP_HEADER)) ;
	memset(&m_rank_manager, 0, sizeof(SRANK_MANAGER)) ;
	m_rank_map = NULL ;
	m_mesh_info = NULL ;
	m_cache_info = NULL ;
	//Ãß°¡
	m_mapping = NULL ;
	//2013.06.27
	r_idx = NULL ;
	r_idx_count = 0 ;
}

void i_smr_destruct(void)
{
	memset(m_dir, 0, FILE_LENGTH) ;
	memset(&m_map_header, 0, sizeof(SMAP_HEADER)) ;
	memset(&m_rank_manager, 0, sizeof(SRANK_MANAGER)) ;
	m_manage_checksum = 0 ;
	m_meshinfo_checksum = 0 ;

	if(m_rank_map != NULL) free(m_rank_map) ;
	m_rank_map = NULL ;
	if(m_mesh_info != NULL) free(m_mesh_info) ;
	m_mesh_info = NULL ;
	if(m_cache_info != NULL) free(m_cache_info) ;
	m_cache_info = NULL ;
	//Ãß°¡
	if(m_mapping != NULL) free(m_mapping) ;
	m_mapping = NULL ;

}

unsigned short i_smr_check_sum(unsigned short *buf, int len)
{
	unsigned int sum = 0;
	while (len > 1) {
		sum += *(buf++);
		len -= sizeof(unsigned short); // 2byte씩 넘겨 가면서 sum
	}
	if (len) {
		sum += *(unsigned char*)buf;
	}
	//sum = 0xffff 
	sum = (sum & 0xffff) + (sum >> 16);
	sum += (sum >> 16);

	return (unsigned short)(~sum);
}

///2013.06.27
int ridx_get(int map_id, int *entry)
{
	int ret = ERR_NOT_FOUND ;
	int i = 0 ;

	for(i = 0 ; i < r_idx_count ; i++) {
		if(r_idx[i].map_id == map_id) {
			ret = NO_ERR ;
			*entry = i ;
			break ;
		}
	}

	return ret ;
}

int ridx_set_entry(int *entry)
{
	int ret = NO_ERR ;
	int i = 0 ;
	int min = 0 ;
	int min_entry = 0 ;
	int max_order = 0 ;

	min_entry = 0 ;
	min = r_idx[0].order ;
	max_order = r_idx[0].order ;

	for(i = 0 ; i < r_idx_count ; i++) {
		if(r_idx[i].order < min)  {
			min = r_idx[i].order ; // 최소값 저장
			min_entry = i ; // order의 최소값을 찾고 index를 저장한다
		}
		if(r_idx[i].order > max_order) { // 최대값일 경우
			max_order = r_idx[i].order ; //max_order를 찾는다
		}
	}
	max_order += 1 ; // 최대값 + 1 
	r_idx[min_entry].order = max_order ;
//	r_idx[min_entry].size = sz ;

	memset(r_idx[min_entry].data, 0, m_max_mesh_size) ;
	*entry = min_entry ;
//	memcpy(r_idx[min_entry].data, ptr, sz) ;
//	*ptr = r_idx[min_entry].data ;

	return ret ;
}

int ridx_init(int sz)
{//ridx_init(m_max_mesh_size)
	int ret = NO_ERR ;
	int i = 0 ;

	if((sz <= 0) || (sz > 2*1024*1024)) { // 2MB 미만
		D("ERR: sz(%d) is invalid\n", sz) ;
		ret = ERR_BAD_PARAM ;
		goto error_exit ;
	}

	r_idx_count = 1024*1024/sz ; // 2*1024*1024 >= sz > 0  
	if(r_idx_count == 0) r_idx_count = 1 ;
	D("sz[%d]==>r_idx_count[%d]\n", sz, r_idx_count) ;

	if((r_idx = (R_IDX *) malloc (r_idx_count * sizeof(R_IDX))) == NULL) {
		D("ERR: malloc fail(r_idx)\n") ;
		ret = ERR_MALLOC_FAIL ;
		goto error_exit ;
	}
	memset(r_idx, 0, r_idx_count * sizeof(R_IDX)) ;

	for(i = 0 ; i < r_idx_count ; i++) {
		if((r_idx[i].data = (char *) malloc (sz)) == NULL) {
			D("ERR: malloc fail(r_idx[%d])\n", i) ;
			ret = ERR_MALLOC_FAIL ;
			goto error_exit ;
		}
		memset(r_idx[i].data, 0, sz) ;
		r_idx[i].map_id = -1 ;		//2013.07.02
	}

error_exit:
	if(ret != NO_ERR) {
		if(r_idx != NULL) {
			for(i = 0 ; i < r_idx_count ; i++) {
				if(r_idx[i].data != NULL) free(r_idx[i].data) ;
				r_idx[i].data = NULL ;
			}
			free(r_idx) ;
			r_idx = NULL ;
		}
	}

	return ret ;
}

void ridx_end(void)
{
	int i = 0 ;
	for(i = 0 ; i < r_idx_count ; i++) {
		if(r_idx[i].data != NULL) free(r_idx[i].data) ;
		r_idx[i].data = NULL ;
	}
	if(r_idx != NULL) free(r_idx) ;
	r_idx = NULL ;

	r_idx_count = 0 ;
}

////[END OF SOURCE]////////////////////////////////

