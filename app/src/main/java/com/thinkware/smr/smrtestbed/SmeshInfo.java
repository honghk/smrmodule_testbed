package com.thinkware.smr.smrtestbed;

/**
 * Created by honghk on 2016. 12. 12..
 */

public class SmeshInfo {
    short meshId;                   // 메쉬ID
    int ds;                         // 해당 메쉬 데이터 DS
    int size;                       // 해당 메쉬 데이터 크기
}
