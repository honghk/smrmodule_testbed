package com.thinkware.smr.smrtestbed;

import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by honghk on 2016. 12. 8..
 */

public class SmrControl {

    private final static String TAG = "SmrControl";
//
    //////////////////////////////////////////////////////////

    private final static int SUCCESS                    = 0;
    private final static int ERR_FAIL                   = -1;
    private final static int ERR_UNKNOWN                = -1;
    private final static int ERR_INVALID_OPERATION      = -12;
    private final static int ERR_MALLOC_FAIL            = -13;
    private final static int ERR_NOTMATCH_CHECKSUM      = -14;
    private final static int ERR_BAD_PARAM              = -15;
    private final static int ERR_NULL                   = -16;
    private final static int ERR_INVALID_DATA           = -17;
    private final static int ERR_FILE_OPEN              = -18;
    private final static int ERR_BAD_HANDLE             = -19;
    private final static int ERR_WRITE_FAIL             = -20;
    private final static int ERR_NOT_FOUND              = -21;
    private final static int ERR_EMPTY_DATA             = -22;
    private final static int ERR_CHECK_SUM              = -23;
    private final static int ERR_OPEN_SMR_MANAGE        = -24;
    private final static int ERR_OPEN_SMR_MESHINFO      = -25;
    private final static int ERR_OPEN_SMR_CACHEINFO     = -26;
    private final static int ERR_OPEN_SMR_MESHDATA      = -27;
    private final static int ERR_LIMIT                  = -28;
    private final static int ERR_SMR_MANAGE_CHECKSUM    = -29;
    private final static int ERR_SMR_MESHINFO_CHECKSUM  = -30;
    private final static int ERR_SMR_CACHEINFO_CHECKSUM = -31;
    private final static int ERR_SMR_MESHDATA_CHECKSUM  = -32;
    private final static int ERR_SMR_MANAGE_HANDLE      = -33;
    private final static int ERR_SMR_MESHINFO_HANDLE    = -34;
    private final static int ERR_SMR_CACHEINFO_HANDLE   = -35;
    private final static int ERR_SMR_MESHDATA_HANDEL    = -36;
    private final static int ERR_SMR_MANAGE_DATA        = -37;
    private final static int ERR_SMR_MESHINFO_DATA      = -38;
    private final static int ERR_SMR_CACHEINFO_DATA     = -39;
    private final static int ERR_SMR_MESHDATA_DATA      = -40;
    private final static int ERR_OPEN_SMR               = 300;

    //통신에러코드//////////////////////////////////////////////

    public final static int ERR_HTTP_TCP			= 1000 ;	//소켓 통신 실패
    public final static int ERR_HTTP_EMPTY_DATA		= 1001 ;

    //////////////////////////////////////////////////////////

    public final static int SIDX_SIZE = 50;
    public final static int MAX_FILE_WRITE_SIZE	= 1073741824;

    //////////////////////////////////////////////////////////

    private jcSMR m_jcSMR = null;
    private HttpDataServer m_httpDataServer = null;
    private MainActivity m_mainActivity = null;
    private NodeActivity m_nodeActivity = null;
    private LinkActivity m_linkActivity = null;
    private SrankMapActivity m_srankMapActivity = null;

    private SrankMap m_srankMap = null;

    private String m_dir = null;
    private String m_str_url = null;
    private byte[] m_file_data = null;

    private boolean m_isPlay = false;

    //////////////////////////////////////////////////////////

    public SmrControl(){//
        m_jcSMR = new jcSMR();
        m_httpDataServer = new HttpDataServer();
    }//

    public SmrControl(MainActivity mainActivity){//
        m_jcSMR = new jcSMR();
        m_httpDataServer = new HttpDataServer();
        m_mainActivity = mainActivity;
    }//

    public SmrControl(NodeActivity nodeActivity){//
        m_jcSMR = new jcSMR();
        m_httpDataServer = new HttpDataServer();
        m_nodeActivity = nodeActivity;
    }//

    public SmrControl(LinkActivity linkActivity){//
        m_jcSMR = new jcSMR();
        m_httpDataServer = new HttpDataServer();
        m_linkActivity = linkActivity;
    }//

    public SmrControl(SrankMapActivity srankMapActivity){//
        m_jcSMR = new jcSMR();
        m_httpDataServer = new HttpDataServer();
        m_srankMapActivity = srankMapActivity;
    }//

    public void setIsPlay(boolean isPlay_main){
        m_isPlay = isPlay_main;
    }

    public Runnable m_smrTread = new Runnable() {
        @Override
        public void run() {

                try{
                    Log.v("thread", "running");
                    receiveMapIdMeshData();

                }catch(Exception e){

                    Thread.currentThread().interrupt();
                    Log.v(TAG, "m_smrTread::Exception : " + e);
            }
        }
    };

    public Snode[] getNode(int map_id){
        int i = 0;
        int ret = SUCCESS;
        Snode[] m_snodes = null;

        SrankMap srankMap = getSrankMap(map_id);

        if(m_srankMap.numOfNode >= 0){

            m_snodes = new Snode[m_srankMap.numOfNode];

            for(i = 0; i < m_srankMap.numOfNode ; i++){
                m_snodes[i] = new Snode();
            }

            ret = m_jcSMR.getNode(map_id, m_snodes);

            for(i = 0; i < srankMap.numOfNode; i++) {

                m_snodes[i].lat = m_snodes[i].offLat + srankMap.left; // offlat이 char형이라 변환겸오프셋계산
                m_snodes[i].lon = m_snodes[i].offLon + srankMap.bottom;
            }

            if(ret!=SUCCESS){
                m_snodes = null;
                m_nodeActivity.logCall(m_jcSMR.logCall() + "::ret(에러코드) : " + ret);
            }

            m_jcSMR.test3();

        }else{
            m_snodes = null;
        }

        return m_snodes;
    }

    public Slink[] getSlink(int map_id){
        int i = 0;
        int j = 0;
        int ret = SUCCESS;
        Slink[] m_slinks = null;
        String str = null;

        if(m_srankMap.numOfLink >= 0){

            m_slinks = new Slink[m_srankMap.numOfLink];

            for(i = 0; i < m_srankMap.numOfLink; i++){

                m_slinks[i] = new Slink();
                if(i == m_srankMap.numOfLink-1) break;
            }
            ret = m_jcSMR.getSlink(map_id, m_slinks);
            if(ret != SUCCESS){
                m_slinks = null;
                m_linkActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);//sprintf(str_log, "i_smr_write_smr_manage::ERR:fopen(%s) fail\n", file_smr_manage);
                return m_slinks;
            }

            for(i = 0; i < m_srankMap.numOfLink; i++){

                m_slinks[i].SVertex(m_slinks[i]._numOfVertex);

                for(j = 0;  j < m_slinks[i]._numOfVertex; j++){

                    m_jcSMR.getSvertex(map_id, i, j, m_slinks[i].sVertex[j]);

                    if(ret != SUCCESS){
                        m_slinks[i].sVertex = null;
                        m_linkActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);//sprintf(str_log, "i_smr_write_smr_manage::ERR:fopen(%s) fail\n", file_smr_manage);
                    }

                }

                if(i==m_srankMap.numOfLink-1) break;
            }

        }else{
            str = null;
            m_slinks = null;
        }

        if(str != null ){
            Log.v("node", str);
            Log.v("node", "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ"); //39, 49, 437, 1234
        }

        return m_slinks;
    }

    public SrankManager getSrankManager(){
        int ret = SUCCESS;
        SrankManager srankManager = new SrankManager();
        ret = m_jcSMR.getSrankManager(srankManager);

        if(ret!=SUCCESS){
            m_mainActivity.logCall("getSrankManager::ERR\n");
        }

        return srankManager;
    }

    public SmapHeader getSmapHeader(){
        int ret = SUCCESS;
        SmapHeader smapHeader = new SmapHeader();
        ret = m_jcSMR.getSmapHeader(smapHeader);
        if(ret!=SUCCESS){
            m_mainActivity.logCall("getSmapHeader::ERR\n");
        }

        return smapHeader;
    }

    public int getLink(int map_id, int link_id, Slink slink){

        int ret = SUCCESS;
        ret = m_jcSMR.getLink(map_id, link_id, slink);

        if(ret!=SUCCESS){
            m_mainActivity.logCall(m_jcSMR.logCall());
        }

        return ret;
    }

    public SrankMap getSrankMap(int map_id){

        int ret = SUCCESS;

        m_srankMap = new SrankMap() ;

        ret = m_jcSMR.getSrankMap(map_id, m_srankMap);

        if(ret != SUCCESS){

            if(m_mainActivity != null){
                m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
            }else if(m_srankMapActivity != null){
                m_srankMapActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                return null;
            }else if(m_nodeActivity != null){
                m_nodeActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
            }else{
                m_linkActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
            }
        }

        return m_srankMap;
    }

    public  SmeshInfo getSmeshInfo(int map_id){
        int ret = SUCCESS;

        SmeshInfo smeshInfo = new SmeshInfo();
        ret = m_jcSMR.getSmeshInfo(map_id, smeshInfo);

        if(ret != SUCCESS){
            m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
        }

        return smeshInfo;

    }

    public void initSmr(){
        m_jcSMR.init(); // smr_first = 0 / smr_open = 0
        m_jcSMR.start(); // 변수 초기화 / sidx -1로 초기화 / smr_first = 1
    }

    public int startSmr(String str_url, String str_dir){

        int ret = SUCCESS;

        initSmr();

        setDir(str_dir);

        createDirIfNotExists();
        m_str_url = str_url;
        ret = openSmr();
        Log.v("hong", "setIsPlay()");
        //setIsPlay(true);

        return ret;
    }

    public void end(){
        m_jcSMR.close();
        m_jcSMR.end();
    }

    public boolean createDirIfNotExists()
    {
        boolean ret = true;

        //File file = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "smrData" + File.separator + "mrdata");
        File file = new File(m_dir);
        Log.e(TAG, "ERR: createDirIfNotExists::Problem creating folder[" + file.getPath() + "]");
        if (!file.exists()) {
            if (!file.mkdirs()) {
                //Log.e(TAG, "createDirIfNotExists::ERR: Problem creating folder[" + file.getPath() + "]");
                ret = false;
            }
        }
        m_dir = file.getPath();

        return ret;
    }

    public void setDir(String dir) {

        if(dir != null)	m_dir = dir;
        m_jcSMR.setDir(m_dir);
        Log.v("honghk", m_jcSMR.logCall());/////////////////////sprintf(str_log, "i_smr_write_smr_manage::ERR:fopen(%s) fail\n", file_smr_manage);
        //smrMapIndexer.SetDir(n_dir);/// 해야됨

    }

    public int openSmr(){
        int ret = SUCCESS;
        int cnt = 0;
        int receive_flag = 0;
        int cache_flag = 0;

        do {
            cnt++;
            if (cnt > 3) {
                return ERR_OPEN_SMR;
            }

            ret = m_jcSMR.fileExist("smrmanage.bin"); // 0 존재, -1 존재하지 않음

            if (ret != SUCCESS) {
                Log.e(TAG, "ERR : openSmr::fileExist::smrmanage.bin not exist");
                if (receive_flag == 0) {
                    ret = receiveFile(m_str_url, "smrmanage.bin");

                    if (ret != SUCCESS) {
                        m_mainActivity.logCall("ERR : openSmr::receiveFile[smrmanage.bin][" + ret + "]");
                        continue;
                    }

                    try {
                        m_file_data = m_httpDataServer.readData();
                    } catch (InterruptedException interexc) {
                        Log.e(TAG, "openSmr::InterruptedException : " + interexc);
                        continue;
                    }
                    receive_flag = 1;
                }
                ret = m_jcSMR.writeSmrManage(m_file_data);
                if (ret != SUCCESS) {
                    m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                    //Log.e(TAG, "ERR : openSmr::writeSmrManage[smrmanage.bin][" + ret + "]");
                    continue;
                }
            }
            ret = m_jcSMR.openManage();
            if(ret != SUCCESS){
                m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                //Log.e(TAG, "ERR : openSmr::openManage()[" +ret+"]");
                continue;
            }
            ret = m_jcSMR.readSmrManage();
            if(ret != SUCCESS){
                m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                Log.e(TAG, "ERR : openSmr::readSmrManage[" + ret + "]");
                if((ret == ERR_SMR_MANAGE_CHECKSUM) || (ret == ERR_SMR_MANAGE_DATA)) {
                    m_jcSMR.closeManage() ;
                    ret = m_jcSMR.fileDelete("smrmanage.bin") ;

                    if(ret != SUCCESS){
                        m_mainActivity.logCall("i_smr_file_delete::remove[smrmanage.bin] 파일삭제 실패 " + ret);
                    }

                    Log.e(TAG, "openSmr::fileDelete[smrmanage.bin][" + ret + "]") ;
                }
            }


        }while(ret!=SUCCESS);

        cnt = 0;
        receive_flag = 0;

        do{
            cnt++;
            if(cnt > 3){
                return ERR_OPEN_SMR;
            }

            ret = m_jcSMR.fileExist("smrmeshinfo.bin");

            if(ret != SUCCESS){
                if(receive_flag == 0){
                    ret = receiveFile(m_str_url, "smrmeshinfo.bin");
                    if(ret != SUCCESS){
                        m_mainActivity.logCall("ERR : openSmr::receiveFile[smrmeshinfo.bin][" + ret + "]");
                        //Log.e(TAG, "ERR : openSmr::receiveFile[" + ret + "]");
                        continue;
                    }

                    try{
                        m_file_data = m_httpDataServer.readData();
                    }catch(InterruptedException interexc){
                        Log.e(TAG, "openSmr::InterruptedException : " + ret);
                        continue;
                    }
                    receive_flag = 1;
                }
                ret = m_jcSMR.writeSmrMeshinfo(m_file_data);
                if(ret != SUCCESS){
                    m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                    continue;
                }
            }
            ret = m_jcSMR.openMeshinfo();
            if(ret != SUCCESS){
                m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                continue;
            }
            ret = m_jcSMR.readSmrMeshinfo();
            if(ret != SUCCESS){
                Log.e(TAG, "ERR : openSmropenSmr::readSmrMeshinfo[" + ret + "]");
                m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                if((ret == ERR_SMR_MESHINFO_CHECKSUM) || (ret == ERR_SMR_MESHINFO_DATA)){
                    m_jcSMR.closeMeshinfo();
                    ret = m_jcSMR.fileDelete("smrmeshinfo.bin");

                    if(ret != SUCCESS){
                        m_mainActivity.logCall("i_smr_file_delete::remove[smrmeshinfo.bin] 파일삭제 실패 " + ret);
                    }

                    Log.e(TAG, "ERR : openSmr::fileDelete[smrmeshinfo.bin][" + ret + "]");
                }
                continue;
            }
            ret = m_jcSMR.mappingInit();
            if(ret != SUCCESS){
                m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                Log.e(TAG, "ERR : openSmr::mappingInit[" + ret + "]");
            }
            ret = m_jcSMR.ridxInit();
            if(ret != SUCCESS){
                m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                Log.e(TAG, "ERR : ridxInit[" + ret + "]");
                continue;
            }
        }while(ret != SUCCESS);

        cnt = 0;
        do{
            cnt++;
            if(cnt > 3){
                return ERR_OPEN_SMR;
            }
            ret = m_jcSMR.fileExist("smrmeshdata.bin");
            if(ret != SUCCESS){
                Log.e(TAG, "ERR : openSmr::fileExist[smrmeshdata.bin][" + ret + "]");

                ret = m_jcSMR.createSmrMeshdata();
                if(ret != SUCCESS){
                    m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                    Log.e(TAG, "ERR : openSmr::createSmrMeshdata()[" + ret + "]");
                    continue;
                }
            }
            ret = m_jcSMR.openMeshdata();
            if(ret != SUCCESS){
                Log.e(TAG, "ERR : openSmr::openMeshdata()[" + ret + "]");
            }
        }while(ret != SUCCESS);

        cnt = 0;

        do{
            cnt++;
            if(cnt > 3){
                return ERR_OPEN_SMR;
            }
            ret = m_jcSMR.fileExist("smrcacheinfo.bin");
            Log.v("honghk", m_jcSMR.logCall() + "ret(에러코드) : " + ret);
            if(ret != SUCCESS){
                cache_flag = ERR_OPEN_SMR_CACHEINFO;
                ret = m_jcSMR.readCacheinfo(cache_flag);
                if(ret != SUCCESS){
                    m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                    Log.e(TAG, "ERR : openSmr::readCacheinfo()[" + ret + "]");
                    continue;
                }
                ret = m_jcSMR.openCacheinfo();
                if(ret != SUCCESS){
                    m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                    Log.e(TAG, "ERR : openSmr::openCacheinfo()[" + ret + "]");
                    continue;
                }
            }else{
                ret = m_jcSMR.openCacheinfo();
                if(ret != SUCCESS){
                    m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                    Log.e(TAG, "ERR : openSmr::openCacheinfo[" + ret + "]");//
                    continue;
                }
                ret = m_jcSMR.readCacheinfo(0);
                if(ret != SUCCESS){
                    m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
                    Log.e(TAG, "openSmr::readCacheinfo()[" + ret + "]");

                    if((ret == ERR_SMR_CACHEINFO_CHECKSUM) || (ret == ERR_SMR_CACHEINFO_DATA)){
                        m_jcSMR.closeCacheinfo();
                        ret = m_jcSMR.fileDelete("smrcacheinfo.bin");
                        Log.e(TAG, "openSmr::fileDelete[smrcacheinfo.bin][" + ret + "]");
                    }
                }
            }
        }while(ret != SUCCESS);

        if(ret == SUCCESS){
            m_jcSMR.openSet(1);
        }

        return ret;
    }

    public int receiveFile(String str_url, String file_name){

        int ret = SUCCESS;
        String full_url = null;
        int read_sum = 0;
        int check_sum = 0;

        full_url = str_url + file_name;

        ret = m_httpDataServer.connectSmrData(full_url);
        if(ret == SUCCESS){

            try {
                m_file_data = m_httpDataServer.readData();

                if (m_file_data == null) {
                    Log.e(TAG, "ERR : receiveFile::파일을 읽었을때 데이터가 존재하지 않음 filename[" + file_name + "]" );
                    m_mainActivity.logCall("ERR : receiveFile::파일을 읽었을때 데이터가 존재하지 않음 filename[" + file_name + "]" );
                    ret = ERR_UNKNOWN;
                }else{
                    if (m_file_data.length > 4) {
                        ByteBuffer byteBuffer = ByteBuffer.wrap(m_file_data); // 배열이 해당 버퍼에 put 되어서 클래스형 객체 리턴 => 버퍼생성과 동시에 데이터 저장
                        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                        read_sum = byteBuffer.getInt(); // read 4byte
                        check_sum = checkSum(m_file_data);

                        if (read_sum != check_sum) {
                            Log.e(TAG, "receiveFile::check_sum[" + check_sum + "] read_sum[" + read_sum + "]");
                            m_mainActivity.logCall("receiveFile::check_sum[" + check_sum + "] read_sum[" + read_sum + "]");
                            ret = ERR_CHECK_SUM;
                        }

                    }
                }

            }catch (InterruptedException interexc) {
                Log.e(TAG, "receiveFile::InterruptedException : " + interexc);
            }
        }
        Log.v(TAG, "receiveFile::receiveFile[" + file_name + "] result[" + ret + "]" );

        return ret;

    }

    public int receiveMapIdMeshData() {

        int ret = SUCCESS;
        int i = 0;
        int cnt = 0;
        String file_path = null;
        String file_name = null;
        int temp_path = 0;
        int total_length = 0;

        byte[] total_data = null;

        int[] sidx = new int[SIDX_SIZE];
        int[] get_sidx = new int[SIDX_SIZE];
        int[] get_offset = new int[SIDX_SIZE];

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        for(i = 0; i < SIDX_SIZE; i++){
            sidx[i] = -1;
            get_sidx[i] = -1;
            get_offset[i] = -1;
        }
        ret = m_jcSMR.sidxMultiGet(sidx);

        if(ret != SUCCESS){
            setIsPlay(false);
            m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
            return ret;
        }

        for(i = 0; i < SIDX_SIZE; i++){
            if (sidx[i] == -1){
                break;
            }
        }

        if(sidx[0] == -1){ // 가져올 데이터가 없으면 종료
            return ret;
        }

        for(i = 0; i < SIDX_SIZE; i++){
            if (sidx[i] != -1){

                if((sidx[i] >= 0) && (sidx[i] <= Integer.MAX_VALUE)){

                    temp_path = sidx[i] / 1000;
                    file_path = String.format("%s%010d/", m_str_url, temp_path); //2000번대 파일 폴더이름 "00002" 아래 있음
                    file_name = String.format("smr_%010d.bin", sidx[i]); // 10자리 표현

                    ret = receiveFile(file_path, file_name);
                }else{
                    ret = ERR_FAIL;
                }

                if(ret == SUCCESS){

                    try{
                        m_file_data = m_httpDataServer.readData();
                    }catch (InterruptedException interexc) {
                        Log.e(TAG, "receiveMapIdMeshData::InterruptedException : " + interexc);
                        return ret;
                    }

                    byteArrayOutputStream = new ByteArrayOutputStream();

                    try{
                        byteArrayOutputStream.write(m_file_data);
                    }catch(IOException ioexc){
                        Log.e(TAG, "receiveMapIdMeshData::IOException : " + ioexc);
                        return ret;
                    }

                    get_sidx[cnt] = sidx[i];
                    get_offset[cnt] = total_length;
                    total_length += m_file_data.length;

                    cnt++;

                    if(total_length > MAX_FILE_WRITE_SIZE){
                        break;
                    }

                }
            }else{
                break;
            }
        }

        total_data = byteArrayOutputStream.toByteArray();
        if(total_data == null){
            return ret ;
        }
        if(total_data.length <= 0){
            return ret;
        }
        ret = m_jcSMR.writeMeshdata(total_data, get_sidx, get_offset, cnt);

        if(ret != SUCCESS){
            setIsPlay(false);
            m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
            Log.e(TAG, "ERR : receiveMapIdMeshData::writeMeshdata ret : " + ret);
            ret = m_jcSMR.openMeshdata();

            if(ret != SUCCESS){
                Log.e(TAG, "ERR : receiveMapIdMeshData::openMeshdata ret : " + ret);
            }
        }

        return ret;
    }

    public int sidxInsert(int map_id){
        int ret = SUCCESS;

        ret = m_jcSMR.sidxInsert(map_id);
        if(ret!=SUCCESS){
            m_mainActivity.logCall(m_jcSMR.logCall() + "ret(에러코드) : " + ret);
        }

        return ret;
    }

    private int checkSum(byte[] data){
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data, 4, (int)(data.length-4));
        DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);

        long sum_long = 0;
        try {
            int cnt = dataInputStream.available();
            while(cnt>1){
                sum_long += byteUnsignedShort(dataInputStream);
                cnt = dataInputStream.available();
            }
            if(cnt == 1){
                sum_long += byteUnsignedChar(dataInputStream);
            }
            sum_long = (sum_long & 0xffff) + (sum_long >> 16);
            sum_long += (sum_long >> 16);
            sum_long = (~sum_long);
            sum_long = (sum_long & 0xffffL);

        }catch(IOException ioexc){
            Log.e(TAG,"checkSum::IOException : " + ioexc);
        }finally{
            try {
                if(dataInputStream != null){
                    dataInputStream.close();
                }
                if(byteArrayInputStream != null){
                    byteArrayInputStream.close();
                }
            }catch(IOException ioexc){
                Log.e(TAG, "checkSum::dataInputStream.close::IOException : " + ioexc);
            }
        }
        return (int)sum_long;
    }

    private int byteUnsignedShort(DataInputStream dataInputStream){/////////다시보기////////////
        byte[] value = new byte[2];
        int num_0 = 0;
        int num_1 = 0;
        try{
            dataInputStream.read(value, 0, value.length);
            num_0 = value[0] & 0xff;
            num_1 = value[1] & 0xff;
            return (num_1<<8) + (num_0<<0);

        }catch (IOException ioexc){
            Log.e(TAG, "byteUnsignedShort::IOException : " + ioexc);
        }
        return -225;
    }

    private int byteUnsignedChar(DataInputStream dataInputStream){/////////다시보기////////////
        byte[] value = new byte[1];
        try {
            dataInputStream.read(value, 0, value.length);
            int num = value[0] & 0xff;
            return (num<<0);

        }catch (IOException ioexc){
            Log.e(TAG, "byteUnsignedChar::IOException : " + ioexc);
        }
        return -225;
    }

}













