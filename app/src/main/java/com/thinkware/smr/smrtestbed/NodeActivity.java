package com.thinkware.smr.smrtestbed;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class NodeActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    private int m_mapId = -1;
    private int m_startNode = -1;
    private int m_endNode = -1;
    private String m_filter_type = null;
    private String m_filter_value = null;

    private Snode[] m_snodes = null;

    private ArrayAdapter<CharSequence> m_adapter_nodeOption = null;
    private ArrayAdapter<CharSequence> m_adapter_nodeType = null;
    private ListAdapterNode m_listAdapterNode = null;

    private Spinner m_spinner_nodeType = null;
    private Spinner m_spinner_nodeOption = null;
    private Button m_button_nodeSearch = null;
    private EditText m_editText_nodeSearch = null;
    private TextView m_textViewNodeList = null;
    //branch test(keyvaluetextdivide
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_textver);

        if(m_button_nodeSearch == null){
            m_button_nodeSearch = (Button) findViewById(R.id.button_node_search);
            m_button_nodeSearch.setOnClickListener(this);
        }

        if(m_editText_nodeSearch == null){
            m_editText_nodeSearch = (EditText) findViewById(R.id.editText_nodeSearch);
            m_editText_nodeSearch.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(6) }); //입력숫자개수 제한
        }
        m_editText_nodeSearch.setVisibility(View.INVISIBLE);

        ListView listViewNodelist = (ListView) findViewById(R.id.listViewNodelist);
        ArrayList<Snode> listItem = new ArrayList<Snode>();
        SmrControl smrControl = new SmrControl(this);

        int i = 0;

        final Intent intent = getIntent();

        m_mapId = intent.getIntExtra("map_id", -1);
        m_startNode = intent.getIntExtra("start_node_id", -1);
        m_endNode = intent.getIntExtra("end_node_id", -1);

        SrankMap srankMap = smrControl.getSrankMap(m_mapId);

        if(m_textViewNodeList == null){
            m_textViewNodeList = (TextView) findViewById(R.id.textView_nodelist);
        }
        m_textViewNodeList.setText("map id : " + m_mapId + " / node cnt : " + srankMap.numOfNode);

        m_snodes = smrControl.getNode(m_mapId);

        if(m_snodes == null){
            return;
        }

        for(i = 0; i < srankMap.numOfNode; i++){
            m_snodes[i].nodeId = i;
            listItem.add(m_snodes[i]);
        }

        m_listAdapterNode = new ListAdapterNode(this, R.layout.nodelist_nodeitems_textver, listItem);
        listViewNodelist.setAdapter(m_listAdapterNode);

        if(m_spinner_nodeOption == null){
            m_spinner_nodeOption = (Spinner) findViewById(R.id.spinner_node_option);
        }

        if(m_adapter_nodeOption == null){
            m_adapter_nodeOption = ArrayAdapter.createFromResource(this, R.array.spiner_node_option_array, android.R.layout.simple_spinner_item);
        }

        if(m_spinner_nodeType == null){
            m_spinner_nodeType = (Spinner) findViewById(R.id.spinner_node_type);
            m_spinner_nodeType.setVisibility(View.VISIBLE);
        }

        m_adapter_nodeOption.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        m_spinner_nodeOption.setAdapter(m_adapter_nodeOption);
        m_spinner_nodeOption.setOnItemSelectedListener(this);

        TextView textViewNodeListTitle = (TextView)findViewById(R.id.textViewNodeListTitle);
        textViewNodeListTitle.setText("도엽 내 노드리스트");

        if(m_startNode != -1 && m_endNode != -1){
            m_textViewNodeList.setText("map id : " + m_mapId);
            textViewNodeListTitle.setText("링크 내 노드리스트");

            TextView textView15 = (TextView) findViewById(R.id.textView15);
            textView15.setVisibility(View.GONE);

            TextView textView12 = (TextView) findViewById(R.id.textView12);
            textView12.setVisibility(View.GONE);

            m_spinner_nodeOption.setVisibility(View.GONE);

            FrameLayout frame_node_type = (FrameLayout) findViewById(R.id.frame_node_type);
            frame_node_type.setVisibility(View.GONE);

            m_button_nodeSearch.setVisibility(View.GONE);

            m_listAdapterNode.filterRangeId(m_startNode, m_endNode);
        }

        listViewNodelist.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){

                AlertDialogFragment alertDialogFragment = new AlertDialogFragment(NodeActivity.this, R.layout.dialog_adjnode, "AdjNode");


                View dialog =  alertDialogFragment.getDialogView();

                View adjRow1 = dialog.findViewById(R.id.adjnodeRow1);
                View adjRow2 = dialog.findViewById(R.id.adjnodeRow2);

                TextView textViewRow1Col1Title = (TextView)adjRow1.findViewById(R.id.includeCol2Title1);
                TextView textViewRow1Col2Title = (TextView)adjRow1.findViewById(R.id.includeCol2Title2);
                TextView textViewRow2Col1Title = (TextView)adjRow2.findViewById(R.id.includeCol2Title1);
                TextView textViewRow2Col2Title = (TextView)adjRow2.findViewById(R.id.includeCol2Title2);

                TextView textViewRow1Col1Content = (TextView)adjRow1.findViewById(R.id.includeCol2Content1);
                TextView textViewRow1Col2Content = (TextView)adjRow1.findViewById(R.id.includeCol2Content2);
                TextView textViewRow2Col1Content = (TextView)adjRow2.findViewById(R.id.includeCol2Content1);
                TextView textViewRow2Col2Content = (TextView)adjRow2.findViewById(R.id.includeCol2Content2);


                if(m_startNode != -1 && m_endNode != -1){

                    if(m_startNode >= m_endNode){
                        position += m_endNode;
                    }else{
                        position += m_startNode;
                    }

                    Intent intent = null;
                    Toast.makeText(getApplicationContext(), "노드 번호 : " + position, Toast.LENGTH_SHORT).show();

                    if(m_snodes[position].isAdjNode){

                        textViewRow1Col1Title.setText("노드 id");
                        textViewRow1Col1Content.setText(position+"");

                        textViewRow1Col2Title.setText("인접 메시 id");
                        textViewRow1Col2Content.setText(m_snodes[position].sAdjNode.adjMapId+"");

                        textViewRow2Col1Title.setText("인접 노드 id");
                        textViewRow2Col1Content.setText(m_snodes[position].sAdjNode.adjNodeId+"");

                        textViewRow2Col2Title.setText("인접 링크 id");
                        textViewRow2Col2Content.setText(m_snodes[position].sAdjNode.adjLinkId+"");

                        AlertDialog alertDialog = alertDialogFragment.alertDialongCreate(dialog);
                        alertDialog.show();


                    }else{
                        intent = new Intent(getApplicationContext(), CrossNodeActivity.class);
                        intent.putExtra("map_id", m_mapId);
                        intent.putExtra("node_id", position);
                        startActivity(intent);
                    }

                }else{

                    int nodeId = m_listAdapterNode.m_arrSnode.get(position).nodeId;

                    Intent intent = null;
                    Toast.makeText(getApplicationContext(), "노드 번호 : " + nodeId, Toast.LENGTH_SHORT).show();

                    if(m_snodes[nodeId].isAdjNode){

                        textViewRow1Col1Title.setText("노드 id");
                        textViewRow1Col1Content.setText(nodeId+"");

                        textViewRow1Col2Title.setText("인접 메시 id");
                        textViewRow1Col2Content.setText(m_snodes[nodeId].sAdjNode.adjMapId+"");

                        textViewRow2Col1Title.setText("인접 노드 id");
                        textViewRow2Col1Content.setText(m_snodes[nodeId].sAdjNode.adjNodeId+"");

                        textViewRow2Col2Title.setText("인접 링크 id");
                        textViewRow2Col2Content.setText(m_snodes[nodeId].sAdjNode.adjLinkId+"");

                        AlertDialog alertDialog = alertDialogFragment.alertDialongCreate(dialog);
                        alertDialog.show();

                    }else{
                        intent = new Intent(getApplicationContext(), CrossNodeActivity.class);
                        intent.putExtra("map_id", m_mapId);
                        intent.putExtra("node_id", nodeId);
                        startActivity(intent);

                    }

                }


            }
        });

    }

    class ListAdapterNode extends BaseAdapter{

        Context m_context = null;
        LayoutInflater m_inflater = null;
        ArrayList<Snode> m_arrSnode = null;
        ArrayList<Snode> m_arrSnode_filter = null;
        int m_layout;

        public ListAdapterNode(Context context, int layout_id, ArrayList<Snode> arrSnode){
            m_context = context;
            m_layout = layout_id;
            m_arrSnode = arrSnode;

            m_arrSnode_filter = new ArrayList<Snode>();
            m_arrSnode_filter.addAll(m_arrSnode);

            m_inflater = LayoutInflater.from(m_context);
        }

        @Override
        public int getCount(){
            return m_arrSnode.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            if(convertView == null){
                convertView = m_inflater.inflate(m_layout, parent, false);
            }

            View nodelistrow1 = convertView.findViewById(R.id.nodelistitmerow1);
            View nodelistrow2 = convertView.findViewById(R.id.nodelistitmerow2);
            View nodelistrow3 = convertView.findViewById(R.id.nodelistitmerow3);

            TextView Row1_Col1_Title = (TextView) nodelistrow1.findViewById(R.id.title_col1);
            TextView Row1_Col2_Title = (TextView) nodelistrow1.findViewById(R.id.title_col2);
            TextView Row1_Col3_Title = (TextView) nodelistrow1.findViewById(R.id.title_col3);

            Row1_Col3_Title.setVisibility(View.GONE);

            TextView Row2_Col1_Title = (TextView) nodelistrow2.findViewById(R.id.title_col1);
            TextView Row2_Col2_Title = (TextView) nodelistrow2.findViewById(R.id.title_col2);
            TextView Row2_Col3_Title = (TextView) nodelistrow2.findViewById(R.id.title_col3);

            TextView Row3_Col1_Title = (TextView) nodelistrow3.findViewById(R.id.title_col1);
            TextView Row3_Col2_Title = (TextView) nodelistrow3.findViewById(R.id.title_col2);
            TextView Row3_Col3_Title = (TextView) nodelistrow3.findViewById(R.id.title_col3);


            TextView Row1_Col1_Content = (TextView) nodelistrow1.findViewById(R.id.content_col1);
            TextView Row1_Col2_Content = (TextView) nodelistrow1.findViewById(R.id.content_col2);
            TextView Row1_Col3_Content = (TextView) nodelistrow1.findViewById(R.id.content_col3);

            TextView Row2_Col1_Content = (TextView) nodelistrow2.findViewById(R.id.content_col1);
            TextView Row2_Col2_Content = (TextView) nodelistrow2.findViewById(R.id.content_col2);
            TextView Row2_Col3_Content = (TextView) nodelistrow2.findViewById(R.id.content_col3);

            TextView Row3_Col1_Content = (TextView) nodelistrow3.findViewById(R.id.content_col1);
            TextView Row3_Col2_Content = (TextView) nodelistrow3.findViewById(R.id.content_col2);
            TextView Row3_Col3_Content = (TextView) nodelistrow3.findViewById(R.id.content_col3);

            Row1_Col1_Title.setText("node id");
            Row1_Col1_Content.setText(m_arrSnode.get(position).nodeId+"");

            Row1_Col2_Title.setText("실제확장노드");
            Row1_Col2_Content.setText(m_arrSnode.get(position).ds+"");


            Row2_Col1_Title.setText("경도");
            Row2_Col1_Content.setText((int)m_arrSnode.get(position).lon+"");

            Row2_Col2_Title.setText("인접노드여부");
            Row2_Col2_Content.setText(m_arrSnode.get(position).isAdjNode+"");

            Row2_Col3_Title.setText("인접링크개수");
            Row2_Col3_Content.setText(m_arrSnode.get(position).numofJCLink+"");


            Row3_Col1_Title.setText("위도");
            Row3_Col1_Content.setText((int)m_arrSnode.get(position).lat+"");

            Row3_Col2_Title.setText("상위랭크대응노드여부");
            Row3_Col2_Content.setText(m_arrSnode.get(position).isUpperMatchedNode+"");

            Row3_Col3_Title.setText("실제확장노드DS");
            Row3_Col3_Content.setText(m_arrSnode.get(position).exNodeDs+"");


//            TextView m_textViewNodeIndex = (TextView) convertView.findViewById(R.id.textViewNodeIndex);
//            m_textViewNodeIndex.setText("node id    \n" + m_arrSnode.get(position).nodeId);
//
//            TextView m_textViewNodeOffLon = (TextView) convertView.findViewById(R.id.textViewNodeOffLon);
//            m_textViewNodeOffLon.setText("경도\n" + (int)m_arrSnode.get(position).lon);
//
//            TextView m_textViewNodeOffLat = (TextView) convertView.findViewById(R.id.textViewNodeOffLat);
//            m_textViewNodeOffLat.setText("위도\n" + (int)m_arrSnode.get(position).lat);
//
//            TextView m_textViewNodeDs = (TextView) convertView.findViewById(R.id.textViewNodeDs);
//            m_textViewNodeDs.setText("실제확장노드\n" + m_arrSnode.get(position).ds);
//
//            TextView m_textViewNodeIsAdjNode = (TextView) convertView.findViewById(R.id.textViewNodeIsAdjNode);
//            m_textViewNodeIsAdjNode.setText("인접노드여부\n" + m_arrSnode.get(position).isAdjNode);
//
//            TextView m_textViewNodeIsUpperMatchedNode = (TextView) convertView.findViewById(R.id.textViewNodeIsUpperMatchedNode);
//            m_textViewNodeIsUpperMatchedNode.setText("상위랭크대응노드여부    \n" + m_arrSnode.get(position).isUpperMatchedNode);
//
//            TextView m_textViewNodeNumofJCLink = (TextView) convertView.findViewById(R.id.textViewNodeNumofJCLink);
//            m_textViewNodeNumofJCLink.setText("인접링크개수\n" + m_arrSnode.get(position).numofJCLink);
//
//            TextView m_textViewNodeExNodeDs = (TextView) convertView.findViewById(R.id.textViewNodeExNodeDs);
//            m_textViewNodeExNodeDs.setText("실제확장노드DS\n" + m_arrSnode.get(position).exNodeDs);

            return convertView;
        }

        @Override
        public Object getItem(int position){
            return m_arrSnode.get(position).offLat;
        }

        @Override
        public long getItemId(int position){
            return position;
        }

        public void filterRangeId(int startNode, int endNode){

            m_arrSnode.clear();
            int nodeId = 0;

            for(Snode snode : m_arrSnode_filter){

                if(startNode <= nodeId && nodeId <= endNode){
                    snode.nodeId = nodeId;
                    m_arrSnode.add(snode);

                }else if(endNode <= nodeId && nodeId <= startNode){
                    snode.nodeId = nodeId;
                    m_arrSnode.add(snode);

                }
                ++nodeId;

            }
            notifyDataSetChanged();
        }

        public void filter(){

            m_arrSnode.clear();
            boolean select = false;

            if(m_filter_type.equals("선택없음")){
                m_arrSnode.addAll(m_arrSnode_filter);

            }else if(m_filter_type.equals("인접노드 여부")){

                if(!m_filter_value.equals("선택없음")) {

                    if(m_filter_value.equals("교차점")){
                        select = true;
                    }else if(m_filter_value.equals("인접노드")){
                        select = false;
                    }

                    for(Snode snode : m_arrSnode_filter){

                        if (select == snode.isAdjNode) {
                            m_arrSnode.add(snode);
                        }
                    }
                }else{
                    m_arrSnode.addAll(m_arrSnode_filter);
                }

            }else if(m_filter_type.equals("상위랭크대응노드 여부")){

                if(!m_filter_value.equals("선택없음")) {

                    if(m_filter_value.equals("아님")){
                        select = false;
                    }else if(m_filter_value.equals("상위랭크대응노드")) {
                        select = true;
                    }

                    for(Snode snode : m_arrSnode_filter) {

                        if(select == snode.isUpperMatchedNode){
                            m_arrSnode.add(snode);
                        }
                    }
                }else{
                    m_arrSnode.addAll(m_arrSnode_filter);
                }


            }else{

                if(m_editText_nodeSearch.getText().length() == 0 || m_editText_nodeSearch.getText() == null || Integer.parseInt(m_editText_nodeSearch.getText().toString()) < 0 ) {
                    Toast.makeText(getApplicationContext(), "잘못된 값입니다. 다시입력해주세요", Toast.LENGTH_SHORT).show();
                    m_arrSnode.addAll(m_arrSnode_filter);
                }else{
                    if(m_filter_type.equals("인접링크 개수")){

                        for(Snode snode : m_arrSnode_filter){

                            if (Integer.parseInt(m_editText_nodeSearch.getText().toString()) == snode.numofJCLink) {
                                m_arrSnode.add(snode);
                            }
                        }

                    }else if(m_filter_type.equals("노드 id")){

                        for(Snode snode : m_arrSnode_filter){

                            if (Integer.parseInt(m_editText_nodeSearch.getText().toString()) == snode.nodeId) {
                                m_arrSnode.add(snode);
                            }
                        }

                    }
                }
            }

            notifyDataSetChanged();

        }

    }

    public void onClick(View v){

        if(v == null){
            return;
        }else if(v == m_button_nodeSearch){

            m_listAdapterNode.filter();

        }

    }

    public void onItemSelected(AdapterView<?> parent, View v, int pos, long id){

        if(parent == m_spinner_nodeOption){

            m_spinner_nodeType.setVisibility(View.VISIBLE);
            m_editText_nodeSearch.setVisibility(View.INVISIBLE);

            if(parent.getItemAtPosition(pos).equals("인접노드 여부")){

                m_adapter_nodeType = ArrayAdapter.createFromResource(this, R.array.spiner_node_option_isAdjNode_array, android.R.layout.simple_spinner_item);

            }else if(parent.getItemAtPosition(pos).equals("상위랭크대응노드 여부")){

                m_adapter_nodeType = ArrayAdapter.createFromResource(this, R.array.spiner_node_option_isUpperMatchedNode_array, android.R.layout.simple_spinner_item);

            }else if(parent.getItemAtPosition(pos).equals("선택없음")){

                m_adapter_nodeType = ArrayAdapter.createFromResource(this, R.array.spiner_option_default_array, android.R.layout.simple_spinner_item);

            }else{
                m_spinner_nodeType.setVisibility(View.INVISIBLE);
                m_editText_nodeSearch.setVisibility(View.VISIBLE);
                m_adapter_nodeType = ArrayAdapter.createFromResource(this, R.array.spiner_option_default_array, android.R.layout.simple_spinner_item);

            }

            m_adapter_nodeType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            m_spinner_nodeType.setAdapter(m_adapter_nodeType);
            m_spinner_nodeType.setOnItemSelectedListener(this);

            m_filter_type = parent.getItemAtPosition(pos).toString();

        }else if(parent == m_spinner_nodeType){

            m_filter_value = parent.getItemAtPosition(pos).toString();

        }else{
            Toast.makeText(getApplicationContext(), "View 선택없음", Toast.LENGTH_SHORT).show();
        }



    }

    public void onNothingSelected(AdapterView<?> parent){

    }

    public void logCall(String str_log){

            FragmentManager fragmentManager = getSupportFragmentManager();
            LogDialogFragment logDialogFragment = new LogDialogFragment();
            logDialogFragment.setActivity(this);
            logDialogFragment.LogSet(str_log);
            logDialogFragment.show(fragmentManager, "log");
    }

}
