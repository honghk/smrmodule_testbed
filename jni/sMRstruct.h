#ifndef __SMR_STRUCT_H__
#define __SMR_STRUCT_H__

#include <string.h>

#if __APPLE__
//iOS
    #define  D(...) printf(__VA_ARGS__)
#else
//Android
    #include <android/log.h>
    #define  D(...) __android_log_print(ANDROID_LOG_INFO, "SMR", __VA_ARGS__)
#endif


#define FILENAME_SMR_MANAGE             "smrmanage.bin"  
//SMAP_HEADER / SRANK_MANAGER / SRANK_MAP
//도엽관리 테이블 = 도엽관리 테이블 헤더 + 도엽 관리 정보 + 도엽 위치정보 레코드 + 도엽 위치정보 레코드 + ......
#define FILENAME_SMR_MESHINFO           "smrmeshinfo.bin"   
//SMESH_INFO
//도업 패키지 정보 테이블 = Checksum + 도엽 패키지 정보 + 도엽 패키지 정보 + .....
#define FILENAME_SMR_MESHDATA           "smrmeshdata.bin"
//SNODE / SCROSSNODE / SADJNODE / SLINK / SVERTEX
//도엽 패키지 데이터 = mesh도엽 패키지 레코드 테이블 + mesh도엽 패키지 레코드 테이블 + .... 
#define FILENAME_SMR_CACHEINFO			"smrcacheinfo.bin"

//#pragma pack(1)

///[ERROR DEFINE]////////////////////////////////
#define NO_ERR                      0
#define ERR_UNKNOWN					-1
#define ERR_INVALID_OPERATION		-12
#define ERR_MALLOC_FAIL             -13
#define ERR_NOTMATCH_CHECKSUM       -14
#define ERR_BAD_PARAM               -15
#define ERR_NULL                    -16
#define ERR_INVALID_DATA            -17
#define ERR_FILE_OPEN               -18
#define ERR_BAD_HANDLE              -19
#define ERR_WRITE_FAIL              -20
#define ERR_NOT_FOUND               -21         // mesh Á¤º¸°¡ ¾øÀ½
#define ERR_EMPTY_DATA              -22         // mesh´Â ÀÖÀ¸³ª À¯È¿µ¥ÀÌÅÍ´Â ¾øÀ½
#define ERR_CHECK_SUM				-23

#define ERR_OPEN_SMR_MANAGE			-24
#define ERR_OPEN_SMR_MESHINFO		-25
#define ERR_OPEN_SMR_CACHEINFO		-26
#define ERR_OPEN_SMR_MESHDATA		-27

#define ERR_LIMIT					-28

#define ERR_SMR_MANAGE_CHECKSUM		-29
#define ERR_SMR_MESHINFO_CHECKSUM	-30
#define ERR_SMR_CACHEINFO_CHECKSUM	-31
#define ERR_SMR_MESHDATA_CHECKSUM	-32

#define ERR_SMR_MANAGE_HANDLE		-33
#define ERR_SMR_MESHINFO_HANDLE		-34
#define ERR_SMR_CACHEINFO_HANDLE	-35
#define ERR_SMR_MESHDATA_HANDEL		-36

#define ERR_SMR_MANAGE_DATA			-37
#define ERR_SMR_MESHINFO_DATA		-38
#define ERR_SMR_CACHEINFO_DATA		-39
#define ERR_SMR_MESHDATA_DATA		-40


///[ERROR DEFINE]////////////////////////////////

///[SIZE DEFINE]/////////////////////////////////
#define SMESH_INFO_SIZE             10              //(short + int + int/)
#define SNODE_SIZE                  8               //(short + short + int)
#define CROSSNODE_MAX_SIZE          24              //(short*8 + char*8)
#define FILE_LENGTH     			256
#define SIDX_SIZE					50
///[SIZE DEFINE]/////////////////////////////////

typedef struct sMapHeader
{
	char    version[16];            // FileVersion
	char    volumeId[16];           // 볼륨구조 식별
	char    systemId[16];           // 시스템 식별
	char    createdDate[16];        // 볼륨 만들어진 일시(yymmdd)
	char    updatedDate[16];        // 볼륨 갱신 일시(yymmdd)
	int     centerLon;              // 지도 중심 좌표 (경도 0.01초 단위)
	int     centerLat;              // 지도 중심 좌표 (위도 0.01초 단위)
	char    reserved[164];          // Reserved
} SMAP_HEADER ;

typedef struct sRankManager
{
	int numOfMesh;                  // 메쉬 갯수
	int meshWidth;                  // 매쉬 Width(0.01초 단위)
	int meshHeight;                 // 메쉬 Height(0.01초 단위)
	int left;                       // 메쉬 전체의 좌단 경도(0.01초 단위)
	int bottom;                     // 메쉬 전체의 하단 경도(0.01초 단위)
	int right;                      // 메쉬 전체의 우단 경도(0.01초 단위)
	int top;                        // 메쉬 전체의 상단 경도(0.01초 단위)
	int reserved;                   // Reserved
} SRANK_MANAGER ;

typedef struct sRankMap
{
	short numOfNode;                // 메쉬 내 노드 개수
	short numOfLink;                // 메쉬 내 링크 개수
	int xratio;                     // 메쉬의 X축 실거리비(meter/second)
	int left;                       // 메쉬의 좌단 경도(0.01초 단위)
	int bottom;                     // 메쉬의 하단 경도(0.01초 단위)
	int offsetNode;                 // 노드 레코드 테이블 Offset
	int offsetCrossnode;            // 교차점 노드 레코드 테이블 Offset
	int offsetAdjnode;              // 인접 노드 레코드 테이블 Offset
	int offsetLink;                 // 링크 레코드 테이블 Offset
	int offsetVertex;               // 보간점 레코드 테이블 Offset
} SRANK_MAP ;

typedef struct sMeshInfo
{
	short meshId;                   // 메쉬ID
	int ds;                         // 해당 메쉬 데이터 DS
	int size;                       // 해당 메쉬 데이터 크기
} SMESH_INFO ;

typedef struct sRealMeshInfo
{
	int realMeshId ;				// ½ÇÁ¦ MeshId
	int meshId ;					// ½ÇÁ¦ MeshId¿¡ ´ëÇÑ index Ã·ÀÚ
	int flag ;						// 0 - ¼­¹ö¿¡¼­ °¡Á®¿ÀÁö ¸øÇÑ »óÅÂ, 1 - ¼­¹ö¿¡¼­ °¡Á®¿Â »óÅÂ
} REAL_MESH_INFO ;					

typedef struct sMapping
{
	int realMeshId ;
	int meshId ;
} REAL_MAPP ;

#ifndef STRUCT_SNODE
#define STRUCT_SNODE
typedef struct sNode
{
	unsigned short offLon;			// 노드의 경도(0.01초 단위)(메쉬 좌단 경도로부터의 offset)
	unsigned short offLat;          // 노드의 위도(0.01초 단위)(메쉬 하단 경도로부터의 offset)

	int ds;							//인접링크개수 + 해당확장노드레코드의 offset(1+3) 실제확장노드 DS

	bool    isAdjNode()             const { return (ds & 0x80000000) >> 31 == 1; }  // 0 - 교차점, 1 - 인접노드
	bool    isUpperMatchedNode()    const { return (ds & 0x40000000) >> 30 == 1; }  // 0 - 아님, 1 - 상위 랭크 대응 노드
	int     getnumofJCLink()        const { return (ds & 0x3F000000) >> 24; }   // Á¢¼Ó¸µÅ©ÀÇ °³¼ö - 1 byte
	int     getExNodeDS()           const { return ds & 0x00FFFFFF; }           // ÇØ´ç È®Àå ³ëµå ·¹ÄÚµå±îÁöÀÇ DS - 3byte
} SNODE ;
#endif

#ifndef STRUCT_SCROSSNODE
#define STRUCT_SCROSSNODE
typedef struct sCrossNode
{
	short conlink[8];       // connected link id array
	char conpasscode[8];    // intersection pass-code to the link from the node

	int     size(int jc)        { return 2 * jc + jc; }
	void    setData ( char* comein, int jc )
	{
		memset(conlink, 0x00, sizeof(short)*8);
		memset(conpasscode, 0x00, sizeof(char)*8);

		memcpy(conlink,     &(comein[0]),           2 * jc);
		memcpy(conpasscode, &(comein[2 * jc]),      jc);
	}
} SCROSSNODE;
#endif

#ifndef STRUCT_SADJNODE
#define STRUCT_SADJNODE
typedef struct sAdjNode
{
	int adjMapId;               // 인접 메쉬 D
	short adjNodeId;            // 인접 도엽의 연결 노드 ID
	short adjLinkId;            // 인접 도엽의 연결 링크 ID
} SADJNODE;
#endif

#ifndef STRUCT_SLINK
#define STRUCT_SLINK
typedef struct sLink
{
	short _startId;             // 시작 노드 ID
	short _endId;               // 종료 노드 ID
	short _length;              // 링크의 길이 (meter)
	short _attr;            	// 링크 속성
	short _numOfVertex;     	// 링크 내 보간점 개수
	short _vertexIndex;     	// 해당 보간점 리스트의 INDEX
	// 링크 속성
	int laneNum()           { return (_attr & 0x3C00) >> 10; }      //10~13 차선수
	int drive()             { return (_attr & 0x0300) >> 8; }       //8~9 링크 통행 코드
	int divide()            { return (_attr & 0x0080) >> 7; }       //7 본선 분리
	int linkType()          { return (_attr & 0x0060) >> 5; }       //5~6 링크 종별
	int linkAttr()          { return (_attr & 0x001C) >> 2; }       //2~4 링크 속성
	int roadType()          { return (_attr & 0x0003) >> 0; }       //0~1 도로 종별

	friend bool operator==(sLink& x, sLink& y) {
		if (x._startId == y._startId && x._endId == y._endId) {
			return true;
		}
		return false;
	}
} SLINK;
#endif

#ifndef STRUCT_SVERTEX
#define STRUCT_SVERTEX
typedef struct sVertex
{
	short   offLon;     //º¸°£Á¡ÀÇ °æµµ
	short   offLat;     //º¸°£Á¡ÀÇ À§µµ
} SVERTEX;
#endif


//2013.06.27 
#ifndef __SMR_R_IDX_H__
#define __SMR_R_IDX_H__

typedef struct {
	int order ;
	int map_id ;
	int size ;
	char *data ;
} R_IDX ;

#endif


#endif	//__SMR_STRUCT_H__
