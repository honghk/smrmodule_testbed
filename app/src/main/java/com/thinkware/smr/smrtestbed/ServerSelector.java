package com.thinkware.smr.smrtestbed;

/**
 * Created by honghk on 2016. 12. 8..
 */

public class ServerSelector {

    private static ServerSelector m_inst = null;

    // true : 상용서버
    // false : 개발서버
    private boolean mIsService = false;//false;//false;


    ///[상용]///
    public final static String AIR_IP = "ttnaviagt.magicn.com" ; // 실수로 접근할까봐 처음에 tt 넣어둠
    public final static int AIR_PORT = 80 ;
    public final static int AIR_SSL_PORT = 1443 ;

    ///[테스트]///
    //public final static String TEST_AIR_IP = "221.148.247.131";

    public final static String TEST_AIR_IP = "221.148.247.130";
    public final static int TEST_AIR_PORT = 443;

    //[APP_CLAUSEAGREE]//
    public final static String APP_CLAUSEAGREE = "/OllehInavi/clauseAgree";

    //[MAP_DOWNLOAD_INFO]//
    public final static String APP_DOWNLOAD_INFO = "/OllehInavi/mapDownloadInfo";

    private ServerSelector()
    {
        ;
    }

    public static ServerSelector getInstance()
    {
        if( m_inst == null ) {
            synchronized(ServerSelector.class) {
                if (m_inst == null) {
                    m_inst = new ServerSelector();
                }
            }
        }
        return m_inst;
    }

    public static ServerSelector getInstance(boolean serverSelct)
    {
        if( m_inst == null ) {
            synchronized(ServerSelector.class) {
                if (m_inst == null) {
                    m_inst = new ServerSelector();
                }
            }
        }
        return m_inst;
    }

    public String getSvrSSL()
    {
        if( isServiceVersion() )
            return "https";
        else
            return "http";
    }

    public String getSvrURL()
    {
        if( isServiceVersion() )
            return AIR_IP;
        else
            return TEST_AIR_IP;
    }
    public int getSvrPort()
    {
        if( isServiceVersion() )
            return AIR_SSL_PORT;
        else
            return TEST_AIR_PORT;
    }

    public String getURLPrefix()
    {
        return getSvrSSL() + "://" + getSvrURL() +":"+ getSvrPort();
    }

    public boolean isServiceVersion()
    {
        return mIsService;
    }

    public void SetServiceVersion(boolean bServiceServer)
    {
        // true : 상용서버
        // false : 개발서버
        mIsService = bServiceServer;
    }


    public void ClearSeverSelector()
    {
        mIsService = true;
        m_inst = null;
    }

}
