package com.thinkware.smr.smrtestbed;

/**
 * Created by honghk on 2016. 12. 13..
 */

public class Snode {

    int nodeId;
    char offLon;
    char offLat;
    int lon;
    int lat;
    int ds;
    boolean isAdjNode;
    boolean isUpperMatchedNode;
    int numofJCLink;
    int exNodeDs;
    SAdjNode sAdjNode;
    SCrossNode sCrossNode;

    public void create_AdjNode(){
        sAdjNode = new SAdjNode();
    }

    public void create_CrossNode(){
        sCrossNode = new SCrossNode();
    }

    public Snode(){
        lon = 0;
        lat = 0;

        sCrossNode = new SCrossNode();
        sAdjNode = new SAdjNode();
    }

    //////////////인접노드//////////////
    class SAdjNode{

        int adjMapId;               // 인접 메쉬 D
        short adjNodeId;            // 인접 도엽의 연결 노드 ID
        short adjLinkId;            // 인접 도엽의 연결 링크 ID

        public SAdjNode(){
            adjMapId = 0;
            adjNodeId = 0;
            adjLinkId = 0;
        }

    }
    //////////////////////////////////

    //////////////교차점노드//////////////
    class SCrossNode{

        short[] conlink;       // connected link id array
        char[] conpasscode;    // intersection pass-code to the link from the node

        public SCrossNode(){
            conlink = new short[8];
            conpasscode = new char[8];
        }

    }
    //////////////////////////////////
}