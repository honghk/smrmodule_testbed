package com.thinkware.smr.smrtestbed;

/**
 * Created by honghk on 2016. 12. 12..
 */

public class SrankMap {

    short numOfNode;                // 메쉬 내 노드 개수
    short numOfLink;                // 메쉬 내 링크 개수
    int xratio;                     // 메쉬의 X축 실거리비(meter/second)
    int left;                       // 메쉬의 좌단 경도(0.01초 단위)
    int bottom;                     // 메쉬의 하단 위도(0.01초 단위)
    int offsetNode;                 // 노드 레코드 테이블 Offset
    int offsetCrossnode;            // 교차점 노드 레코드 테이블 Offset
    int offsetAdjnode;              // 인접 노드 레코드 테이블 Offset
    int offsetLink;                 // 링크 레코드 테이블 Offset
    int offsetVertex;               // 보간점 레코드 테이블 Offset

}
