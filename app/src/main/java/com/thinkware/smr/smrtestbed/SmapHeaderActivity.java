package com.thinkware.smr.smrtestbed;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class SmapHeaderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smap_header_textver);

        SmrControl smrControl = new SmrControl();
        SmapHeader smapHeader = smrControl.getSmapHeader();
        SrankManager srankManager = smrControl.getSrankManager();

        View includesmaphaeader = findViewById(R.id.include_smapheader);

        TextView textViewTile = (TextView) includesmaphaeader.findViewById(R.id.textViewTitle);
        textViewTile.setText("SmapHeader\n(도엽 관리 테이블 헤더)");

        View includesrankmap7row1col = includesmaphaeader.findViewById(R.id.include_7row1col);
        View includesrankmap7row2col = includesmaphaeader.findViewById(R.id.include_7row2col);
        includesrankmap7row2col.setVisibility(View.GONE);

        TextView textView1coltitle1 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title1);
        TextView textView1coltitle2 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title2);
        TextView textView1coltitle3 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title3);
        TextView textView1coltitle4 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title4);
        TextView textView1coltitle5 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title5);
        TextView textView1coltitle6 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title6);
        TextView textView1coltitle7 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title7);

        TextView textView1colcontent1 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content1);
        TextView textView1colcontent2 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content2);
        TextView textView1colcontent3 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content3);
        TextView textView1colcontent4 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content4);
        TextView textView1colcontent5 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content5);
        TextView textView1colcontent6 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content6);
        TextView textView1colcontent7 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content7);

        textView1coltitle1.setText("FileVersion");
        textView1colcontent1.setText(smapHeader.version+"");

        textView1coltitle2.setText("볼륨구조식별");
        textView1colcontent2.setText(smapHeader.volumeId+"");

        textView1coltitle3.setText("시스템식별");
        textView1colcontent3.setText(smapHeader.systemId+"");

        textView1coltitle4.setText("볼륨생성일시");
        textView1colcontent4.setText(smapHeader.createdDate+"");

        textView1coltitle5.setText("볼륨갱신일시");
        textView1colcontent5.setText(smapHeader.updatedDate+"");

        textView1coltitle6.setText("지도 중심좌표(경도)");
        textView1colcontent6.setText(smapHeader.centerLon+"");

        textView1coltitle7.setText("지도 중심좌표(위도)");
        textView1colcontent7.setText(smapHeader.centerLat+"");


        View includesrankmanager = findViewById(R.id.include_srankmanager);

        TextView textViewTile2 = (TextView) includesrankmanager.findViewById(R.id.textViewTitle);
        textViewTile2.setText("SrankaManager\n(도엽 관리 정보)");

        View includesrankmanager7row1col = includesrankmanager.findViewById(R.id.include_7row1col);
        View includesrankmanager7row2col = includesrankmanager.findViewById(R.id.include_7row2col);
        includesrankmanager7row2col.setVisibility(View.GONE);

        TextView textView1coltitle1_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_title1);
        TextView textView1coltitle2_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_title2);
        TextView textView1coltitle3_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_title3);
        TextView textView1coltitle4_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_title4);
        TextView textView1coltitle5_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_title5);
        TextView textView1coltitle6_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_title6);
        TextView textView1coltitle7_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_title7);

        TextView textView1colcontent1_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_content1);
        TextView textView1colcontent2_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_content2);
        TextView textView1colcontent3_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_content3);
        TextView textView1colcontent4_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_content4);
        TextView textView1colcontent5_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_content5);
        TextView textView1colcontent6_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_content6);
        TextView textView1colcontent7_srankmanager = (TextView) includesrankmanager7row1col.findViewById(R.id.include_7row_content7);

        textView1coltitle1_srankmanager.setText("도엽개수");
        textView1colcontent1_srankmanager.setText(srankManager.numOfMesh+"");

        textView1coltitle2_srankmanager.setText("도엽의 Width");
        textView1colcontent2_srankmanager.setText(srankManager.meshWidth+"");

        textView1coltitle3_srankmanager.setText("도엽의 Height");
        textView1colcontent3_srankmanager.setText(srankManager.meshHeight+"");

        textView1coltitle4_srankmanager.setText("도엽 전체의 좌단경도");
        textView1colcontent4_srankmanager.setText(srankManager.left+"");

        textView1coltitle5_srankmanager.setText("도엽 전체의 하단경도");
        textView1colcontent5_srankmanager.setText(srankManager.bottom+"");

        textView1coltitle6_srankmanager.setText("도엽 전체의 우단경도");
        textView1colcontent6_srankmanager.setText(srankManager.right+"");

        textView1coltitle7_srankmanager.setText("도엽 전체의 상단경도");
        textView1colcontent7_srankmanager.setText(srankManager.top+"");


    }
}
