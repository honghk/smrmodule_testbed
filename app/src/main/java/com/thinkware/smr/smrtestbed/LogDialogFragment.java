package com.thinkware.smr.smrtestbed;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;


public class LogDialogFragment extends android.support.v4.app.DialogFragment {

    private String m_str_log = null;
    private MainActivity m_mainActivity = null;
    //private NodeActivity m_nodeActivity = null;
    private Activity m_activity = null;

    public void LogSet(String str_log){
        m_str_log = "----에러로그----\n" + str_log;
    }
    public void setActivity(MainActivity mainActivity){
        m_mainActivity = mainActivity;
    }
    public void setActivity(Activity activity){
        m_activity = activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());//test1

        builder.setMessage(m_str_log)
                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(m_mainActivity != null){
                            m_mainActivity.setLogFlag(true);
                        }else{
//                            m_nodeActivity.finish();
                            m_activity.finish();
                        }
                    }
                });
        return builder.create();
    }

}

//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        // Use the Builder class for convenient dialog construction
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//
//        //m_mainActivity.threadStop();/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//        builder.setMessage(m_str_log)
//                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        //m_mainActivity.threadReStart();
//                        m_mainActivity.setLogFlag(true);
//                    }
//                });
////                .setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
////                    public void onClick(DialogInterface dialog, int id) {
////                    }
////                })
//        return builder.create();
//    }
//
//}