package com.thinkware.smr.smrtestbed;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class SrankMapActivity extends AppCompatActivity {

    private static final String TAG = "SrankMapActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_srank_map_textver);

        int mapId = -1;

        Intent intent = getIntent();
        mapId = intent.getIntExtra("map_id", -1);

        SmrControl smrControl = new SmrControl(this);
        SrankMap srankMap = smrControl.getSrankMap(mapId);//

        if(srankMap != null){

            View includesrankmap = findViewById(R.id.include_srankmap);

            TextView textViewSrankMapTitle = (TextView) includesrankmap.findViewById(R.id.textViewTitle);
            textViewSrankMapTitle.setText("SrankMap\n(도엽위치정보레코드)");

            View includesrankmap7row1col = includesrankmap.findViewById(R.id.include_7row1col);

            TextView textView1coltitle1 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title1);
            TextView textView1coltitle2 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title2);
            TextView textView1coltitle3 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title3);
            TextView textView1coltitle4 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title4);
            TextView textView1coltitle5 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title5);
            TextView textView1coltitle6 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title6);
            TextView textView1coltitle7 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_title7);

            TextView textView1colcontent1 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content1);
            TextView textView1colcontent2 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content2);
            TextView textView1colcontent3 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content3);
            TextView textView1colcontent4 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content4);
            TextView textView1colcontent5 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content5);
            TextView textView1colcontent6 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content6);
            TextView textView1colcontent7 = (TextView) includesrankmap7row1col.findViewById(R.id.include_7row_content7);

            View includesrankmap7row2col = includesrankmap.findViewById(R.id.include_7row2col);

            TextView textView2coltitle1 = (TextView) includesrankmap7row2col.findViewById(R.id.include_7row_title1);
            TextView textView2coltitle2 = (TextView) includesrankmap7row2col.findViewById(R.id.include_7row_title2);
            TextView textView2coltitle3 = (TextView) includesrankmap7row2col.findViewById(R.id.include_7row_title3);

            TextView textView2colcontent1 = (TextView) includesrankmap7row2col.findViewById(R.id.include_7row_content1);
            TextView textView2colcontent2 = (TextView) includesrankmap7row2col.findViewById(R.id.include_7row_content2);
            TextView textView2colcontent3 = (TextView) includesrankmap7row2col.findViewById(R.id.include_7row_content3);

            textView1coltitle1.setText("도엽 내 노드 개수");
            textView1colcontent1.setText(srankMap.numOfNode+"");

            textView1coltitle2.setText("도엽 내 링크 개수");
            textView1colcontent2.setText(srankMap.numOfLink+"");

            textView1coltitle3.setText("도엽의 X축 실거리비(meter/second)");//
            textView1colcontent3.setText(srankMap.xratio+"");

            textView1coltitle4.setText("도엽의 좌단 경도");
            textView1colcontent4.setText(srankMap.left+"");

            textView1coltitle5.setText("도엽의 하단 위도");
            textView1colcontent5.setText(srankMap.bottom+"");

            textView1coltitle6.setText("노드 레코드 테이블 Offset");
            textView1colcontent6.setText(srankMap.offsetNode+"");

            textView1coltitle7.setText("교차점 노드 레코드 테이블 Offset");
            textView1colcontent7.setText(srankMap.offsetCrossnode+"");

            textView2coltitle1.setText("인접 노드 레코드 테이블 Offset");
            textView2colcontent1.setText(srankMap.offsetAdjnode+"");

            textView2coltitle2.setText("링크 레코드 테이블 Offset");
            textView2colcontent2.setText(srankMap.offsetLink+"");

            textView2coltitle3.setText("보간점 레코드 테이블 Offset");
            textView2colcontent3.setText(srankMap.offsetVertex+"");

        }
    }

    public void logCall(String str_log){

        FragmentManager fragmentManager = getSupportFragmentManager();
        LogDialogFragment logDialogFragment = new LogDialogFragment();//
        logDialogFragment.setActivity(this);
        logDialogFragment.LogSet(str_log);
        logDialogFragment.show(fragmentManager, "log");
    }
}
