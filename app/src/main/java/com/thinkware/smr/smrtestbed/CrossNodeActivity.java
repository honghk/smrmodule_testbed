package com.thinkware.smr.smrtestbed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CrossNodeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cross_node_textver);

        SmrControl smrControl = new SmrControl();
        Intent intent = getIntent();

        int mapId = intent.getIntExtra("map_id", -1);
        int nodeId = intent.getIntExtra("node_id", -1);

        SrankMap srankMap = smrControl.getSrankMap(mapId);

        Snode[] snodes = smrControl.getNode(mapId);

        int i = 0;

        for(i = 0; i<8; i++){

            int resourceId = getResources().getIdentifier("includeCol2Row"+ (i+1) ,"id", "com.thinkware.smr.smrtestbed");
            View linklistrow = findViewById(resourceId);

            TextView Row1_Col1_Title1 = (TextView) linklistrow.findViewById(R.id.includeCol2Title1);
            TextView Row1_Col1_Content1 = (TextView) linklistrow.findViewById(R.id.includeCol2Content1);

            Row1_Col1_Title1.setText("conlink[" + i + "]");
            Row1_Col1_Content1.setText(snodes[nodeId].sCrossNode.conlink[i]+"");

            TextView Row1_Col1_Title2 = (TextView) linklistrow.findViewById(R.id.includeCol2Title2);
            TextView Row1_Col1_Content2 = (TextView) linklistrow.findViewById(R.id.includeCol2Content2);

            Row1_Col1_Title2.setText("conpasscode[" + i + "]");
            Row1_Col1_Content2.setText((int)snodes[nodeId].sCrossNode.conpasscode[i]+"");
        }


//        for(i = 0; i < snodes[nodeId].numofJCLink; i++){
//
//
//            String tempConLink = "conlink(링크 ID)[" + i + "]\n" + snodes[nodeId].sCrossNode.conlink[i] + "\n";
//            strConLink = strConLink + tempConLink;
//
//            String tempConPassCode = "";
//            if((int)snodes[nodeId].sCrossNode.conpasscode[i] == 1){
//                tempConPassCode = "conpasscode(교차점 통행코드)[" + i + "]\n" + (int)snodes[nodeId].sCrossNode.conpasscode[i] + "\n"; //"(통행가능)\n";
//            }
//            else{
//                tempConPassCode = "conpasscode(교차점 통행코드)[" + i + "]\n" + (int)snodes[nodeId].sCrossNode.conpasscode[i] + "\n"; // "(통행불가)\n";
//            }
//
//            strConPassCode = strConPassCode + tempConPassCode;
//
//        }
//
//        TextView m_textViewConLink = (TextView) findViewById(R.id.textViewConLink);
//        m_textViewConLink.setText(strConLink);
//
//        TextView m_textViewConPassCode = (TextView) findViewById(R.id.textViewConPassCode);
//        m_textViewConPassCode.setText(strConPassCode);

    }
}
